import "@cypress/code-coverage/support";
import "cypress-file-upload";

//https://github.com/cypress-io/cypress/issues/6564
Cypress.Commands.overwrite("contains", (originalFn, subject, filter, text, options = {}) => {
  // determine if a filter argument was passed
  if (typeof text === "object") {
    options = text;
    text = filter;
    filter = undefined;
  }

  options.matchCase = false;

  return originalFn(subject, filter, text, options);
});

beforeEach(() => {
  cy.clearCookies();
  cy.clearLocalStorage();
  cy.visit("http://localhost", {
    onBeforeLoad: (win) => {
      win.sessionStorage.clear();
      win.indexedDB.deleteDatabase("firebaseLocalStorageDb");
    }
  });
  cy.exec("npx nps server.restore_default_db");
});

