//@flow
import {mergeTypeDefs} from "@graphql-toolkit/schema-merging";
import deepMerge from "lodash.merge";
import GraphQLJSON from "graphql-type-json";
import {GraphQLUpload} from "graphql-upload";
import GraphQLLong from "graphql-type-long";
import {gql} from "apollo-server-express";
import {TheUserGqls} from "./model/TheUser";
import {TheAlbumGqls} from "./model/TheAlbum";
import {ThePhotoGqls} from "./model/ThePhoto";
import {TheCommentGqls} from "./model/TheComment";
import {albumQueries, albumResolvers} from "./resolvers/albumQueries";
import {userQueries, userResolvers} from "./resolvers/userQueries";
import {commentQueries, commentResolvers} from "./resolvers/commentQueries";
import {photoQueries, photoResolvers} from "./resolvers/photoQueries";
import {Void} from "./resolvers/util";

const scalarDefs = gql`
  scalar Upload
  scalar Long 
  scalar Void 
  scalar JSON
`;

const scalarResolvers = {
  Upload: GraphQLUpload,
  Long: GraphQLLong,
  Void: Void,
  JSON: GraphQLJSON,
};

export const typeDefs: any  = mergeTypeDefs([
  scalarDefs,
  TheUserGqls,
  TheAlbumGqls,
  ThePhotoGqls,
  TheCommentGqls,
  userQueries,
  albumQueries,
  commentQueries,
  photoQueries,
]);

export const resolvers:any = deepMerge(
  scalarResolvers,
  userResolvers,
  albumResolvers,
  commentResolvers,
  photoResolvers,
);
