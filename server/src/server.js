//@flow
import express from "express";
import cookieParser from "cookie-parser";
import path from "path";
import cors from "cors";
import {apolloServer} from "./apolloServer";
import {graphqlUploadExpress} from "graphql-upload";
import {env} from "./env";

console.log("express start...");

const app = express();
// console.log("process.env: ", process.env);

app.use(cookieParser());
app.use(graphqlUploadExpress({ maxFileSize: 5368709120 /* 5Mo */, maxFiles: 50 }));

apolloServer.applyMiddleware({ app,cors : {
    "origin":  ["http://localhost","https://familystories.page","http://familystories.page",
      env.serverHost,
      // "http://192.168.94.73",
      "http://192.168.178.73",
    ],
    "credentials":  true,
    //to allow all domain
    // "origin":   (origin, callback) => {
    //   console.log("origin: ",origin);
    //   callback(null, true);
    // }
  }
 });

app.use(cors());
//https://github.com/gotwarlost/istanbul/blob/master/ignoring-code-for-coverage.md
if (global.__coverage__) {
  console.log("coverage enabled");
  require("@cypress/code-coverage/middleware/express")(app);
}

app.use("/storybook_build",express.static(path.join(env.projectRoot, "storybook_build")));

//front end is served by 'react-scripts start' during development
if (process.env.NODE_ENV==="production")
  app.use("/",express.static(path.join(env.projectRoot, "build")));

app.use("/uploads",express.static(path.join(env.serverRoot,"uploads")));

const port = process.env.NODE_ENV==="production" ? 80 : 8080;
app.listen(port,() => {
  // eslint-disable-next-line no-undef
  console.info("🚀 Server ready at "+port);
});

