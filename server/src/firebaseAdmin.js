//@flow
let serviceAccount;
if (process.env.firebaseKey)
  serviceAccount = JSON.parse(Buffer.from(process.env.firebaseKey, "base64").toString("ascii"));
else
  serviceAccount = require("../firebaseKey.secure.json");

const firebase = require("firebase-admin");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount)
});

export const fire = firebase;
