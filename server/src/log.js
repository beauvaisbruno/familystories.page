//@flow
console.log("logger start...");

const SimpleNodeLogger = require("simple-node-logger");
export let logger : any;
if (process.env.NODE_ENV==="production") {
  logger = SimpleNodeLogger.createRollingFileLogger({
    errorEventName: "error",
    logFilePath: ".log",
    timestampFormat: "HH:mm:ss",
    logDirectory: "logs",
    fileNamePattern: "<DATE>.log",
    dateFormat: "YYYY.MM.DD"
  });
  logger.setLevel("info");
  // logger.setLevel("debug");
}
else{
  logger = SimpleNodeLogger.createSimpleLogger();
  logger.setLevel("debug");
}

["log","info","warn","error"].forEach((level) => {
  const prev = console[level];
  //$FlowIgnore
  console[level]= (...args) => {
    logger.debug(...args);
    prev(...args);
  };
});


process.on("unhandledRejection", function(reason, p){
  console.warn("Unhandled Rejection at:", p, "reason:", reason);
  logger.warn("Unhandled Rejection at:", p, "reason:", reason);
});

export const log = logger;
