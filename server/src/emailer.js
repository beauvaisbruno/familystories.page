//@flow

import {env} from "./env";
import fs from "fs";

let credential;
if (process.env.smtp)
  credential = JSON.parse(Buffer.from(process.env.smtp, "base64").toString("ascii"));
else {
  if (fs.existsSync("../smtp.secure.json"))
    credential = require("../smtp.secure.json");
  else
    console.warn("No email credentials are provided. To send emails create server/smtp.secure.json with credentials.");
}
console.log("credential: ", credential);
const nodemailer = require("nodemailer");

const footer = `
  <br/>
  <div style="font-style: italic;"> <a href="${env.serverHost}/">familystories.page</a> est une application open source créée par 
  <a href="https://equipage.tech">equipage.tech</a> : <a href="https://gitlab.com/beauvaisbruno/familystories">code source</a><br/>
  <a href="${env.serverHost}/"><img src='https://familystories.page/familystories-title.png'/></a><br/>
`;

exports.sendEmail = async ({
                             emails,
                             subject,
                             body,
                             from
                           }: { emails: string, subject: string, body: string, from: string }
) => {
  if (!credential){
    console.warn("Impossible to send emails to :",emails,". To send emails add credentials to the environment variable 'smtp' or to server/smtp.secure.json.");
    return;
  }
  try {
    console.log("email sending... ", {emails, subject, body, from});
    if (emails === "") {
      console.log("no email to send, emails: ", emails);
      return;
    }
    let transporter = nodemailer.createTransport({
      host: credential.server,
      port: credential.port,
      // secure: credential.port === 465,
      auth: {
        user: credential.user,
        pass: credential.password,
      },
    });

    await transporter.sendMail({
      from: `"${from}" <${credential.user}>`,
      subject,
      text: body + footer,
      html: body + footer,
      bcc: emails,
    });
    console.log("email sent ", {emails, subject, body, from});

  } catch (error) {
    console.error("sendEmail error: ", error, ", emails: ", emails, ", subject: ", subject);
  }
};

// exports.sendEmail({
//   emails: "beauvaisbruno@gmail.com", subject: "subject", body: "body",
// }).then(res => console.log("res: ", res));
