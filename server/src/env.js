//@flow
// console.log("process.env: ", process.env);

import path from "path";

let consts = {
  // verifiedUser: {email: "example1@familystories.page"},
  NODE_ENV: process.env.NODE_ENV,
  projectRoot: path.resolve(__dirname,"..",".."),
  serverRoot: path.resolve(__dirname,".."),
  serverHost: "http://localhost",
};

if (process.env.NODE_ENV === "production") //
  consts = {
    ...consts,
    verifiedUser: undefined,
    serverHost: "https://familystories.page"
  };

console.log("consts: ", consts);
console.log("process.env: ", process.env);
export const env = (consts: any);
