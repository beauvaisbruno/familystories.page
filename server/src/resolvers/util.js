//@flow
import path from "path";
import {env} from "../env";
import fs from "fs";

import { GraphQLScalarType } from "graphql";
export function filterId(row:any):any {
  const shallowClone = {...row};
  if (shallowClone._id !== undefined)
    delete shallowClone._id;
  return shallowClone;
}

export async function getString(stream:any):Promise<string> {
  return await new Promise((resolve, reject) => {
    let out = "";
    stream
      .on("data", data => {
        out += data.toString();
        // console.log("data out: ",out);
      })
      .on("end", () => {
          // console.log("finish out: ",out);
          resolve(out);
        }
      )
      .on("error", (error) => {
        console.log("error: ",error);
        reject("read stream failed");
      });
  });
}
export function getUploadFilePath({userId, name, filename}:{userId:string, name:string, filename:string}):string {
  const uploadFolder = path.join(env.serverRoot, "uploads");
  if (!fs.existsSync(uploadFolder)) {
    console.info("create uploadFolder: ", uploadFolder);
    fs.mkdirSync(uploadFolder);
  }
  const userFolder = path.join(uploadFolder, userId + "");
  if (!fs.existsSync(userFolder)) {
    console.info("create userFolder: ", userFolder);
    fs.mkdirSync(userFolder);
  }

  return path.join(userFolder, name + path.extname(filename));
}


export const Void:any = new GraphQLScalarType({
  name: "Void",
  description: "Represents NULL values",

  serialize() {
    return null;
  },

  parseValue() {
    return null;
  },

  parseLiteral() {
    return null;
  }
});


