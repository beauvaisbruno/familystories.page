//@flow

import {db} from "../mongo";
import {ObjectID} from "mongodb";
import type {TheAlbumIds, TheAlbumFields} from "../model/TheAlbum";
import {gql} from "apollo-server-express";
import type {ThePhotoIds} from "../model/ThePhoto";
import path from "path";
import {env} from "../env";
import fs from "fs";
import {sendEmail} from "../emailer";
import type {CookiesType} from "../apolloServer";
import type {TheUserIds} from "../model/TheUser";
import type {Contact} from "../model/TheUser";

export const albumQueries: any = gql`
  type Query {
    getAlbum(_id: String): TheAlbum
    getAllAlbums : [TheAlbum]
  }
  
  type Mutation {
    createAlbum : TheAlbum
    updateAlbumTitle(_id: String!, title: String!) : TheAlbum
    setCouverture(photoId: String!, albumId: String!) : TheAlbum
    deleteAlbum(albumId: String!) : Void
    sendEmail(albumId: String!, contacts: JSON!) : Void
  }
`;

export const albumResolvers = {
  TheAlbum: {
    async owner(album: TheAlbumIds) :any{
      // console.log("TheAlbum owner resolver, album: ",album);
      return await db().collection("TheUser").findOne({_id: ObjectID(album.ownerId)});
    },
    async couverturePhoto(album: TheAlbumIds):any {
      // console.log("TheAlbum couverturePhoto resolver, album: ", album);
      if (album.couverturePhotoId === null)
        return null;
      return await db().collection("ThePhoto").findOne({_id: ObjectID(album.couverturePhotoId)});
    },
    async comments(album: TheAlbumIds):any {
      // console.log("TheAlbum comments resolver, album: ",album);
      const comments = await db().collection("TheComment")
        .find({_id: {$in: album.commentIds.map(id=> ObjectID(id))}})
        .sort({date: 1/*ASC old first*/}).toArray();
      // console.log("comments: ",comments);
      return comments;
    },
    async photos(album: TheAlbumIds):any {
      // console.log("TheAlbum photos resolver, album: ",album);
      const photos = await db().collection("ThePhoto")
        .find({_id: {$in: album.photoIds.map(id=> ObjectID(id))}})
        .sort({addedDate: -1/*DSC recent first*/}).toArray();
      // console.log("photos: ",photos);
      return photos;
    },
  },
  Query: {
    getAlbum: async (_:any, {_id}: {_id:string}, {cookies}: {cookies:CookiesType}):any => {
      const album = await db().collection("TheAlbum").findOne({_id: ObjectID(_id)});
      // console.log("getAlbum seenAlbumIds: ", cookies.seenAlbumIds);
      if (!cookies.seenAlbumIds || !cookies.seenAlbumIds.includes(_id)) {
        album.seenCount++;
        const res = await db().collection("TheAlbum").updateOne(
          {"_id": ObjectID(_id)},
          {$set: album});
        console.log("updateAlbum seenCount, modifiedCount: ", res.modifiedCount, ",  album.seenCount: ", album.seenCount);
      }
      return album;
    },
    getAllAlbums: async (_:any, __:any, {verifiedUser: user}:{verifiedUser:TheUserIds}):any => {
      // console.log("user: ", user);
      //todo use user.albumIds
      const albums = await db().collection("TheAlbum")
        .find({ownerId: user._id.toString()})
        .sort({addedDate: -1/*DSC recent first*/}).toArray();
      console.log("getAllAlbums, albums.length: ", albums.length);
      // console.log("getAllAlbums, albums: ",albums);
      return albums;
    }
  },
  Mutation: {
    createAlbum: async (_:any, __:any, {verifiedUser: user}:{verifiedUser:TheUserIds}):any => {
      const album: TheAlbumFields = {
        ownerId: user._id + "",
        title: "Titre de votre album",
        addedDate: Date.now(),
        couverturePhotoId: null,
        commentIds: [],
        photoIds: [],
        seenCount: 0,
      };

      const res = await db().collection("TheAlbum").insertOne((album));
      console.log("create album: ", album, ", insertedCount: ", res.insertedCount);
      return album;
    },
    updateAlbumTitle: async (_:any, {_id, title}:{_id:string, title:string}, {verifiedUser: user}:{verifiedUser:TheUserIds}):any => {
      console.log("updateAlbumTitle, album: ", {_id, title});
      let album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(_id)});

      if (album.ownerId.toString() !== user._id.toString()) {
        console.error("updateAlbum wrong user, ownerId: ", album.ownerId, ", user._id: ", user._id);
        throw Error("The album is not owned by the user");
      }

      const res = await db().collection("TheAlbum").updateOne(
        {"_id": ObjectID(_id)},
        {$set: {title}});

      console.log("updateAlbum updateOne, modifiedCount: ", res.modifiedCount);
      return {...album, title};
    },
    setCouverture: async (_:any, {albumId, photoId}:{albumId:string, photoId:string}, {verifiedUser: user}:{verifiedUser:TheUserIds}):any => {
      console.log("setCouverture: ", {albumId, photoId});

      let album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      if (album.ownerId.toString() !== user._id.toString()) {
        console.error("setCouverture wrong user, ownerId: ", album.ownerId, ", user._id: ", user._id);
        throw Error("The album is not owned by the user");
      }

      let photo: ThePhotoIds = await db().collection("ThePhoto").findOne({_id: ObjectID(photoId)});
      if (photo.ownerId.toString() !== user._id.toString()) {
        console.error("setCouverture wrong user, ownerId: ", photo.ownerId, ", user._id: ", user._id);
        throw Error("The photo is not owned by the user");
      }

      const res = await db().collection("TheAlbum").updateOne(
        {"_id": ObjectID(albumId)},
        {$set: {couverturePhotoId: photoId}});

      console.log("updateAlbum updateOne, modifiedCount: ", res.modifiedCount);
      return {...album, couverturePhotoId: photoId};
    },
    deleteAlbum: async (_:any, {albumId}:{albumId:string}, {verifiedUser: user}:{verifiedUser:TheUserIds}):any => {
      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      // console.log("updatePhotoDescription photo: ", photo,", album: ",album);

      if (user._id.toString() !== album.ownerId.toString()) {
        console.error("deletePhoto wrong user, user._id: ", user._id, {albumId});
        throw Error("wrong user");
      }

      const resAlbum = await db().collection("TheAlbum").deleteOne({"_id": ObjectID(albumId)});
      console.log("deleteAlbum() album, deletedCount: ", resAlbum.deletedCount);

      const photos = await db().collection("ThePhoto")
        .find({_id: {$in: album.photoIds.map(id=> ObjectID(id))}})
        .sort({addedDate: -1/*DSC recent first*/}).toArray();

      const resPhoto = await db().collection("ThePhoto").deleteMany(
        {"_id": {$in: album.photoIds}});
      console.log("delete photos, deletedCount: ", resPhoto.deletedCount);

      let commentIds = [...album.commentIds];
      let photoUrls = [];
      photos.forEach((photo) => {
        commentIds = [...commentIds, ...photo.commentIds];
        photoUrls.push(photo.url);
      });

      photoUrls.forEach((photoUrl) => {
        const unlinkPath = path.resolve(env.serverRoot, photoUrl);
        console.info("deleteAlbum() unlinkPath: ", unlinkPath);
        fs.unlinkSync(unlinkPath);
      });

      const resAlbumComments = await db().collection("TheComment").deleteMany(
        {"_id": {$in: commentIds}});
      console.log("delete comments, deletedCount: ", resAlbumComments.deletedCount);

      return null;
    },
    sendEmail: async (_:any,
                      {albumId, contacts}:{albumId:string, contacts: Array<Contact>},
                      {verifiedUser: user}:{verifiedUser: TheUserIds})
      : Promise<null>=> {
      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      // console.log("updatePhotoDescription photo: ", photo,", album: ",album);

      let emails = "";
      contacts.forEach((contact) => {
        emails += contact.name + " <"+contact.email+">, ";
      });
      //equivalent to btoa function
      const hash = "#"+Buffer.from(JSON.stringify({name: "OneAlbum", props: {albumId, showComment: true}})).toString("base64");

      await sendEmail({
        from: "Partage Family Stories",
        subject : user.pseudo+" partage l'album "+album.title,
        emails,
        body : ` ${user.pseudo} a créé un album photos sur le site Family Stories
            : <a href="${env.serverHost}/${hash}">${album.title}</a> <br/>`
      });

      return null;
    },
  },
};



