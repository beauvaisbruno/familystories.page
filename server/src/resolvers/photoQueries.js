//@flow

import {gql} from "apollo-server-express";
import {db} from "../mongo";
import {ObjectID} from "mongodb";
import {getVerifiedUser} from "./userQueries";
import path from "path";
import fs from "fs";
import {getUploadFilePath} from "./util";
import type {ThePhotoIds,ThePhotoFields} from "../model/ThePhoto";
import type {TheUser} from "../model/TheUser";
import type {TheAlbumIds} from "../model/TheAlbum";
import {env} from "../env";

export const photoQueries: any = gql`
  type Mutation {
    addPhoto(albumId: String!, file: Upload! ) : TheAlbum
    updatePhotoDescription(_id: String!, description: String! ) : ThePhoto
    deletePhoto(photoId: String!,albumId: String!) : TheAlbum
  }
`;

export const photoResolvers = {
  ThePhoto: {
    async owner(album: TheAlbumIds):any {
      // console.log("ThePhoto owner resolver, album: ", album);
      return await db().collection("TheUser").findOne({_id: ObjectID(album.ownerId)});
    },
    async comments(album: TheAlbumIds):any {
      // console.log("ThePhoto comments resolver, album: ", album);
      const comments = await db().collection("TheComment")
        .find({_id: {$in: album.commentIds.map(id=> ObjectID(id))}})
        .sort({date: 1/*ASC old first*/}).toArray();
      // console.log("comments: ",comments);
      return comments;
    },
  },
  Mutation: {
    deletePhoto: async (_:any, 
      {photoId, albumId}:{photoId:string, albumId:string}, 
      {verifiedUser:user}: {verifiedUser:TheUser}
      ):any => {
      const photo: ThePhotoIds = await db().collection("ThePhoto").findOne({_id: ObjectID(photoId)});
      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      // console.log("updatePhotoDescription photo: ", photo,", album: ",album);

      if (user._id.toString() !== photo.ownerId.toString()
        || user._id.toString() !== album.ownerId.toString()) {
        console.error("deletePhoto wrong user, user._id: ", user._id, {photoId, albumId});
        throw Error("wrong user");
      }
      album.photoIds = album.photoIds.filter(function (value) {
        return value.toString() !== photoId.toString();
      });

      const res = await db().collection("TheAlbum").updateOne(
        {"_id": ObjectID(album._id)},
        {$set: {photoIds: album.photoIds}});
      // console.log("deletePhoto() updateOne TheAlbum, modifiedCount: ", res.modifiedCount);

      const resPhoto = await db().collection("ThePhoto").deleteOne(
        {"_id": ObjectID(photo._id)});
      // console.log("delete photo, deletedCount: ", resPhoto.deletedCount);

      const resComments = await db().collection("TheComment").deleteMany(
        {"_id": {$in: photo.commentIds}});
      console.log("delete comments, deletedCount: ", resComments.deletedCount);

      const unlinkPath = path.resolve(env.serverRoot,photo.url);
      console.info("deletePhoto() unlinkPath: ",unlinkPath);
      fs.unlinkSync(unlinkPath);

      return album;
    },
    updatePhotoDescription: async (_:any, 
      {_id, description}:{_id:string, description:string}, 
      {verifiedUser:user}: {verifiedUser:TheUser}
      ):any => {
      const photo: ThePhotoIds = await db().collection("ThePhoto").findOne({_id: ObjectID(_id)});
      console.log("updatePhotoDescription photo: ", photo);
      if (user._id.toString() !== photo.ownerId.toString()) {
        console.error("updatePhotoDescription wrong user, user._id: ", user._id, ", comment.userId: ", photo.ownerId);
        throw Error("wrong user");
      }

      const res = await db().collection("ThePhoto").updateOne(
        {"_id": ObjectID(photo._id)},
        {$set: {description}});
      ;
      console.log("updatePhotoDescription updateOne, modifiedCount: ", res.modifiedCount);
      return {...photo, description};
    },
    addPhoto: async (_:any, {albumId, file}:{albumId:string, file:any}, {verifiedUser:user}: {verifiedUser:TheUser}):any => {

      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      console.log("addPhoto album: ", album);

      if (user._id.toString() !== album.ownerId.toString()) {
        console.error("addPhoto wrong user, user._id: ", user._id, ", comment.userId: ", album.ownerId);
        throw Error("wrong user");
      }

      const now = Date.now();
      const {createReadStream, mimetype, encoding, filename} = await file;
      // console.log("addPhoto, file: ", filename);
      let stream = createReadStream();
      const filePath = getUploadFilePath({userId: user._id, name: now+"", filename});
      console.info("write photo, filePath: ", filePath);
      await new Promise((resolve, reject) => {
        stream
          .pipe(fs.createWriteStream(filePath))
          .on("finish", () => resolve("success"))
          .on("error", (error) => {
            console.error("Photo Write Stream failed", {error, mimetype, encoding, filename});
            reject("Photo Write Stream failed");
          });
      });

      const photo: ThePhotoFields = {
        description: path.parse(filename).name,
        addedDate: now,
        url: "uploads/" + user._id + "/" + now + path.extname(filename),
        seenCount: 0,
        ownerId: user._id,
        commentIds: []
      };

      const resPhoto = await db().collection("ThePhoto").insertOne(photo);
      console.log("create photo: ", photo, ", insertedCount: ", resPhoto.insertedCount);
      const photoWithId: ThePhotoIds = (photo:any);


      album.photoIds.push(photoWithId._id);
      if (!album.couverturePhotoId)
        album.couverturePhotoId = photoWithId._id;

      const resAlbum = await db().collection("TheAlbum").updateOne(
        {"_id": ObjectID(album._id)},
        {$push: {photoIds: ObjectID(photoWithId._id)}, $set: {couverturePhotoId: ObjectID(album.couverturePhotoId)}});
      // console.log("addPhoto updateOne, modifiedCount: ", resAlbum.modifiedCount);
      console.log("addPhoto return album: ", album);

      return album;
    },
  },
};
