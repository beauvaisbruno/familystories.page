//@flow

import {gql} from "apollo-server-express";
import {db} from "../mongo";
import {ObjectID} from "mongodb";
import type {TheAlbumIds} from "../model/TheAlbum";
import type {TheComment, TheCommentIds, TheCommentFields} from "../model/TheComment";
import type {TheUser} from "../model/TheUser";
import {filterId} from "./util";
import {sendEmail} from "../emailer";
import type {TheUserIds} from "../model/TheUser";
import {env} from "../env";
import type {ThePhotoIds} from "../model/ThePhoto";

export const commentQueries:any = gql`
  type Mutation {
    addAlbumComment(albumId: String!, text: String!): TheAlbum
    deleteAlbumComment(albumId: String!, commentId: String!): TheAlbum
    addPhotoComment(albumId: String!, photoId: String!, text: String!): ThePhoto
    deletePhotoComment(photoId: String!, commentId: String!): ThePhoto
  }
`;

async function getOtherEmails({ownerId, commentIds, userId}) {
  let contactIds = [ownerId.toString()];
  const comments = await db().collection("TheComment")
    .find({_id: {$in: commentIds.map(id => ObjectID(id))}}).toArray();
  console.log("comments: ", comments);

  comments.forEach((comment: TheCommentIds) => {
    if (!contactIds.includes(comment.userId.toString()))
      contactIds.push(comment.userId.toString());
  });

  //filter author
  contactIds = contactIds.filter(id => id.toString() !== userId.toString());

  const contacts: [TheUserIds] = await db().collection("TheUser")
    .find({_id: {$in: contactIds.map(id => ObjectID(id))}}).toArray();
  console.log("contacts: ", contacts);

  let emails = "";
  contacts.forEach((contact) => {
    emails += contact.pseudo + " <" + contact.email + ">, ";
  });
  return emails;
}

export const commentResolvers = {
  TheComment: {
    async user(comment: TheCommentIds):any {
      return await db().collection("TheUser").findOne({_id: ObjectID(comment.userId)});
    },
  },
  Mutation: {
    addAlbumComment: async (_:any, {albumId, text}:{albumId:string, text:string}, {verifiedUser: user} : {verifiedUser:TheUser}):any => {
      let comment: TheCommentFields = {
        date: Date.now(),
        text,
        userId: user._id,
      };
      const resComment = await db().collection("TheComment").insertOne(comment);
      const commentWithId:TheCommentIds = ((comment:any));
      console.log("create comment: ", comment, ", insertedCount: ", resComment.insertedCount);
      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      console.log("comment album: ", album);
      album.commentIds.push(commentWithId._id);
      const resAlbum = await db().collection("TheAlbum").updateOne(
        {"_id": ObjectID(albumId)},
        {$push: {commentIds: ObjectID(commentWithId._id)}});

      console.log("updateAlbum updateOne, modifiedCount: ", resAlbum.modifiedCount);

      //notify others
      let emails = await getOtherEmails({
        ownerId: album.ownerId,
        commentIds: album.commentIds,
        userId: user._id
      });

      //equivalent to btoa function
      const hash = "#" + Buffer.from(JSON.stringify({
        name: "OneAlbum",
        props: {albumId, showComment: true}
      })).toString("base64");

      await sendEmail({
        from: "Commentaire Family Stories",
        subject: user.pseudo + " a commenté l'album " + album.title,
        emails,
        body: ` ${user.pseudo} a commenté l'album photos sur le site Family Stories
            : <a href="${env.serverHost}/${hash}">${album.title}</a> <br/>`
      });

      return album;
    },
    deleteAlbumComment: async (_:any, {albumId, commentId}:{albumId:string, commentId:string}, {verifiedUser: user}: {verifiedUser:TheUser}):any => {

      const comment: TheCommentIds = await db().collection("TheComment").findOne({_id: ObjectID(commentId)});
      console.log("deleteAlbumComment comment: ", comment);

      if (user._id.toString() !== comment.userId.toString()) {
        console.error("deleteAlbumComment wrong user, user._id: ", user._id, ", comment.userId: ", comment.userId);
        throw Error("wrong user");
      }

      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      console.log("deleteAlbumComment album: ", album);
      album.commentIds = album.commentIds.filter(function (value) {
        return value !== commentId;
      });
      const resAlbum = await db().collection("TheAlbum").updateOne(
        {"_id": ObjectID(albumId)},
        {$set: filterId(album)});
      console.log("updateAlbum updateOne, modifiedCount: ", resAlbum.modifiedCount);

      const resComment = await db().collection("TheComment").deleteOne({_id: ObjectID(commentId)});
      console.log("delete comment: ", comment, ", deletedCount: ", resComment.deletedCount);

      return album;
    },
    addPhotoComment: async (_:any, {albumId, photoId, text}:{albumId:string, photoId:string, text:string}, {verifiedUser: user}: {verifiedUser:TheUser}):any => {
      const comment: TheCommentFields = {
        date: Date.now(),
        text,
        userId: user._id,
      };
      const resComment = await db().collection("TheComment").insertOne(comment);
      const commentWithId:TheCommentIds = ((comment:any));
      console.log("create comment: ", comment, ", insertedCount: ", resComment.insertedCount);
      const photo: ThePhotoIds = await db().collection("ThePhoto").findOne({_id: ObjectID(photoId)});
      console.log("comment photo: ", photo);
      photo.commentIds.push(commentWithId._id);
      const resPhoto = await db().collection("ThePhoto").updateOne(
        {"_id": ObjectID(photoId)},
        {$push: {commentIds: ObjectID(commentWithId._id)}});

      console.log("addPhotoComment updateOne, modifiedCount: ", resPhoto.modifiedCount);

      //notify others
      let emails = await getOtherEmails({
        ownerId: photo.ownerId,
        commentIds: photo.commentIds,
        userId: user._id
      });

      //equivalent to btoa function
      const hash = "#" + Buffer.from(JSON.stringify({
        name: "Diaporama",
        props: {photoId,albumId}
      })).toString("base64");

      const album: TheAlbumIds = await db().collection("TheAlbum").findOne({_id: ObjectID(albumId)});
      console.log("addPhotoComment, album: ", album);

      await sendEmail({
        from: "Commentaire Family Stories",
        subject: user.pseudo + " a commenté une photo de l'album " + album.title,
        emails,
        body: ` ${user.pseudo} a commenté une photo sur le site Family Stories
            : <a href="${env.serverHost}/${hash}">${album.title} > ${photo.description}</a> <br/>`
      });

      return photo;
    },
    deletePhotoComment: async (_:any, {photoId, commentId}:{photoId:string, commentId:string}, {verifiedUser: user}: {verifiedUser:TheUser}):any => {

      const comment: TheCommentIds = await db().collection("TheComment").findOne({_id: ObjectID(commentId)});
      console.log("deletePhotoComment comment: ", comment);

      if (user._id.toString() !== comment.userId.toString()) {
        console.error("deletePhotoComment wrong user, user._id: ", user._id, ", comment.userId: ", comment.userId);
        throw Error("wrong user");
      }

      const photo: ThePhotoIds = await db().collection("ThePhoto").findOne({_id: ObjectID(photoId)});
      console.log("deletePhotoComment photo: ", photo);
      photo.commentIds = photo.commentIds.filter(function (value) {
        return value !== commentId;
      });
      const resPhoto = await db().collection("ThePhoto").updateOne(
        {"_id": ObjectID(photoId)},
        {$set: filterId(photo)});
      console.log("updatePhoto updateOne, modifiedCount: ", resPhoto.modifiedCount);

      const resComment = await db().collection("TheComment").deleteOne({_id: ObjectID(commentId)});
      console.log("delete comment: ", comment, ", deletedCount: ", resComment.deletedCount);

      return photo;
    },
  },
};
