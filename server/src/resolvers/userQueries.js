//@flow
import {db} from "../mongo";
import {ObjectID} from "mongodb";
import path from "path";
import fs from "fs";
import type {TheUser, TheUserIds, TheUserFields,ContactList} from "../model/TheUser";
import {gql} from "apollo-server-express";
import {filterId, getString, getUploadFilePath} from "./util";
import type {TheAlbumIds} from "../model/TheAlbum";

export const userQueries:any = gql`
  type Query {
    getUser(email: String): TheUser
  }
  
  type Mutation {
    updateProfile(user: TheUserInput!): TheUser
    updateAvatar (file: Upload!, user: TheUserInput!) : TheUser
    updateContactLists(lists: JSON! ) : TheUser
    importContacts(file: Upload!, listId: String!) : TheUser
  }
`;

export function createUser({email, pseudo}:{email:string, pseudo?:string}): TheUserFields {
  console.info("createUser, email: ", email);
  return {
    email,
    pseudo: pseudo ? pseudo : email.split("@")[0],
    avatar: "default",
    albumIds: [],
    contactLists: {},
  };
}

export function checkUser({context:{verifiedUser}, user}
  :{context:{verifiedUser:TheUserIds},user:TheUserIds}) {
  if (verifiedUser.email !== user.email) {
    console.error("Wrong user, email: ", user.email, ", auth: ", verifiedUser ? verifiedUser.email : "");
    throw Error("Wrong user");
  }
}

export async function getVerifiedUser({verifiedUser}:{verifiedUser:TheUser}): Promise<TheUser> {
  const user:TheUser = await db().collection("TheUser").findOne({email: verifiedUser.email});
  if (user)
    return user;
  throw Error("Unknown user, email: "+ verifiedUser.email);
}

export const checkOrCreateUser = async ({context: {verifiedUser}, email}
  :{context:{verifiedUser:TheUser},email:string})
  : Promise<TheUserIds> => {
  if (verifiedUser.email !== email) {
    console.error("Wrong user, email: ", email, ", auth: ", verifiedUser ? verifiedUser.email : verifiedUser);
    throw Error("Wrong user");
  }
  let user = await db().collection("TheUser").findOne({email});
  if (user) {
    console.log("checkOrCreateUser user: ", user);
    return user;
  }
    user = createUser({email});
    await db().collection("TheUser").insertOne(user);
    const userWithId : TheUserIds = (user:any);
    console.log("checkOrCreateUser create user: ", user);
  
  return userWithId;
};

export const userResolvers = {
  TheUser: {
    async albums(user: TheUserIds, _:any, {verifiedUser}:{verifiedUser:TheUser}):Promise<Array<TheAlbumIds>> {
      if (!verifiedUser || !user.albumIds)
        return [];
      let albums = await db().collection("TheAlbums")
        .find({_id: {$in: user.albumIds.map(id => ObjectID(id))}})
        .sort({date: -1/*DSC recent first*/}).toArray();
      if (verifiedUser._id === user._id)
        return albums;
      albums = albums.filter((album: TheAlbumIds) => {
        return album.ownerId.toString() === verifiedUser._id.toString();
      });
      // console.log("TheUser albums resolver, albums: ",albums);
      return albums;
    },
  },
  Query: {
    getUser: async (_:any, {email}:{email:string}, context:{verifiedUser: TheUser} ):Promise<TheUserIds> => {
      console.log("getUser...");
      const user = checkOrCreateUser({context, email});
      return user;
    },
  },

  Mutation: {
    updateContactLists: async (_:any, {lists}:{lists:{[string] : ContactList}}, {verifiedUser: user}:{verifiedUser:TheUser}):Promise<TheUserIds> => {
      const resUser = await db().collection("TheUser").updateOne(
        {"_id": ObjectID(user._id)},
        {$set: {contactLists: lists}});
      console.log("updateContactLists updateOne, modifiedCount: ", resUser.modifiedCount);

      return await db().collection("TheUser").findOne({_id: ObjectID(user._id)});
    },
    updateAvatar: async (_:any, {file}: {file:any}, {verifiedUser: user}:{verifiedUser:TheUser}):Promise<TheUserIds> => {
      const {createReadStream, mimetype, encoding, filename} = await file;
      console.log("updateAvatar, file: ", file);
      let stream = createReadStream();

      const filePath = getUploadFilePath({userId: user._id, name: "avatar", filename});
      console.info("write avatar, filePath: ", filePath);
      await new Promise((resolve, reject) => {
        stream
          .pipe(fs.createWriteStream(filePath))
          .on("finish", () => resolve("success"))
          .on("error", (error) => {
            console.error("Avatar Write Stream failed", {error, mimetype, encoding, filename});
            reject("Avatar Write Stream failed");
          });
      });
      const res = await db().collection("TheUser").updateOne(
        {"_id": ObjectID(user._id)},
        {$set: {avatar: "uploads/" + user._id + "/avatar" + path.extname(filename)}});
      // console.log("updateAvatar updateOne, modifiedCount: ", res.modifiedCount);
      return await db().collection("TheUser").findOne({email: user.email});
    },
    importContacts: async (_:any, {file, listId}:{file:any, listId:string}, {verifiedUser: user}:{verifiedUser:TheUser}):Promise<TheUserIds> => {
      const {createReadStream, filename} = await file;
      console.log("importContacts, filename: ", filename);
      let content = await getString(createReadStream());

      // console.log("content: ", content);
      const contacts = [];
      const newEmails = [];
      if (path.extname(filename) === ".vcf") {
        const blocks = content.split("END");
        blocks.forEach((block) => {
          const resEmail = block.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
          if (resEmail) {
            newEmails.push(resEmail[0]);
            const line = block.match(/FN:(.*)$/m);
            if (line && line.length >0)
            contacts.push({
              email: resEmail[0],
              name: line[1]
            });
          }
        });
      }
      if (path.extname(filename) === ".csv") {
        const lines = content.split("\n");
        console.log("lines: ", lines);
        lines.forEach((line) => {
          const resEmail = line.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
          if (resEmail) {
            newEmails.push(resEmail[0]);
            contacts.push({
              email: resEmail[0],
              name: line.substring(0, line.indexOf(","))
            });
          }
        });
      }
      console.log("important contacts: ", contacts);
      //merge doublon,
      user.contactLists[listId].contacts.forEach(contact => {
        if (!newEmails.includes(contact.email))
          contacts.push(contact);
      });

      contacts.sort((c1,c2)=> {
        return c1.name.toLowerCase().localeCompare(c2.name.toLowerCase());
      });

      user.contactLists[listId].contacts = contacts;
      const resUser = await db().collection("TheUser").updateOne(
        {"_id": ObjectID(user._id)},
        {$set: {contactLists: user.contactLists}});
      console.log("importContacts updateOne, modifiedCount: ", resUser.modifiedCount);

      return await db().collection("TheUser").findOne({_id: ObjectID(user._id)});
    },
    updateProfile: async (_:any, {user}:{user:TheUserIds}, context:{verifiedUser:TheUserIds}):Promise<TheUserIds> => {
      console.log("updateProfile, user: ", user);
      checkUser({context, user});

      const res = await db().collection("TheUser").updateOne(
        {"_id": ObjectID(user._id)},
        {$set: filterId(user)});
      console.log("updateProfile updateOne, modifiedCount: ", res.modifiedCount);
      return await db().collection("TheUser").findOne({email: user.email});
    },
  }
};


