//@flow

import type {TheUser} from "./TheUser";
import type {TheComment} from "./TheComment";
import type {ThePhoto} from "./ThePhoto";
import {createGqlTypes, getScalarFields} from "./util";
import gql from "graphql-tag";

export type TheAlbumFields = {|
  title: string,
  addedDate: number,
  seenCount: number,
  ownerId: string,
  commentIds: Array<string>,
  photoIds: Array<string>,
  couverturePhotoId: string | null,
|}

export type TheAlbumIds = {|
  ...TheAlbumFields,
  _id: string,
  __typename: string,
|}

export type TheAlbum = {|
  ...TheAlbumIds,
  owner: TheUser,
  comments: Array<TheComment>,
  photos: Array<ThePhoto>,
  couverturePhoto?: ThePhoto,
|}

export const fields = `
    _id: String
    owner: TheUser
    title: String
    addedDate: Long
    seenCount: Int
    commentIds: [String]
    photoIds: [String]
    couverturePhoto: ThePhoto
    comments: [TheComment]
    photos: [ThePhoto]
`;
export const TheAlbumGqls:any = createGqlTypes("TheAlbum",fields);
export const TheAlbumScalars:string = getScalarFields(fields);
