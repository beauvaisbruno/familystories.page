//@flow
import gql from "graphql-tag";

/**
 * Get only the scalar fields (leaf node) without types, e.g. "_id: String" => "_id" Or remove owner: [TheUser]
 * @param gql fields with types
 * @returns string, list of all scalar fields without types
 */
const scalars = ["String", "Int", "Long", "Upload", "JSON"];
export const getScalarFields = (fields:string):string => {
  let out = "\n";
  const lines = fields.split("\n");
  lines.forEach((value) => {
    // match type of the field
    const type = value.match(/ \[{0,1}([^\] ]*)\]{0,1}$/);
    if (type && type[1] && scalars.includes(type[1]))
      out += value.replace(/:.*$/gm, "") + "\n";
  });
  return out + "\n";
};


/**
 * Create :
 * - type with fields (output)
 * - associated input type, e.g. "owner: TheUser" => "owner: TheUserInput"
 * - fragment with all fields without type, e.g. "_id: String" => "_id"
 * @param type, table name, must start by "The"
 * @param fields with types
 * @returns graphql-tag, output and input types
 */
export const createGqlTypes = (type:string, fields: string):any => {
  return gql`
  type ${type} {
    ${fields}
  }

  input ${type}Input {
    ${fields.replace(/(The\w+)/gm, "$1Input")}
  }
  
`;
};
