//@flow
import type {TheUser} from "./TheUser";
import {createGqlTypes, getScalarFields} from "./util";
import gql from "graphql-tag";

export type TheCommentFields = {|
  date: number,
  text: string,
  userId: string,
|}

export type TheCommentIds = {|
  ...TheCommentFields,
  _id: string,
  __typename: string,
|}
export type TheComment = {|
  ...TheCommentIds,
  user: TheUser,
|}

export const fields = `
     _id: String
    user: TheUser
    date: Long
    text: String
`;
export const TheCommentGqls:any = createGqlTypes("TheComment",fields);
export const TheCommentScalars:string = getScalarFields(fields);
