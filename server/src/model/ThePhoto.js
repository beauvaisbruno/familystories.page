//@flow
import type {TheUser} from "./TheUser";
import type {TheComment} from "./TheComment";
import {createGqlTypes, getScalarFields} from "./util";
import gql from "graphql-tag";

export type ThePhotoFields = {|
  description: string,
  url: string,
  addedDate: number,
  seenCount: number,
  ownerId: string,
  commentIds: Array<string>,
|}

export type ThePhotoIds = {|
  ...ThePhotoFields,
  _id: string,
  __typename: string,
|}

export type ThePhoto = {|
  ...ThePhotoIds,
  owner: TheUser,
  comments: Array<TheComment>,
|}

export const fields = `
    _id: String
    description: String
    url: String
    addedDate: Long
    seenCount: Int
    owner: TheUser
    comments: [TheComment]
`;
export const ThePhotoGqls:any = createGqlTypes("ThePhoto",fields);
export const ThePhotoScalars:string = getScalarFields(fields);
