//@flow
import {ApolloServer} from "apollo-server-express";
import {env} from "./env";
import {fire} from "./firebaseAdmin";
import {resolvers, typeDefs} from "./loadResolvers";
import {db} from "./mongo";
import type {TheUserIds} from "./model/TheUser";

export type CookiesType = {
  seenAlbumIds : [string]
}
export type ContextType = {
  verifiedUser?: TheUserIds,
  cookies: CookiesType
}

export const context = async ({req, res}:any): Promise<ContextType> => {
  const authorization = req.headers.authorization;
  const cookies = (req.cookies: CookiesType);
  // console.log("context cookies: ",cookies);

  if (!authorization) {
    if (env.verifiedUser)
      return {verifiedUser: env.verifiedUser, cookies};
    return {cookies};
  }
  const token = authorization.split("Bearer ")[1];
  // console.log("token: ",token);
  const decodedIdToken = await fire.auth().verifyIdToken(token);
  // console.log("decodedIdToken: ",decodedIdToken);

  const verifiedUser = (await fire.auth().getUser(decodedIdToken.uid):TheUserIds);
  // console.log("verifiedUser: ",verifiedUser);
  const user = await db().collection("TheUser").findOne({email: verifiedUser.email});
  // console.log("user: ",user);
  return {verifiedUser: {...verifiedUser, ...user}, cookies};
};

export const apolloServer:any = new ApolloServer({
  //https://www.apollographql.com/docs/apollo-server/data/file-uploads/#uploads-in-node-14-and-later
  uploads: false,
  context,
  typeDefs,
  resolvers,
  formatError: (error) => {
    console.error("apolloServer error: ", error);
    if (error && error.extensions && error.extensions.exception)
      console.error("stacktrace: ", error.extensions.exception.stacktrace);
    return error;
  }
});

