#!/bin/bash
set -x #echo on

source /home/ficheski/nodevenv/public_html/realityquest.city/10/bin/activate
cd /home/ficheski/public_html/realityquest.city
mv app.js appBack.js
pkill Passenger
npm run-script build
mv appBack.js app.js
touch /home/ficheski/familystories.page/tmp/restart.txt
