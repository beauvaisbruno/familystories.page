
export type TheUser = {|
  email: string, //id
  name: string,
  avatar: string, //default avatar + uploaded avatars
  contacts: Array<TheUser>,
  contactLists: Array<TheContactList>,
  ownedCollections: Array<TheAlbum>,
  sharedCollections: Array<ThePhoto>,
|}

export type TheContactList = {|
  id: string,
  name: string,
  contacts: Array<TheUser>,
|}

export type TheReaction = {|
  id: string, //id
  userId: TheUser,
  emoji: string,
  photo?: ThePhoto,
  album?: TheAlbum,
|}

export type TheComment = {|
  id: string, //id
  user: TheUser,
  date: number,
  photo?: ThePhoto,
  album?: TheAlbum,
  reactions: Array<TheReaction>,
|}

export type TheAlbum = {|
  id: string,
  owner: TheUser,
  name: string,
  description: string,
  addedDate: number,
  comments: Array<TheComment>,
  photos: Array<ThePhoto>,
  reactions: Array<TheReaction>,
  sharedUsers: Array<TheUser>,
  seenUsers: Array<TheUser>,
  seenCount: number,
|}

export type ThePhoto = {|
  id: string, //hash => use as path
  owner: TheUser,
  description: string,
  collection: TheAlbum,
  addedDate: number,
  comments: Array<TheComment>,
  reactions: Array<TheReaction>,
  seenUsers: Array<TheUser>,
  seenCount: number,
|}


