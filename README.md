![react native](./asset/prototype/auth/title.png)

High velocity setup for [devops](https://www.atlassian.com/devops) teams. 
Component & End-to-end tests driven. CI pipeline artefacts : [code coverage](https://beauvaisbruno.gitlab.io/familystories.page/coverage/), 
[responsive screenshots & videos](https://beauvaisbruno.gitlab.io/familystories.page/screen_captures/), 
[storybook components](https://beauvaisbruno.gitlab.io/familystories.page/storybook/). 

[![pipeline status](https://gitlab.com/beauvaisbruno/familystories.page/badges/master/pipeline.svg)](https://gitlab.com/beauvaisbruno/familystories.page/-/commits/master)
[![coverage report](https://gitlab.com/beauvaisbruno/familystories.page/badges/master/coverage.svg)](https://gitlab.com/beauvaisbruno/familystories.page/-/commits/master)

## Technologies

- [React](https://facebook.github.io/react-native/) to build a declarative, efficient, and flexible user interfaces.
- [ECMAScript 6](http://es6-features.org/),
  [Node.js](https://nodejs.org/en/) and [Flow types](https://flow.org/) to code fast and safely
- [Storybook](https://storybook.js.org/) to build bulletproof UI (isolation & specification)
- [Jest](https://jestjs.io/) & [Puppeteer](https://github.com/puppeteer/puppeteer) to test components 
- [Apollo GraphQL](https://www.apollographql.com/) for state management and to connect modern apps using an industry-standard data layer.
- [Firebase Authentication](https://firebase.google.com/docs/auth) to secure authentification flow 
- [Mongodb](https://www.mongodb.com/why-use-mongodb) a scalable and fast noSQL database
- [Cypress](https://www.cypress.io/) for end-to-end tests
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/introduction/) for [Continuous integration & delivery](https://www.atlassian.com/continuous-delivery)
  
![react](./asset/doc/img/react.png)
![flow](./asset/doc/img/flow.png)
![storybook](./asset/doc/img/storybook.png)
![puppeteer](./asset/doc/img/puppeteer.png)
![Apollo GraphQL](./asset/doc/img/graphql.png)
![Firebase](./asset/doc/img/firebase.png)
![Mongodb](./asset/doc/img/mongodb.png)
![Gitlab](./asset/doc/img/gitlab.png)

### Setup to dev
*You’ll need to have Node >= 10 on your local development machine : guide for [macOS/Linux](https://github.com/nvm-sh/nvm#installation)
or [windows](https://github.com/coreybutler/nvm-windows#node-version-manager-nvm-for-windows). 
You have also to install [yarn](https://classic.yarnpkg.com/en/docs/install/).*

Run `yarn install`

Run `cd ./server && yarn install`

Install [mangodb community edition](https://www.mongodb.com/try/download/community) 
and [MongoDB Database Tools](https://www.mongodb.com/try/download/database-tools?tck=docs_databasetools)

Add MongoDB client (Compass) and MongoDB tools to your OS path.

You may also install [Altair](https://altair.sirmuel.design/#download) a graphQL client more complete than the default playground. 

Create firebase project and add server/firebaseKey.secure.json ([guide](https://firebase.google.com/docs/web/setup)) for authentication flow.

Add "localhost" as authorised domains : Firebase console Auth section -> Sign in method -> Authorised domains

(Optional) To send emails : add credential of an email account to server/smtp.secure.json

Run `nps workflow.dev_with_storybook` for component-driven development

Run `nps workflow.snapshot_tests` to run component tests and `nps workflow.snapshot_update` to update snapshots. 

Run `nps workflow.dev_with_cypress` for e2e-test-driven development and `nps workflow.e2e_tests` to run all end-to-end tests 

Run `nps help` to see all [available scripts](scripts/scripts.md)

### Setup Gitlab CI
Create a gitlab project.

Run `nps server.encode_secure` and add secure environment variables to the process running your CI jobs ([guide](https://docs.gitlab.com/ee/ci/variables/#project-cicd-variables)).

You may need to install [docker desktop](https://www.docker.com/products/docker-desktop) and install a gitlab runner : 
`nps docker.create_docker && nps docker.register_gitlab_runner`. ([guide](https://docs.gitlab.com/runner/install/docker.html)) 

To add coverage badge, on Gitlab > Settings > CI/CD > General pipelines > Test coverage parsing field : `All files[^|]*\|[^|]*\s+([\d\.]+)` 

### Setup deployment

Choose a provider supporting nodeJs app and add scripts/ssh.secure.json to the local copy of your project.

Create a Mongo Atlas project and add server/mongodb.secure.json ([guide](https://docs.atlas.mongodb.com/getting-started/)) to the local copy of your project.

Run `nps server.encode_secure` and add secure environment variables to the process running your server (hosting provider dependent).

To let the CI script to manage git version tags, add a ssh key ([guide](https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/)) 

### [Gitlab Workflow](https://about.gitlab.com/blog/2016/10/25/gitlab-workflow-an-overview/)

![Gitlab Workflow](asset/doc/img/workflow.png)

- Maintainer
  - Create issue with the story to implement 
  - Eventually add a link to a google drawing
  - Create a merge request from the issue  => auto create a dedicated branch
- Developer
  - Checkout the dedicated branch
  - Implement the story and push commits => trigger tests on Gitlab
  - Eventually rebase, resolve conflicts, clean commit history
  - On label board move the issue on waiting for review
- Reviewer
  - Eventually annotate codes and send feedback
  - Approve and move the issue on label board to reviewed
- Maintainer
  - Merge to master
  - push release version tag => trigger tests and deploy
