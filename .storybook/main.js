module.exports = {
  "stories": [
    "../src/**/*.stories.js",
    //to work with one file uncomment below
    // "../src/screens/contacts/*.stories.js"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/preset-create-react-app"
  ]
}
