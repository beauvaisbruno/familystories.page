import { addons } from '@storybook/addons';

addons.setConfig({
  isFullscreen: true,
  showNav: true,
  showPanel: true,
  panelPosition: 'bottom',
  sidebarAnimations: false,
  enableShortcuts: true,
  isToolshown: true,
  theme: undefined,
  selectedPanel: undefined,
  initialActive: 'sidebar',
  sidebar: { showRoots: true },
});
