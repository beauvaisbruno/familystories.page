// flow-typed signature: d59a7fdc11c347dccdf6abc8700548f9
// flow-typed version: <<STUB>>/graphql-type-json_v^0.3.2/flow_v0.149.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'graphql-type-json'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'graphql-type-json' {
  declare module.exports: any;
}
