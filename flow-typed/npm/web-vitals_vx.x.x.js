// flow-typed signature: c702e8bb256a2450976e69895ed41130
// flow-typed version: <<STUB>>/web-vitals_v^1.0.1/flow_v0.149.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'web-vitals'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'web-vitals' {
  declare module.exports: any;
}
