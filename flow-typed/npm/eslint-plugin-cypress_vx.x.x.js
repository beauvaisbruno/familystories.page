// flow-typed signature: 40dcacef529d67d65408ce8a0f2b4eb3
// flow-typed version: <<STUB>>/eslint-plugin-cypress_v^2.11.2/flow_v0.149.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'eslint-plugin-cypress'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'eslint-plugin-cypress' {
  declare module.exports: any;
}
