// flow-typed signature: c3247097cc794c97c97b71b1218dd38a
// flow-typed version: <<STUB>>/mock-apollo-client_v^1.0.0/flow_v0.149.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'mock-apollo-client'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'mock-apollo-client' {
  declare module.exports: any;
}
