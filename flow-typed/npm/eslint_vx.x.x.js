// flow-typed signature: 4afd9bda9f6e4ab9a6d4587ea295fdaa
// flow-typed version: <<STUB>>/eslint_v^7.23.0/flow_v0.149.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'eslint'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'eslint' {
  declare module.exports: any;
}
