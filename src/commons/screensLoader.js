//@flow
import type { Node } from "react";
import { Auth } from "../screens/auth/Auth";
import { SplashScreen } from "../screens/splashScreen/SplashScreen";
import {Profile} from "../screens/profile/Profile";
import {MyAlbums} from "../screens/myAlbums/MyAlbums";
import {OneAlbum} from "../screens/oneAlbum/OneAlbum";
import {Contacts} from "../screens/contacts/Contacts";
import {Diaporama} from "../screens/diaporama/Diaporama";

// eslint-disable-next-line no-use-before-define
export const loadScreens: () => { [string]: (any)=>Node } = () => {
  return {
    SplashScreen,
    Auth,
    Profile,
    MyAlbums,
    OneAlbum,
    Contacts,
    Diaporama,
  };
};


