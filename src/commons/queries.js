//@flow
import {gql} from "@apollo/client";
import {TheUserScalars} from "../model/TheUser";
import {ThePhotoScalars} from "../model/ThePhoto";
import {TheCommentScalars} from "../model/TheComment";

export const getUserGql: any = gql`
  query ($email: String!) {
    getUser(email: $email) {
      ${TheUserScalars}
    }
  }
`;

export const photoResponse = `
  ${ThePhotoScalars}
  comments {
    ${TheCommentScalars}
    user {
      ${TheUserScalars}
    }
  }
`;

export const deletePhotoCommentGql: any = gql`
  mutation deletePhotoComment($photoId: String!, $commentId: String! ) {
     deletePhotoComment(photoId: $photoId, commentId: $commentId) {
       ${photoResponse}
    }
  }
`;

export const addPhotoCommentGql: any = gql`
  mutation addPhotoComment($albumId: String!, $photoId: String!, $text: String! ) {
    addPhotoComment(albumId: $albumId, photoId: $photoId, text: $text) {
      ${photoResponse}
    }
  }
`;
