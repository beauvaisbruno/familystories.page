//@flow
import {bake_cookie, read_cookie} from "sfcookies";

export const updateSeenAlbum = (albumId:string) => {
  let seenAlbumIds = read_cookie("seenAlbumIds");
  if (!seenAlbumIds)
    seenAlbumIds = [];
  if (!seenAlbumIds.includes(albumId))
    seenAlbumIds.push(albumId);
  bake_cookie("seenAlbumIds", seenAlbumIds);
};
