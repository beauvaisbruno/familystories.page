// console.log("process.env: ", process.env);

let consts = {
  version: "1",
  NODE_ENV: process.env.NODE_ENV,
  STORYBOOK: process.env.STORYBOOK,
  serverUrl: process.env.NODE_ENV==="production" ? "" : "http://"+window.location.host+":8080/",
  // disableCypressMobile: false,
  // fillForm: true,
  // welcomeScreen: "Contacts",
};

if (process.env.STORYBOOK)
  consts = {
    ...consts,
    fillForm: true,
    serverUrl: ""
  };

console.log("consts: ", consts);
export const env = consts;
