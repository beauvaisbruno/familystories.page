//@flow
import {ApolloClient, ApolloLink, createHttpLink, HttpLink, InMemoryCache} from "@apollo/client";
import {setContext} from "@apollo/client/link/context";
import {auth} from "./firebaseInit";
import {env} from "./env";
import { createUploadLink } from "apollo-upload-client";
import { getMainDefinition } from "apollo-utilities";
import omitDeep from "omit-deep-lodash";

const httpLink = createUploadLink({
  uri: env.serverUrl +  "graphql",
  credentials: "include",
});

const authLink = setContext(async (_, {headers}) => {
  const user = auth().currentUser;
  // console.log("headers: ",headers);
  // console.log("setContext done, user: ",user);
  if (!user)
    return {headers};
  const token = await user.getIdToken();
  // console.log("token; ",token);
  let options = {
      headers: {
        ...headers,
        authorization:"Bearer "+token,
      }
  };
  // console.log("setContext options: ",options);
  return options;
});

//https://github.com/apollographql/apollo-client/issues/1564
const cleanTypeName = new ApolloLink((operation, forward) => {
  const def = getMainDefinition(operation.query);
  if (def && def.operation === "mutation") {
    operation.variables = omitDeep(operation.variables, ["__typename"]);
  }
  return forward ? forward(operation) : null;
});

export const apolloClient: any = new ApolloClient({
  link: ApolloLink.from([
    cleanTypeName,
    authLink,
    httpLink,
  ]),
  cache: new InMemoryCache({
    // addTypename: false
  }),
});


