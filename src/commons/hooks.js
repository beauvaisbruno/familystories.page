//@flow

import React, {useRef, useState} from "react";
import {colors} from "./css";
import type {Node} from "react";

/**
 * Merge loading and error states and return a Comp to display a message or a spinner
 * @returns {{Loader: Node, setLoading, setError, loading, error}}
 */

type useLoaderType =  {
  Loader: (any)=>Node,
    loading:boolean | string,
    error:boolean | string,
    setLoading: ( boolean | string) => void,
    setError: ( boolean | string) => void,
}

export const useLoader = ():useLoaderType => {

  const [loading: boolean | string, setLoading] = useState(false);
  const [error: boolean | string, setError] = useState(false);

  const Error = ({style}: { style?: any }): Node => {
    let text = "Operation impossible :(";
    if (typeof error === "string") {
      text = error;
    }
    return <div {...{
      style: {
        marginBottom: "1vw",
        ...(style ? style : ({}: any)),
        color: colors.errorMsg
      }
    }}>
      {text}
    </div>;
  };
  const Loading = ({style}: { style?: any }): Node => {
    // let text = "Chargement...";
    let text;
    if (typeof loading === "string") {
      text = loading;
    }
    return <div
      {...(style ? style : ({}: any))}
    >
      <img
        style={{alignSelf: "center"}}
        width={"30vw"}
        src={require("./img/loading.gif").default}/>
      {text && <div style={{fontSize: "5vw", textAlign: "center"}}>{text}</div>}
    </div>;
  };

  const selectComp = (): (any) =>Node => {
    if (error) return Error;
    if (loading) return Loading;
    return () => null;
  };

  return {
    Loader: selectComp(),
    loading,
    error,
    setLoading: (value: boolean | string) => {
      setLoading(value);
      if (value) setError(false);
    }, setError: (value: boolean | string) => {
      setError(value);
      if (value) setLoading(false);
    },
  };
};

export function useUpdate():()=>void {
  const update = useState()[1];
  return () => {
    update(Date.now());
  };
}

export const useFocus = () : [{current: any}, ()=>void] => {
  const htmlElRef = useRef();
  const setFocus = () => {
    htmlElRef.current && htmlElRef.current.focus();
  };

  return [htmlElRef, setFocus];
};
