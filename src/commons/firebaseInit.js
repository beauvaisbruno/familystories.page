//@flow
// import * as firebaseRoot from "firebase";
import app from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyCsOG3HCAPuEEVeHQKoB81uZ5LOlIMszZc",
  authDomain: "family-stories.firebaseapp.com",
  projectId: "family-stories",
  storageBucket: "family-stories.appspot.com",
  messagingSenderId: "519290456343",
  appId: "1:519290456343:web:45e31f9afdcf3893a8cf52",
  measurementId: "G-DGE3VL6NGJ"
};

app.initializeApp(firebaseConfig);

export const auth = app.auth;
export const fire = app;



