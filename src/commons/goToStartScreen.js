//@flow
import {env} from "./env";
import {bake_cookie, read_cookie} from "sfcookies";
import type {TheUser} from "../model/TheUser";

export const goToStartScreen = (
  {setScreen, user}: { setScreen: (string, any)=>void, user?: TheUser }) => {
  console.log("goToStartScreen, user: ", user);
  if (env.welcomeScreen) {
    setScreen(env.welcomeScreen);
    return;
  }
  if (window.location.hash) {
    //Hash is used to direct access page and share album or photo
    try {
      console.log("goToStartScreen hash : ", atob(window.location.hash.substring(1)));
      const screen = JSON.parse(atob(window.location.hash.substring(1)));
      console.log("screen props: ", screen.props);
      if (screen.name !== "Auth" && screen.name !== "SplashScreen") {
        setScreen(screen.name, screen.props);
        return;
      }
    } catch (error) {
      console.error("error: ", error);
    }
  }
  if (!user) {
    setScreen("Auth");
    return;
  }
  const first = read_cookie("first");
  console.log("first: ", first);
  if (first !== true) {
    bake_cookie("first", true);
    setScreen("Profile");
    return;
  }

  //TODO go to news by default
  setScreen("MyAlbums");
};
