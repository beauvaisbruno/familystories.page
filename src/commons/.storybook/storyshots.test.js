//@flow
import puppeteer from "puppeteer";
import path from "path";
import diff from "jest-diff";
import glob from "glob";
import pretty from "pretty";
import fs from "fs";
import config from "../../../.storybook/main.js";
import {camelToDash} from "../util";

let page;
let browser;
let files;
files = glob.sync(config.stories[0], {cwd: path.resolve(__dirname, "../../../.storybook/")});

beforeAll(async () => {
  console.log("files: ", files);
  browser = await puppeteer.launch({args: ["--disable-extensions", "--no-sandbox", "--disable-setuid-sandbox"]});
  page = await browser.newPage();
});

afterAll(async () => {
  await browser.close();
});

const devices = [{
  name: "desktop",
  userAgent: "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36",
  viewport: {
    width: 1920,
    height: 1080
  }
},
  {
    name: "mobile", //iphone 6
    userAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1",
    viewport: {
      width: 375,
      height: 667,
    }
  }
];

expect.extend({
  toMatchHtml(received, expected) {
    if (expected === received)
      return {
        message: () => diff(received, expected),
        pass: true
      };
    else
      return {
        message: () => diff(received, expected),
        pass: false
      };
  }
});

const port = process.env.CI ? 9009 : 80;
jest.setTimeout(20000);
console.log("storybook port: ", port);
files.forEach((file) => {

  file = path.resolve(__dirname, "..", "..", file);
  console.log("file: ", file);
  //$FlowIgnore
  const exports = require(file);
  const comp = exports.default.title.toLowerCase();
  describe(comp, () => {
    const stories = Object.keys(exports).filter((key) => key !== "default");
    stories.forEach((story) => {
      devices.forEach((device) => {
        // console.log(file,"\n",comp,story,device.name);
        it(story + "-" + device.name, async () => {

          await page.emulate(device);
          const url = "http://localhost:" + port + "/iframe.html?id=" + comp + "--" + camelToDash(story) + "&viewMode=story&fullScreen=1";
          const baseDir = path.resolve(__dirname, path.dirname(file), "__snapshots__");
          const fileName = comp + "-" + story + "-" + device.name;
          let baseName = path.join(baseDir, fileName);
          let snapshotPath = baseName + ".html";

          console.log("url: ", url, "=>", snapshotPath);
          await page.goto(url);

          let html = pretty(await page.$eval("#root", (element) => {
            return element.outerHTML;
          }));
          //remove webpack hash
          html = html.replace(/"[^"]*\.([^".]*\.)[^".]*[^"]*"/gm, function (str, hash) {
            return str.replace(hash, "");
          });
          html = html.replace(/(src="data[^;]*;base64,.{0,4})[^"]*"/gm, "$1\"");
          // for cross platform support. Git must not convert the end of line.
          html = html.replace(/\r\n|\n/gm, "\n");

          if (!fs.existsSync(baseDir))
            fs.mkdirSync(baseDir);
          // const writeStories = !fs.existsSync(snapshotPath) || process.env.STORY_UPDATE;
          if (process.env.STORY_UPDATE) {
            console.info("write snapshot: ", snapshotPath);
            fs.writeFileSync(snapshotPath, html);
            await page.screenshot({path: baseName + ".png"});
          } else {
            // console.log("snapshotPath: ",snapshotPath);
            let expected = fs.readFileSync(snapshotPath).toString("utf8");
            //$FlowIgnore
            expect(html).toMatchHtml(expected);
            //TODO use docker(cypress/included:6.8.0) to render screenshot so they are consistent between platform
            // expect(baseName + ".png").toMatchImageSnapshot();
          }
        });
      });
    });
  });
});
