import initStoryshots, {Stories2SnapsConverter} from '@storybook/addon-storyshots';
import {configure, mount} from 'enzyme';
import toJson from 'enzyme-to-json';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import path from "path";

configure({adapter: new Adapter()});

// TODO fix act error message: [..]was not wrapped in act(...)
const prevError = console.error;
console.error = (...args) => {
  if (args[0] && args[0].includes("was not wrapped in act(...)"))
    return;
  prevError.error(...args);
}

initStoryshots({
  configPath: './src/commons/.storybook',
  asyncJest: true,
  test: async ({
           story,
           context,
           done,
         }) => {
    const converter = new Stories2SnapsConverter();
    const snapshotFilename = path.resolve("src",converter.getSnapshotFileName(context));
    const storyElement = story.render();

    let tree = mount(storyElement)
    setTimeout(() => {
      expect(toJson(tree.update())).toMatchSpecificSnapshot(snapshotFilename);
      done();
    }, 1);
  }
});
