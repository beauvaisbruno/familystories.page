//Screenshots diff disabled : TODO run storyshots using the same docker as the CI server
// see https://storybook.js.org/addons/@spring-media/storybook-addon-image-snapshots

import path from "path";
import initStoryshots from '@storybook/addon-storyshots';
import {imageSnapshot} from '@storybook/addon-storyshots-puppeteer';

const getMatchOptions = ({context: {fileName}}) => {
  let snapshotPath = path.join(__dirname, "../../", path.dirname(fileName), "__snapshots__");
  console.log("snapshotPath: ", snapshotPath);
  return {customSnapshotsDir: snapshotPath};
};

//see node_modules/puppeteer/lib/esm/puppeteer/common/DeviceDescriptors.js
const devices = [
  {
    name: 'desktop',
    userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36',
    viewport: {
      width: 1920,
      height: 1080
    }
  },
  {
    //iphone 6
    name: 'mobile',
    userAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
    viewport: {
      width: 375,
      height: 667,
      deviceScaleFactor: 2,
      isMobile: true,
      hasTouch: true,
      isLandscape: false,
    },
  }];

let storybookUrl = "http://localhost";
if (process.env.REACT_APP_CI)
  storybookUrl = "file://" + path.join(__dirname, "build");
// storybookUrl ="file:///opt/storybook-static";
console.log("storybookUrl: ", storybookUrl);


devices.forEach((device) => {
  const customizePage = (page) => {
    return page.emulate(device);
  }

  initStoryshots({
    configPath: './src/commons/.storybook',
    suite: device.name,
    test: imageSnapshot({
      storybookUrl,
      customizePage,
      getMatchOptions
    }),
  });

});

