//@flow
import type {ContactList, TheUser} from "../../../model/TheUser";
import {pickRandomFromArray, pseudoRndId, pseudoRndInt} from "./mockUtils";

const pseudos = ["Gérard", "Erwan", "Noha", "Odile", "Julie", "Lea"];
const names = ["Famille", "Amis", "Collègue", "Tennis Club", "École", "Cousins"];

const avatars = [
  "default",
  require("./imgs/avatar0.jpg").default,
  require("./imgs/avatar1.jpg").default,
  require("./imgs/avatar2.jpg").default,
  require("./imgs/avatar3.jpg").default,
  require("./imgs/avatar4.jpg").default,
  require("./imgs/avatar5.jpg").default,
];

export const mockContactList = ():ContactList => {
  const contacts = [];
  const  nbr = 5+pseudoRndInt(20);
  for (let i = 0; i < nbr; i++) {
    contacts.push({
      name: pickRandomFromArray(pseudos),
      email: pickRandomFromArray(pseudos).toLowerCase()+ "@familystory.page",
    });
  }
  return {
    name: pickRandomFromArray(names),
    contacts,
  };
};

export const mockContactLists = () :{[string] : ContactList} => {
  const lists = {};
  for (let i = 0; i < 3; i++) {
    lists[pseudoRndId()] = mockContactList();
  }
  return lists;
};

export const mockUser = ({email = pseudoRndId() + "@familystory.page"}: { email: string }): TheUser => {
  const user = {
    _id: "user" + pseudoRndId(),
    __typename: "TheUser",
    email,
    pseudo: pickRandomFromArray(pseudos),
    avatar: pickRandomFromArray(avatars),
    albumIds: [],
    albums: [],
    contactLists: mockContactLists(),
  };
  return user;
};

export const mockUsers  = ({nbr}: { nbr: number }) : Array<TheUser> => {
  const users = [];
  for (let i = 0; i < nbr; i++) {
    users.push(mockUser({}));
  }
  return users;
};
