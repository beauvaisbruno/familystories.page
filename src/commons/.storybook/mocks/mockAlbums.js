//@flow
import type {TheAlbum} from "../../../model/TheAlbum";
import type {TheUser} from "../../../model/TheUser";
import {getIds, pickRandomFromArray, pseudoRndId, pseudoRndInt, rndDate} from "./mockUtils";
import {mockComments} from "./mockComments";
import {mockPhotos} from "./mockPhotos";

const titles = ["Votre title d'album", "Vacances à la plage", "Noël en famille"];

export const mockAlbum = ({owner, users = [], photoNbr = pseudoRndInt(5), commentNbr = pseudoRndInt(2)}
                            : { owner: TheUser, users?: Array<TheUser>, photoNbr?: number, commentNbr?: number }): TheAlbum => {
  const photos = mockPhotos({owner, users, nbr: photoNbr});
  const comments = mockComments({users: [...users, owner], nbr: commentNbr});
  const couverturePhoto = photos.length===0 ? null : pickRandomFromArray(photos);
  const album: TheAlbum = {
    _id: "album" + pseudoRndId(),
    __typename: "TheAlbum",
    title: pickRandomFromArray(titles),
    addedDate: rndDate(),
    seenCount: pseudoRndInt(15),
    ownerId: owner._id,
    owner,
    comments: comments,
    commentIds: getIds(comments),
    photos,
    photoIds: getIds(photos),
    couverturePhotoId: couverturePhoto ? couverturePhoto._id : null,
    couverturePhoto
  };
  return album;
};

export const mockAlbums = ({owner, users, nbr}: { owner: TheUser, users: Array<TheUser>, nbr: number }): Array<TheAlbum> => {
  const albums = [];
  for (let i = 0; i < nbr; i++) {
    const album = mockAlbum({owner, users});
    albums.push(album);
    owner.albums.push(album);
  }
  return albums;
};
