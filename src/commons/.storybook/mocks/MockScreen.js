//@flow
import {createMockClient} from "mock-apollo-client";
import React from "react";
import type {Node} from "react";
import {createScreenContext, ScreenContext, ScreenNavigator} from "../../components/ScreenNavigator";
import {LoggedUserContext} from "../../components/AuthLoader";
import {ApolloProvider} from "@apollo/client";
import {loadScreens} from "../../screensLoader";
import gql from "graphql-tag";
import type {TheUser} from "../../../model/TheUser";

export type RemotesType = {
  query: any,
  data: { [string]: any }
};
export const MockScreen = (
  {
    user,
    screen = "SplashScreen",
    screenProps = {},
    remotes = [],
  }: {
    user?: TheUser,
    screen: string,
    screenProps?: any,
    remotes? : Array<RemotesType>,
  }):  Node => {
  const mockClient = createMockClient();
  remotes.forEach((obj) => {
    mockClient.setRequestHandler(
      obj.query,
      () => {
        return Promise.resolve({data: {...obj.data}});
      });
  });
  // console.log("MockScreen user: ",user);
  console.log("MockScreen, screen: ", screen);
  return (
    <ApolloProvider client={mockClient}>
      <ScreenContext.Provider
        value={createScreenContext({
          name: screen,
          props: screenProps
        }, (screen, screenProps) => console.log("setScreen not available, screen: ", screen))}>
        <LoggedUserContext.Provider value={{user, setUser: (user) => console.log("setUser not implemented: ", user)}}>
          <ScreenNavigator {...{
            screens: loadScreens(),
          }} />
        </LoggedUserContext.Provider>
      </ScreenContext.Provider>
    </ApolloProvider>
  );
};
