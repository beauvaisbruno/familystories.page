//@flow
import type {TheUser} from "../../../model/TheUser";
import {getIds, pickRandomFromArray, pseudoRndId, pseudoRndInt, rndDate} from "./mockUtils";
import type {ThePhoto} from "../../../model/ThePhoto";
import type {TheComment} from "../../../model/TheComment";
import {mockComments} from "./mockComments";

const descriptions = ["Tous a la plage", "Noël en famille", "Les mariés"];
// const urls = ["mocks/beach.jpg","mocks/christmas.jpg","mocks/married.png","mocks/picnic.jpg"];
const urls = [
  require("./imgs/beach.jpg").default,
  require("./imgs/christmas.jpg").default,
  require("./imgs/married.png").default,
  require("./imgs/picnic.jpg").default,
];

export const mockPhotos= ({owner, users, nbr}: { owner: TheUser, users: Array<TheUser>, nbr:number }): Array<ThePhoto>  => {
  const photos = [];
  for (let i = 0; i < nbr; i++) {
    const comments = mockComments({users: [...users, owner]});
    photos.push({
      _id: "photo" + pseudoRndId(),
      __typename: "ThePhoto",
      description: pickRandomFromArray(descriptions),
      url:  pickRandomFromArray(urls),
      addedDate: rndDate(),
      seenCount: pseudoRndInt(15),
      ownerId: owner._id,
      owner,
      comments,
      commentIds:getIds(comments),
    });
  }
  return photos;
};
