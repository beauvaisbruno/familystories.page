//@flow
import type {TheUser} from "../../../model/TheUser";
import {pickRandomFromArray, pseudoRndId, pseudoRndInt, rndDate} from "./mockUtils";
import type {TheComment} from "../../../model/TheComment";

const texts = [
  "Qui se trouve sur la photo ?",
  "Photo prise le 5 juillet 2021 avec toute la famille !",
  "Je me souviens de cette belle journée :)",
];

export const mockComments = ({users, nbr=pseudoRndInt(2)}: { users: Array<TheUser>, nbr?:number }): Array<TheComment> => {
  const comments = [];
  for (let i = 0; i < nbr; i++) {
    const user = pickRandomFromArray(users);
    comments.push({
      _id: "comment" + pseudoRndId(),
      __typename: "TheComment",
      text: pickRandomFromArray(texts),
      userId: user._id,
      user,
      date:  rndDate(),
    })
  }
  return comments;
};
