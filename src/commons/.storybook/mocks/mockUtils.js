//@flow

let m_w = 123456789;
let m_z = 987654321;

export function pseudoRndReset (seed: number = 0) {
  m_w = 123456789 + seed;
  m_z = 987654321 - seed;
}

/**
 * Create a pseudo aleatoire number. So number series are equals between call sequence.
 * @returns {number}
 */

//https://en.wikipedia.org/wiki/Multiply-with-carry_pseudorandom_number_generator
export function pseudoRnd (): number {
  m_z = (36969 * (m_z & 65535) + (m_z >> 16)) & 0xffffffff;
  m_w = (18000 * (m_w & 65535) + (m_w >> 16)) & 0xffffffff;
  let result = ((m_z << 16) + (m_w & 65535)) >>> 0;
  result /= 4294967296;
  return result;
}

export function pseudoRndInt (max: number): number {
  return Math.floor(pseudoRnd () * (max + 1));
}
export function pseudoRndId (): string {
  return Math.floor(pseudoRnd () * 10000)+"";
}

export const pickRandomKeys = (fullObj: {}, max?: number):Array<string> => {
  const randomArray = [];
  Object.keys(fullObj).forEach((key) => {
    if (max && max <= randomArray.length) {
      return;
    }
    if (pseudoRnd() > 0.5) randomArray.push(key);
  });
  return randomArray;
};

const now = Date.now();
export const rndDate = ():number => {
  const MIN = 60 * 1000;
  return now - (pseudoRndInt(60) * MIN);
}
export const getIds = (collection:any) : Array<string>=> {
  return collection.map(row => row._id);
}
export const pickRandomFromArray = <Item>(fullArray: Array<Item>): Item => {
  return fullArray[Math.floor(pseudoRnd() * fullArray.length)];
};

export const pickSomeFromArray = <Item>(fullArray:Array<Item>):Array<Item> => {
  if (fullArray.length===0)
    return [];
  const array = [];
  fullArray.forEach((value) => {
    if (pseudoRnd() > 0.5) array.push(value);
  });
  return array;
};

export const pickRandomKeyFromObj = (fullObj:any):string => {
  const randomArray = Object.keys(fullObj);
  return randomArray[Math.floor(pseudoRnd() * randomArray.length)];
};
