
// import {describe, beforeEach, test} from '@jest/globals';
import {env} from "../env";

export function mobile(tests) {
  describe("mobile", () => {
    beforeEach(() => {
      cy.viewport("iphone-6");
      cy.reload();
    });
    tests();
  });
}

export function desktop(tests) {
  describe("desktop", () => {
    beforeEach(() => {
      cy.viewport("macbook-15");
      cy.reload();
    });
    tests();
  });
}

export function mobileAndDesktop(tests) {
  if (!env.disableCypressMobile)
  mobile(tests);
  desktop(tests);
}
