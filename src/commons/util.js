//@flow
/**
 *  ex: EmptySQLQuery => empty-sql-query
 * @param string using camel case
 * @returns string using dash case
 */
export const camelToDash = (str:string):string => {
  let result = "";

  function checkIsLowerOrNumber(item:string) {
    return item.charCodeAt(0) >= "a".charCodeAt(0) || !Number.isNaN(+item);
  }

  for (let i = 0; i < str.length; i++) {
    const item = str[i];
    let prevWithDash = false;

    if (!checkIsLowerOrNumber(item))
      prevWithDash = true;

    if (i > 0  && i < str.length-1
      && !checkIsLowerOrNumber(str[i+1])
      && !checkIsLowerOrNumber(str[i-1])
    )
      prevWithDash = false;

    if (i===0)
      prevWithDash = false;

    if (prevWithDash) {
      result += "-" + item.toLocaleLowerCase();
    } else {
      result += item.toLocaleLowerCase();
    }
  }
  return result;
};

let portrait = undefined;
export const isPortrait = <T>  (value: ?T): any => {
  if (portrait===undefined)
    portrait =  window.innerWidth < window.innerHeight;
 if (value===undefined)
   return portrait;
 return portrait ? value : {};
};

export const isLandscape= <T> (value : ?T): any => {
  const landscape =  window.innerWidth > window.innerHeight;
  if (value===undefined)
    return landscape;
  return landscape ? value : {};
};

export const plu = (count:number, word:string):string => {
  return count > 1 ? word+"s" : word;
};

/**
 * Object.values return mixed type.
 * Use this function instead.
 * @param objs
 * @returns {[]}
 */
export const values = <Row> (obj: { [string]: Row }): Array<Row> => {
  return Object.keys(obj).map(key => obj[key]);
};
