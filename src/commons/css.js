// @flow

import {isLandscape, isPortrait} from "./util";

export const padding = (value:string) : any=> {
  return {
    paddingBottom: value,
    paddingRight: value,
    paddingLeft: value,
    paddingTop: value,
  };
};

export const marginT = (value: number): any => {
  return {
    marginRight: value,
    marginLeft: value,
    marginTop: value,
  };
};
export const margin = (value: number): any => {
  return {
    marginBottom: value,
    marginRight: value,
    marginLeft: value,
    marginTop: value,
  };
};

export const size:any = {
  text: isLandscape() ? "1.5vw" : "5vw",
};

export const colors = {
  text: "#333333",
  textDark: "#333333",
  textClear: "#bababa",
  gold: "rgb(223 189 0 / 87%)",
  linkDark: "#0000a2",
  linkClear: "#7777a7",
  errorMsg: "#e52b2b",
};

export const css: any = {
  flexCol : {display:"flex", flexDirection:"column"},
  flexCenterH : {display:"flex", justifyContent:"center", alignItems:"center"},
  flexCenterV : {display:"flex", flexDirection:"column", justifyContent:"center", alignItems:"center"},
  buttonBig: {
    cursor:"pointer",
    textAlign: "center",
    borderStyle: "solid",
    borderColor: "white",
    width: "100%",
    boxSizing: "border-box",
    ...isLandscape({
      ...padding("1vw"),
      fontSize: "2vw",
      borderRadius: "1vw",
      borderWidth: "0.2vw"
    }),
    ...isPortrait({
      ...padding("3vw"),
      fontSize: "6vw",
      borderRadius: "3vw",
      borderWidth: "0.2vw"
    })
  },
  buttonSmall: {
    cursor:"pointer",
    textAlign: "center",
    backgroundColor: "white",
    color: "black",
    ...isLandscape({
      ...padding("0.3vw"),
      borderRadius: "1vw",
      fontSize: "1.3vw",
    }),
    ...isPortrait({
      ...padding("3vw"),
      fontSize: "6vw",
      borderRadius: "3vw",
    })
  },
  inputBig: {
    textAlign: "center",
    borderStyle: "solid",
    borderColor: "white",
    color: colors.text,
    width: "100%",
    boxSizing: "border-box",
    ...isLandscape({
      ...padding("1vw"),
      borderColor: "gray",
      borderWidth: 1,
      borderRadius: "5vw",
      fontSize: "1.8vw"
    }),
    ...isPortrait({
      ...padding("3vw"),
      fontSize: "6vw",
      borderRadius: "3vw",
      borderWidth: "0.2vw"
    }),
  },
  link: {
    color: "#3549ff",
    textDecorationLine: "underline",
  },
  textError: {
    fontSize: "5vw",
    textAlign: "center", ...margin(5),
    color: colors.errorMsg,
  },

};

