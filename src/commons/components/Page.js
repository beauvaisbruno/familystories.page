//@flow
import React from "react";
import {isPortrait} from "../util";
import type {Node} from "react";

export const Page = ({children}:{children:Node}) :Node => {
  return (<div style={{
    backgroundColor: "black",
    backgroundImage: "url(" + require("../../commons/img/headerBg.png").default + ")",
    backgroundRepeat: "no-repeat",
    // backgroundPosition: "top",
    backgroundPosition: "0 6%",
    backgroundSize: "100vw",
    backgroundAttachment: "fixed",
  }}>
    <div style={{
      backgroundColor: "rgba(0,0,0,0.80)",
      maxWidth:"1000px",
      minHeight:"80vh",
      margin: "0 auto",
      padding: "2vw 2vw"
    }}>
      {children}
    </div>
  </div>);
};
