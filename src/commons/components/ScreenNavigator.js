//@flow
import type {Node} from "react";
import React, {useContext, useEffect} from "react";
import {SplashScreen} from "../../screens/splashScreen/SplashScreen";
import {isLandscape, isPortrait} from "../util";

export type ScreenType = {|
  name: string, props: any
|};
type ScreenContextType = {|
  screen: ScreenType,
  setScreen: (name: string, props: any) => void
|};

export const ScreenContext: React$Context<ScreenContextType> = React.createContext<ScreenContextType>({
  screen: {name: "SplashScreen", props: {}},
  setScreen: () => console.warn("ScreenContext is not initialized")
});

export function createScreenContext(
  screen: ScreenType,
  setScreen: (screen: ScreenType) => void = () => console.log("setScreen not initialised")
): ScreenContextType {
  return {screen, setScreen: (name, props):void => setScreen({name, props})};
}

export function pushHistory(screen: ScreenType) {
  document.title = screen.name;
  const hash = "#" + btoa(JSON.stringify(screen));
  // console.log("hash: ", hash, window.location.hash, window.location.hash !== hash);
  if (window.location.hash !== hash) {
    // console.log("pushState location: " + document.location + ", state: " + JSON.stringify(state));
    window.history.pushState(screen, screen.name, hash);
  }
}

export function ScreenNavigator({ screens }: { screens: { [string]: (any)=>Node } }): Node {
  const {screen, setScreen} = useContext(ScreenContext);
  console.log("screen: ", screen);
  useEffect(() => {
    if (screen.name) {
      pushHistory(screen);
    }
  }, [screen]);

  useEffect(() => {
    const listener = (event) => {
      const state: ScreenType = event.state;
      console.log("popstate location: ", document.location, ", state: " + JSON.stringify(state));
      setScreen(state.name, state);
      // setScreen(state.screen, state);
    };
    window.addEventListener("popstate", listener);
    return () => window.removeEventListener("popstate", listener);

  }, []);

  useEffect(() => {
    const listener = () => window.location.reload();
    window.addEventListener("orientationchange", listener);
    return () => window.removeEventListener("popstate", listener);
  }, []);

  if (!screen.name)
    return <SplashScreen/>;

  let Screen = screens[screen.name];
  if (Screen === undefined) {
    console.error("Unknown screenName: ", screen.name,
      ", current screens: ", screens);
    return <screens.Auth/>;
  }

  return <div style={{
    flex: 1,
    ...isLandscape({
      fontSize: "1.5vw"
    }),
    ...isPortrait({
      fontSize: "5vw"
    }),
  }} data-cy={"screen-" + screen.name}>
    <Screen {...screen.props} />
  </div>
    ;
}

