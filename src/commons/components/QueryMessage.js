//@flow
import React from "react";
import {colors} from "../css";
import {isLandscape, isPortrait} from "../util";
import type {Node} from "react";

export const QueryMessage = ({error, loading, children, Loader, errorMsg}:
                               {error?:boolean|string, loading:boolean|string, children?:Node, Loader?:any, errorMsg?:string}):Node => {
  if (error)
    return (
      <div style={{display: "flex",  alignItems: "center"}}>
        <div style={{
          color: colors.errorMsg,
          ...isLandscape({marginBottom: "1vw"}),
          ...isPortrait({marginBottom: "2vw"}),
        }}>
          {errorMsg ? errorMsg : "Oups ): le serveur est injoignable."}
        </div>
      </div>);
  if (loading) {
    if (Loader)
      return <Loader key="Loader"/>;
    return (
      <div style={{display: "flex", alignItems: "center"}}>
        <img src={require("../img/loading.gif").default}
             style={{
               ...isLandscape({width: "5vw"}),
               ...isPortrait({width: "10vw"}),
             }}/>
      </div>);
  }
  if (children)
    return (children);
  else
    return null;
};
