//@flow
import React, {useContext} from "react";
import {env} from "../env";
import type {TheUser} from "../../model/TheUser";
import {LoggedUserContext} from "./AuthLoader";
import {ScreenContext} from "./ScreenNavigator";
import type {Node} from "react";

export const AvatarImg = ({style, user }: {style:any,user?:TheUser}):Node => {
  const {user: currentUser} = useContext(LoggedUserContext);
  const {setScreen} = useContext(ScreenContext);
  return (<img
    data-cy="avatarImg"
    style={{
      ...style,
      borderRadius: "50%",
      cursor: "pointer"
    }}
    src={!user || user.avatar === "default" ? require("../../commons/img/defaultAvatar.png").default
      : env.serverUrl + user.avatar
    }
    onClick={()=>{
      if (!currentUser || !user){
        setScreen("Auth");
        return;
      }
      if (currentUser._id === user._id){
        setScreen("Profile");
      }
    }}
  />);
};
