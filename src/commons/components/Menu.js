//@flow

import React, {useContext} from "react";
import {ScreenContext} from "./ScreenNavigator";
import {colors} from "../css";
import {isLandscape, isPortrait} from "../util";
import {LoggedUserContext} from "./AuthLoader";
import {AvatarImg} from "./AvatarImg";
import type {Node} from "react";

const  itemStyle = {
  color: colors.textClear,
  ...isLandscape({fontSize: "2.2vw"}),
  ...isPortrait({fontSize: "8vw", padding: "5vw 0vw"})
};

export const Menu = ():Node => {
  const {screen: {name: currentScreen}, setScreen} = useContext(ScreenContext);
  const {user} = useContext(LoggedUserContext);
  const screens: Array<{ label: string, screen: string }> = [{
    label: "Albums",
    screen: "MyAlbums",
  }, {
    label: "Contacts",
    screen: "Contacts",
  }, {
    label: "Profile",
    screen: "Profile",
  }];

  let rows = [];

  if (user) {
  let first = true;
    screens.forEach((screenOne) => {
      // console.log("screenOne: ",screenOne);
      if (!first && isLandscape())
        rows.push(<div style={{padding: "0 1vw",}} key={screenOne.screen + "-"}>-</div>);
      if (isLandscape() || (screenOne.screen !== currentScreen))

        rows.push(<div
          key={screenOne.screen}
          style={{
            ...itemStyle,
            ...(screenOne.screen === currentScreen ? {fontWeight: "bold", color: "white"} : {cursor: "pointer"})
          }}
          onClick={() => {
            setScreen(screenOne.screen);
          }}>
          {screenOne.label}
        </div>);
      first = false;
    });
  }

if (!user)
  rows.push(<div
    key={"Connexion"}
    style={{
      ...itemStyle,
      cursor: "pointer"
    }}
    onClick={() => {
      setScreen("Auth");
    }}>
    Connexion
  </div>);

  return (
    <div style={{ ...isPortrait({height:"21vw"}), }}>
      <div style={{
        display: "flex",
        width: "100vw",
        fontSize: "2vw",
        ...isPortrait({justifyContent: "space-evenly", position:"fixed", backgroundColor:"black"}),
        ...isLandscape({justifyContent: "center", padding: "0.8vw 0vw"})
      }}>
        {rows}
        {isLandscape() && <div><img
          style={{
            position: "absolute",
            width: "5vw",
            left: "1vw",
            top: "1vw",
          }}
          src={require("../img/icon.png").default}/>
          <AvatarImg {...{
            user,
            style: {
              position: "absolute",
              width: "5vw",
              right: "1vw",
              top: "1vw",
              cursor:"pointer"
            }
          }}/>
        </div>}
      </div>
    </div>
  );
};
