//@flow
import React, {useState} from "react";
import {loadScreens} from "../screensLoader";
import {createScreenContext, ScreenContext, ScreenNavigator} from "./ScreenNavigator";
import "./commons.css";
import type {Node} from "react";

import {ApolloProvider} from "@apollo/client";
import {apolloClient} from "../apolloClient";
import {AuthLoader} from "./AuthLoader";
import type {ScreenType} from "./ScreenNavigator";

export default function AppLoader():Node {
  // const [screen, setScreen]: [ScreenType, (ScreenType)=>void] = useState<ScreenType>({name: "SplashScreen", props: {}});
  const [screen, setScreen] = useState({name: "SplashScreen", props: {}});

  return (
    <ApolloProvider client={apolloClient}>
      <ScreenContext.Provider value={createScreenContext(screen, setScreen)}>
        <AuthLoader>
          <ScreenNavigator {...{
            screens: loadScreens(),
          }} />
        </AuthLoader>
      </ScreenContext.Provider>
    </ApolloProvider>
  );
}

