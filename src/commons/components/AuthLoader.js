//@flow
import React, {useContext, useEffect, useState} from "react";
import {auth} from "../firebaseInit";
import {ScreenContext} from "./ScreenNavigator";
import type {TheUser} from "../../model/TheUser";
import {QueryMessage} from "./QueryMessage";
import {goToStartScreen} from "../goToStartScreen";
import {useQuery} from "@apollo/client";
import {getUserGql} from "../queries";
import type {Node} from "react";

export const LoggedUserContext: React$Context<{ user?:  TheUser, setUser: (TheUser)=>void }>
  = React.createContext({user:undefined, setUser: (user)=>console.log("setUser not implemented, user: ",user)});

export const UserLoader = ({children, firebaseUser}: {children:Node, firebaseUser:any}): Node => {
  const {error, loading, data} = useQuery(getUserGql, {variables: {email: firebaseUser.email}});
  const {setScreen} = useContext(ScreenContext);
  const {setUser} = useContext(LoggedUserContext);

  useEffect(() => {
    if (data) {
      console.log("UserLoader, user: ",data.getUser);
      setUser(data.getUser);
      goToStartScreen({setScreen, user:data.getUser});
    }
  }, [data]);

  return (
    <QueryMessage {...{error, loading}}>
      {children}
    </QueryMessage>
  );
};

export const AuthLoader = ({children}:{children:Node} ): Node => {
    const [firebaseUser, setFirebaseUser] = useState(undefined);
    const [user, setUser] = useState(undefined);
    const [loading, setLoading] = useState(true);
    const {setScreen} = useContext(ScreenContext);
    useEffect(() => {
      auth().onAuthStateChanged(function (firebaseUser) {
        console.log("firebaseUser: ", firebaseUser);
        setFirebaseUser(firebaseUser);
        setLoading(false);
      });
    }, []);

    useEffect(() => {
      if (!firebaseUser && !loading)
        goToStartScreen({setScreen});
    }, [firebaseUser, loading]);
    return (
      <QueryMessage {...{loading}}>
        <LoggedUserContext.Provider value={{user, setUser}}>
          {firebaseUser && <UserLoader {...{firebaseUser}}>
            {children}
          </UserLoader>}
          {!firebaseUser && children}
        </LoggedUserContext.Provider>
      </QueryMessage>);
  }
;
