//@flow
import React from "react";
import {isLandscape, isPortrait} from "../util";
import type {Node} from "react";

export function Sep(): Node {
  return <div
    style={{backgroundColor: "rgba(255,255,255,0.62)",
      ...isLandscape({
        width: "100%",
        height: "0.01vw",
        margin: "1.2vw 0"
      }),
      ...isPortrait({
        width: "100%",
        height: "0.1vw",
        margin: "3vw 0"
      }),
    }}>

  </div>;
}
