//@flow
import TimeAgo from "javascript-time-ago";

import fr from "javascript-time-ago/locale/fr";

TimeAgo.addDefaultLocale(fr);

const timeAgo = new TimeAgo("fr-fr");
export const ago = (timestamp: number):string => timeAgo.format(new Date(timestamp));
