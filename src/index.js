import React from "react";
import ReactDOM from "react-dom";

import AppLoader from "./commons/components/AppLoader";

ReactDOM.render(
    <AppLoader />,
  document.getElementById("root")
);

