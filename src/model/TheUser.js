//@flow
import {getScalarFields, createGqlTypes} from "./util";
import type {TheAlbum} from "./TheAlbum";
import gql from "graphql-tag";


export type Contact = { name: string, email: string };
export type ContactList = { name: string, contacts: Array<Contact> };

export type TheUserFields = {|
  email: string, //unique
  pseudo: string,
  avatar: string, //default avatar + uploaded avatars
  contactLists: {[string] : ContactList},
  albumIds: Array<string>,
|}

export type TheUserIds = {|
  _id: string,
  __typename: string,
  ...TheUserFields,
|}

export type TheUser = {|
  ...TheUserIds,
  albums: Array<TheAlbum>,
|}

export const fields = `
    _id: String
    email: String
    pseudo: String
    avatar: String
    albums: [TheAlbum]
    contactLists: JSON
`;

export const TheUserGqls:any = createGqlTypes("TheUser",fields);
export const TheUserScalars:string = getScalarFields(fields);
