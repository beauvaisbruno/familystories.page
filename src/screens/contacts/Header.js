//@flow
import type {Node} from "react";
import React, {useRef, useState} from "react";
import {isLandscape, isPortrait} from "../../commons/util";
import {colors, css, padding} from "../../commons/css";
import {btnStyle, marginBottom} from "./styles";
import {gql, useMutation} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {TheUserScalars} from "../../model/TheUser";
import {env} from "../../commons/env";
import {actionStyle} from "./Contacts";
import clonedeep from "lodash.clonedeep";
import type {ContactList, TheUser} from "../../model/TheUser";

export const importContactsGql: any = gql`
  mutation importContacts($file: Upload!, $listId: String!) {
    importContacts(file: $file, listId: $listId ) {
      ${TheUserScalars}
    }
  }
`;

export const ImportBtn = ({actionStyle, listId}:{actionStyle:any, listId:string}): Node => {
  const [importContacts, {error, loading}] = useMutation(importContactsGql);
  const input = useRef();
  return (
    <div>
      <QueryMessage {...{error, loading}}>
        <div
          onClick={() => {
            if (input.current)
            input.current.click();
          }}
          style={actionStyle}>
          Importer CSV/VCF
        </div>
      </QueryMessage>
      <input
        data-cy="importContacts" type="file" ref={input} style={{display: "none"}} required
        onChange={({target: {validity, files: [file],},}) => {
          if (!validity.valid)
            return;
          importContacts({variables: {file, listId}}).then((res) => {
            console.log("importContacts done, res: ", res);
          });
        }}/>
    </div>);
};
export const ActionBtns = (
  {setCreate, user, listId, updateLists}: {
    setCreate: (boolean)=>void,
    user: TheUser,
    listId: string,
    updateLists: ({ [string]: ContactList })=>void,
  }): Node => {

  return (<div style={{
    marginBottom,
    ...isLandscape({...css.flexCenterH}),
    ...isPortrait({...css.flexCenterV}),
  }}>
    <div
      onClick={() => setCreate(true)}
      style={actionStyle}>
      Créer une liste
    </div>
    <ImportBtn {...{actionStyle, listId}}/>
    <div
      onClick={() => {
        const contacts = clonedeep(user.contactLists);
        delete contacts[listId];
        updateLists(contacts);
      }}
      style={actionStyle}>
      Supprimer la liste
    </div>
  </div>);
};

const marginRight = "1vw";

export const AddList = (
  {setCreate, setListId, user, updateLists}:{
    setCreate: (boolean)=>void,
    setListId: (string)=>void,
    user: TheUser,
    updateLists: ({ [string]: ContactList })=>void,
  }): Node => {
  const [name, setName] = useState(env.fillForm ? "Famille" : "");

  const rowStyle = {
    ...isLandscape(css.flexCenterH),
    ...isPortrait(css.flexCenterV),
  };

  return (
    <div style={{
      ...css.flexCenterV,
    }}>
      <div style={{
        ...isLandscape({
          ...css.flexCenterH,
          marginBottom
        }),
        ...isPortrait(css.flexCenterV),
        // display: "flex", flexDirection: "column", alignItems: "center"
      }}>
        <div style={{
          ...isLandscape({marginRight}),
          ...isPortrait({
            fontSize: "8vw",
            marginBottom
          })
        }}>Nom de la liste :
        </div>
        <input
          type="text"
          style={{
            textAlign: "center",
            color: colors.textClear,
            backgroundColor: "transparent",
            outline: "none !important",
            ...isLandscape({
              fontSize: "1.5vw",
              ...padding("0.2vw"),
              width: "25vw",
            }),
            ...isPortrait({
              fontSize: "8vw",
              ...padding(marginRight),
              width: "90vw",
              marginBottom
            }),
            borderColor: "white",
          }}
          placeholder={"Nom de la liste"}
          onChange={event => setName(event.target.value)}
          value={name}
        />
      </div>
      <div style={{
        display: "flex", justifyContent: "center", marginBottom,
        ...isPortrait({flexDirection: "column"})
      }}>
        <div
          onClick={() => {
            const id = Date.now() + "";
            updateLists({
              ...clonedeep(user.contactLists),
              [id]: {name, contacts: []}
            });
            setCreate(false);
            setListId(id);
          }}
          style={{...btnStyle, marginRight}}>
          Créer la liste
        </div>
        <div
          onClick={() => {
            setCreate(false);
            setName("");
          }}
          style={{...btnStyle, marginLeft: marginRight}}>
          Annuler
        </div>
      </div>
    </div>);
};

export const Header = (
  {listId, setListId, user, updateLists, isContactEdit, isCreating, setCreate}:
    { listId?:string,
      setListId: (string)=> void,
      user : TheUser,
      updateLists: ({ [string]: ContactList })=>void,
      isContactEdit : boolean,
      isCreating: boolean,
      setCreate: (boolean)=> void
    })
  : Node => {

  const options = [];
  console.log("Header contactLists: ", user.contactLists);
  Object.keys(user.contactLists).forEach(pListId => {
    options.push(<option key={pListId} style={{color: "black"}} value={pListId}>{user.contactLists[pListId].name}</option>);
  });

  if (isCreating)
    return <AddList {...{setCreate, setListId, user, updateLists}}/>;
  return (<div>
      <div style={{
        ...isLandscape(css.flexCenterH),
        ...isPortrait(css.flexCenterV),
        marginBottom
      }}>
        <div style={{
          ...isLandscape({marginRight}),
          ...isPortrait({
            fontSize: "6vw", marginBottom
          }),

        }}>Contacts Liste :
        </div>
        <select
          disabled={isContactEdit}
          style={{
            color: "black",
            ...isLandscape({
              fontSize: "1.5vw",
              height: "2vw",
              width: "24vw",
            }),
            ...isPortrait({
              fontSize: "6vw",
              width: "80vw",
              ...padding("3vw"),
            }),
          }}
          value={listId}
          onChange={(event) => setListId(event.target.value)}>
          {options}
        </select>
      </div>
      {!isContactEdit && <ActionBtns {...{setCreate, user, updateLists, listId:(listId:any)}}/>}
    </div>
  );

};
