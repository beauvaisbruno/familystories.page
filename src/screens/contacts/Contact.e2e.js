//To run a specified suite or test, append .only to the it function. it.only(...)
//To run for mobile mobile(() => it(...)) or desktop desktop(() => it(...)) or both mobileAndDesktop(() => it(...))

import {desktop, mobileAndDesktop} from "../../commons/.e2e/e2eUtils";

mobileAndDesktop(()=> {
  // desktop(()=> {
  it("Contacts: Auth, create contact list, import contacts, create album then share album", () => {
    //Auth
    cy.contains("email").click();
    cy.get("[placeholder='email@example.com']")
      .type("example1@familystories.page");
    cy.get("[placeholder='Mot de passe']")
      .type("family");
    cy.contains("connecter").click();

    //Contact
    cy.contains("Contacts").click();
    const listName = "Cousins";
    cy.get("[placeholder='Nom de la liste']")
      .type(listName);
    cy.contains("Créer la liste").click();
    cy.contains("Supprimer").click();

    cy.get("[placeholder='Nom de la liste']")
      .type(listName);
    cy.contains("Créer la liste").click();

    cy.get("select option:selected").should("have.text", listName);
    const contact = "test <test@email.com>";

    cy.get("[placeholder='Aucun contact dans cette liste...']").type(contact);
    cy.contains("Sauvegarder").click();
    cy.get("[data-cy='contactsList']").should("include.text",contact+",");

    cy.contains("Importer").click();
    cy.get("[data-cy='importContacts']").attachFile(["googleContacts.csv"]);
    cy.get("[data-cy='contactsList']").should("include.text", "google");
    cy.get("[data-cy='importContacts']").attachFile(["outlookContacts.csv"]);
    cy.get("[data-cy='contactsList']").should("not.include.text", "google");
    cy.get("[data-cy='contactsList']").should("include.text", "outlook");
    cy.get("[data-cy='importContacts']").attachFile(["iosContacts.vcf"]);
    cy.get("[data-cy='contactsList']").should("not.include.text", "outlook");
    cy.get("[data-cy='contactsList']").should("include.text", "ios");

    //Share album
    cy.contains("Albums").click();
    cy.contains("Créer un album").click();

    cy.contains("Partager").click();
    cy.get("[data-cy='addContacts']").click();

    cy.get("[data-cy='contactCount']").should("include.text", "4 contacts");

    cy.get("[data-cy='removeContacts']").click();
    cy.get("[data-cy='addContacts']").click();

    cy.contains("Envoyer des emails").click();

    cy.contains("Un email a été envoyé à 4 contacts");

  });
});
