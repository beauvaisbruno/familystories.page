//@flow

import type {Node} from "react";
import React, {useEffect, useState} from "react";
import {isLandscape, isPortrait} from "../../commons/util";
import {colors, css} from "../../commons/css";
import {marginBottom} from "./styles";
import {actionStyle} from "./Contacts";
import {env} from "../../commons/env";
import clonedeep from "lodash.clonedeep";
import type {Contact, ContactList, TheUser} from "../../model/TheUser";

export const parse = (text: string): Array<Contact> => {
  const contacts = [];
  const blocks = text.split(",");
  blocks.forEach((block) => {
    // console.log("block: ",block);
    const resEmail = block.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi);
    if (resEmail) {
      let name = block.trim();
      const split = block.split("<");
      if (split.length > 0)
        name = split[0].trim();
      contacts.push({
        email: resEmail[0].trim(),
        name
      });
    }
  });
  return contacts;
};
export const createText = ({user, listId}: { user: TheUser, listId: string }): string => {
  let contacts = "";
  const list = user.contactLists[listId];
  if (!list)
    return "";
  if (list.contacts.length === 0) {
    contacts = "exemple de nom <exemple@familystories.page>,\n";
    if (env.fillForm) {
      contacts += "test alexis <alexis@familystories.page>,\n";
      contacts += "test sophie <sophie@familystories.page>,\n";
    }
  }
  list.contacts.forEach((contact) => {
    contacts += contact.name + " <" + contact.email + ">,\n";
  });
  return contacts;
};
export const ContactsList = (
  {user, listId, updateLists, isContactEdit, setContactEdit}
    : {
    user: TheUser,
    listId: string,
    updateLists: ({ [string]: ContactList })=>void,
    isContactEdit: boolean,
    setContactEdit: (boolean)=>void
  }): Node => {
  // console.log("ContactsList user: ", user);

  const [text, setText] = useState("");
  useEffect(() => {
    setText(createText({user, listId}));
  }, [user]);

  return (<div style={{...css.flexCenterV}}>
    {isContactEdit && <div style={{
      marginBottom,
      ...isLandscape({...css.flexCenterH}),
      ...isPortrait({...css.flexCenterV}),
    }}>
      <div
        onClick={() => {
          const contacts = clonedeep(user.contactLists);
          contacts[listId].contacts = parse(text);
          updateLists(contacts);
          setContactEdit(false);
        }}
        style={actionStyle}>
        Sauvegarder
      </div>
      <div
        onClick={() => {
          setText(createText({user, listId}));
          setContactEdit(false);
        }}
        style={actionStyle}>
        Annuler
      </div>
    </div>}
    <textarea
      data-cy={"contactsList"}
      style={{
        cursor: isContactEdit ? "auto" : "pointer",
        ...isLandscape({
          width: "40vw",
          fontSize: "1.3vw",
          height: "30vw",
          padding: "0.3vw 0.3vw",
          backgroundColor: "#ffffffaa"
        }),
        ...isPortrait({
          width: "90vw",
          fontSize: "5vw",
          height: "50vw",
          padding: "0.5vw 0.5vw",
        }),
        color: colors.textDark,
      }}
      value={text}
      onChange={event => {
        setText(event.target.value);
        setContactEdit(true);
      }}
      placeholder={"Aucun contact dans cette liste..."}
    />
  </div>);
};
