//@flow
import {css} from "../../commons/css";
import {isLandscape, isPortrait} from "../../commons/util";

export const marginBottom:string = isLandscape() ? "1vw" : "4vw";
export const marginRight:string = isLandscape() ? "1vw" : "2.5vw";
export const lineHeight:string = isLandscape() ? "2.1vw" : "8vw";

export const buttonWidth:string = isLandscape() ? "10vw" : "50vw";
export const btnStyle = {
  ...css.buttonSmall,
  width: buttonWidth,
  marginBottom,
  // ...isLandscape({lineHeight: "1.5vw"}),
  // ...isPortrait({lineHeight: "5vw"}),
};
export const width = "25vw";
export const listStyle = {
  width,
  height: "30vw",
  overflowX: "auto",
  borderColor: "white",
  borderWidth: "0.1vw",
  borderStyle: "solid",
  boxSizing: "border-box"
};
export const iconStyle:any = {
  cursor: "pointer",
  verticalAlign: "middle",
  ...isLandscape({
    width: "2vw", height: "2vw", objectFit: "contain", marginRight: "1vw"
  }),
  ...isPortrait({
    width: "5vw", height: "5vw", marginRight: "2vw"
  }),
};
export const rowStyle = {color: "white", padding: "0.4vw 0.4vw", display: "inline-flex"};
