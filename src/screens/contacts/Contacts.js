//@flow
import type {Node} from "react";
import React, {useContext, useState} from "react";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import {Menu} from "../../commons/components/Menu";
import {Page} from "../../commons/components/Page";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import type {TheUser} from "../../model/TheUser";
import {Header} from "./Header";
import {gql, useMutation} from "@apollo/client";
import {TheUserScalars} from "../../model/TheUser";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {ContactsList} from "./ContactsList";
import {btnStyle} from "./styles";
import {isLandscape, isPortrait} from "../../commons/util";

export const actionStyle:any = {
  ...btnStyle,
  ...isLandscape({
    width: "14vw",
    margin: "0 1vw"
  }),
  ...isPortrait({
    width: "75vw",
    margin: "2vw 1vw"
  }),
};

export const updateListsGsl: any = gql`
  mutation updateContactLists($lists: JSON!) {
    updateContactLists(lists: $lists ) {
      ${TheUserScalars}
    }
  }
`;

function getFirstId(contactLists) {
  const keys = Object.keys(contactLists);
  if (keys.length ===0)
    return undefined;
  return keys[0];
}

export const Contacts = (): Node => {

  const {user} = useContext(LoggedUserContext);
  if (!user) throw Error ("The user should be authentified");
  const [updateContactLists, {error, loading}] = useMutation(updateListsGsl);

  console.log("Contacts user: ", user);

  const [listId, setListId] = useState(getFirstId(user.contactLists));
  const [isContactEdit, setContactEdit] = useState(false);
  let [isCreating, setCreate] = useState(false);
  if (Object.keys(user.contactLists).length === 0)
    isCreating = true;

  const updateLists = (lists) => {
    updateContactLists({variables:{lists}})
      .then(res => console.log("updatesLists res: ", res));
  };
  return (
    <div>
      <Menu/>
      <Page>
        <QueryMessage {...{error, loading}}>
          <Header {...{listId, setListId, user, updateLists, isContactEdit, setContactEdit,isCreating, setCreate}}/>
        </QueryMessage>
        {!isCreating && <ContactsList {...{user, listId: (listId:any), updateLists, isContactEdit, setContactEdit}}/>}
      </Page>
    </div>
  );
};
