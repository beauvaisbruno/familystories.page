//@flow
import React from "react";
import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";
import {mockUser, mockUsers} from "../../commons/.storybook/mocks/mockUser";
import {mockAlbums} from "../../commons/.storybook/mocks/mockAlbums";
import {getUserGql} from "../../commons/queries";
import {pseudoRndReset} from "../../commons/.storybook/mocks/mockUtils";
import type {Node} from "react";

export default {
  title: "Contacts",
};

export const nominal = ():Node => {
  pseudoRndReset(1);
  return <MockScreen
    {...{
      screen: "Contacts",
      user: mockUser({email: "email1@familystory.page"}),
    }}/>;
};



