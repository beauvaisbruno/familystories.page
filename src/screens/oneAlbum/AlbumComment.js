//@flow
import type {Node} from "react";
import type {TheAlbum} from "../../model/TheAlbum";
import {colors, css} from "../../commons/css";
import type {TheComment} from "../../model/TheComment";
import {AvatarImg} from "../../commons/components/AvatarImg";
import {isLandscape, isPortrait} from "../../commons/util";
import {ago} from "../../commons/timeAgo";
import React, {useContext, useState} from "react";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import {ScreenContext} from "../../commons/components/ScreenNavigator";
import {env} from "../../commons/env";
import {gql, useMutation} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {btnStyle} from "./styles";
import {albumResponse} from "./albumResponse";
import type {TheUser} from "../../model/TheUser";
import {marginBottom, smallFontSize} from "./OneAlbum";

export const deleteAlbumCommentGql: any = () => gql`
 mutation deleteAlbumComment($albumId: String!, $commentId: String! ) {
   deleteAlbumComment(albumId: $albumId, commentId: $commentId) {
     ${albumResponse}
   }
 }
 `;


export function OneComment({album, comment, loggedUser}:{album:TheAlbum, comment:TheComment, loggedUser?:TheUser}):Node {
  const [deleteAlbumComment, {error, loading}] = useMutation(deleteAlbumCommentGql());
  const user: TheUser = comment.user;
  return (<div key={comment._id} style={{display: "flex", marginBottom}}>
    <AvatarImg
      {...{
        style: {
          ...isLandscape({width: "5vw", marginRight: "1vw"}),
          ...isPortrait({width: "15vw", height: "15vw", marginRight: "2vw"}),
        },
        user
      }}
    />
    <div style={{display: "flex", flexDirection: "column"}}>
      <div style={{color: "white", fontSize: smallFontSize}}>{user.pseudo} {ago(comment.date)}</div>
      <QueryMessage {...{error, loading}}>
        <div style={{color: colors.textClear}}>
          {comment.text}
          {loggedUser && loggedUser._id === user._id
          && <img
            data-cy="deleteAlbumComment"
            style={{
              display: "inline", cursor: "pointer", verticalAlign: "center",
              ...isLandscape({width: "1.2vw", marginLeft: "0.3vw"}),
              ...isPortrait({width: "5vw", marginLeft: "1vw", verticalAlign: "middle"}),
            }}
            src={require("../../commons/img/deleteWhite.png").default}
            onClick={() => {
              deleteAlbumComment({variables: {albumId: album._id, commentId: comment._id}})
                .then(res => console.log("deleteAlbumComment res: ", res))
              ;
            }}
          />}
        </div>
      </QueryMessage>
    </div>

  </div>);
}

export function AllComments({album}: { album: TheAlbum }):Node {
  const {user: loggedUser} = useContext(LoggedUserContext);
  if (album.comments.length === 0)
    return (<div style={{fontStyle: "italic", marginBottom, color: colors.textClear, fontSize: smallFontSize}}>
      Il n'y a aucun commentaire pour l'instant
    </div>);
  const rows = [];
  album.comments.forEach((comment: TheComment) => {
    rows.push(<OneComment {...{album, comment, loggedUser, key:comment._id}}/>);
  });
  return (<div style={css.flexCol}>
    {rows}
  </div>);
}

export const addAlbumCommentGql: () =>any = () => gql`
 mutation addAlbumComment($albumId: String!, $text: String! ) {
   addAlbumComment(albumId: $albumId, text: $text) {
     ${albumResponse}
   }
 }
`;

export function Comments({album}: { album: TheAlbum }):Node {
  const {user: loggedUser} = useContext(LoggedUserContext);
  const {setScreen} = useContext(ScreenContext);
  const [newComment, setNewComment] = useState(env.fillForm ? "Vacance avec toute la famille." : "");
  const [addAlbumComment, {error, loading}] = useMutation(addAlbumCommentGql());
  return (<div style={{
    ...isLandscape({width: "40vw"}),
    ...isPortrait({width: "98vw"})
  }}>

    <AllComments {...{album}}/>
    <textarea
      data-cy={"albumTextarea"}
      style={{
        ...isLandscape({
          width: "40vw",
          fontSize: "1.3vw",
          height: "5vw",
          padding: "0.3vw 0.3vw",
        }),
        ...isPortrait({
          width: "98vw",
          fontSize: "5vw",
          height: "20vw",
          padding: "0.3vw 0.3vw",
        }),
        color: colors.textDark,
        backgroundColor: "#ffffffdd"
      }}
      value={newComment}
      onChange={event => setNewComment(event.target.value)}
      placeholder={"Commentez l'album ici..."}
    />
    {loggedUser && <QueryMessage {...{error, loading}}>
      <div
        onClick={() => {
          addAlbumComment({variables: {albumId: album._id, text: newComment}}).then((res) => {
            console.log("addAlbumComment res: ", res);
          });
        }}
        style={btnStyle}>
        Commenter
      </div>
    </QueryMessage>}
    {!loggedUser && <div>
      <div style={{fontSize: smallFontSize, fontStyle: "italic", marginBottom}}>Pour commenter l'album vous devez vous
        connecter.
      </div>
      <div
        onClick={() => {
          setScreen("Auth");
        }}
        style={btnStyle}>
        Se connecter
      </div>
    </div>}

  </div>);
}
