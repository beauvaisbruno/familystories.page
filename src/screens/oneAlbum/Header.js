//@flow

import type {Node} from "react";
import React, {useContext, useRef, useState} from "react";
import {gql, useMutation} from "@apollo/client";
import type {TheAlbum} from "../../model/TheAlbum";
import {TheAlbumScalars} from "../../model/TheAlbum";
import {colors, css, padding} from "../../commons/css";
import {isLandscape, isPortrait, plu} from "../../commons/util";
import {btnStyle} from "./styles";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {ago} from "../../commons/timeAgo";
import {env} from "../../commons/env";
import {Comments} from "./AlbumComment";
import {Sep} from "../../commons/components/Sep";
import {albumResponse} from "./albumResponse";
import {marginBottom, smallFontSize} from "./OneAlbum";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import {AvatarImg} from "../../commons/components/AvatarImg";
import {Dash} from "../myAlbums/MyAlbums";
import {SharePanel} from "./SharePanel";
import {ScreenContext} from "../../commons/components/ScreenNavigator";

export const addPhotoGql: any = gql`
 mutation addPhoto($albumId: String!, $file: Upload! ) {
   addPhoto(albumId: $albumId, file: $file) {
      ${albumResponse}
   }
 }
`;

export function AlbumActions({album}: { album: TheAlbum }): Node {
  const actionStyle = {
    ...btnStyle,
    ...isLandscape({
      width: "14vw",
      margin: "0 1vw"
    }),
    ...isPortrait({
      width: "75vw",
      margin: "2vw 1vw"
    }),
  };
  const {user: loggedUser} = useContext(LoggedUserContext);
  const {setScreen} = useContext(ScreenContext);
  const input = useRef();
  const [addPhoto] = useMutation(addPhotoGql);
  type loaderType = {
    loading: boolean,
    error?: boolean,
    filename?: string,
    count?: number,
    length?: number
  };
  const [{loading, error, filename, count, length}, setLoading] = useState<loaderType>({loading: false,});
  const [showSharePanel, setShowSharePanel] = useState(env.fillForm);
  const [sentMessage, setSentMessage] = useState();

  return (
    <div>
      {!loading && <div style={{
        marginBottom,
        ...isLandscape({...css.flexCenterH}),
        ...isPortrait({...css.flexCenterV}),
      }}>
        <div
          onClick={() => {
            setScreen("Diaporama", {albumId: album._id});
          }}
          style={actionStyle}>
          Diaporama
        </div>
        <input
          data-cy="addPhotoInput" type="file" ref={input} style={{display: "none"}} required multiple
          onChange={({target: {validity, files,},}) => {
            if (!validity.valid)
              return;
            setLoading({loading: true});
            (async () => {
              let count = 0;
              for (const file of files) {
                console.log("file: ", file);
                setLoading({loading: true, filename: file.name, count, length: files.length});
                const res = await addPhoto({variables: {albumId: album._id, file}});
                console.log("addPhotos done, res: ", res);
                count++;
              }
            })().then(() => setLoading({loading: false})).catch(error => {
              console.error(error);
              setLoading({error: true, loading: false, filename, count, length});
            });
          }}/>
        {loggedUser && loggedUser._id === album.owner._id && <div
          style={actionStyle}
          data-cy="addPhotos"
          onClick={() => {
            if (input.current)
              input.current.click();
          }}
        >
          Ajouter des photos
        </div>}
        {loggedUser && !showSharePanel && <div
          onClick={() => {
            setShowSharePanel(true);
          }}
          style={actionStyle}>
          Partager l'album
        </div>}
      </div>}
      <div style={css.flexCenterH}>
        {error && <div style={{
          color: colors.errorMsg,
          ...isLandscape({marginBottom: "1vw"}),
          ...isPortrait({marginBottom: "2vw"}),
        }}>
          Oups ): le serveur est injoignable.
        </div>}
        {filename && count && length && (filename + " " + count + "/" + length)}
        {loading &&
        <img src={require("../../commons/img/loading.gif").default}
             style={{
               ...isLandscape({width: "5vw"}),
               ...isPortrait({width: "10vw"}),
             }}/>}
      </div>
      {showSharePanel && loggedUser && <SharePanel {...{album, setShowSharePanel, setSentMessage}}/>}
      {sentMessage !== undefined && <div style={{textAlign: "center", fontWeight: "bold"}}> Un email a été envoyé
        à {sentMessage + " " + plu(sentMessage, "contact")} </div>}
    </div>);
}

export function SubTitle({album, showComment}: { album: TheAlbum, showComment: boolean }): Node {
  const [seeComment, setSeeComment] = useState(showComment || env.fillForm);
  return (<div style={css.flexCenterV}>
      <div style={{
        ...css.flexCenterH,
        marginBottom,
        fontSize: smallFontSize,
        // ...isLandscape(css.flexCenterH),
        ...isPortrait({width: "96vw"}),
        flexWrap: "wrap"
      }}>
        <div>Créé {ago(album.addedDate)}</div>
        <div
          className="underlineHover"
          style={{...css.flexCenterH, cursor: "pointer"}}
          onClick={() => setSeeComment(!seeComment)}
        ><Dash/>{album.photos.length + " " + plu(album.photos.length, "photo")}</div>
        <div><Dash/>{album.seenCount + " " + plu(album.seenCount, "vue")}</div>
        <div data-cy={"showAlbumCommentToggle"} onClick={() => setSeeComment(!seeComment)}>
          <Dash/>
          {album.comments.length + " " + plu(album.comments.length, "commentaire")}
          <img
            style={{
              verticalAlign: "middle",
              ...isLandscape({width: "1.3vw", marginLeft: "0.5vw"}),
              ...isPortrait({width: "5vw", marginLeft: "1vw"}),
            }}
            src={(() => {
              if (seeComment)
                return require("../../commons/img/hideWhite.png").default;
              if (album.comments.length === 0)
                return require("../../commons/img/editWhite.png").default;
              return require("../../commons/img/seeWhite.png").default;
            })()}/>
        </div>
      </div>
      <AlbumActions {...{album}}/>
      <Sep/>
      {seeComment && <Comments {...{album}}/>}
      {seeComment && <Sep/>}
    </div>
  );
}

export const updateAlbumGql: any = gql`
 mutation updateAlbumTitle($_id: String!, $title: String! ) {
   updateAlbumTitle(_id: $_id, title: $title) {
      ${TheAlbumScalars}
   }
 }
`;

function Title({album}: { album: TheAlbum }) {
  const {user: loggedUser} = useContext(LoggedUserContext);

  const [edit, setEdit] = useState(loggedUser && env.fillForm);
  const [title, setTitle] = useState(album.title);
  const [updateAlbum, {error, loading}] = useMutation(updateAlbumGql);

  return (<div style={{marginBottom}}>
    {(!loggedUser || loggedUser._id !== album.owner._id) && <div style={{...css.flexCenterV, marginBottom}}>
      <AvatarImg {...{
        user: album.owner, style: {
          ...isLandscape({width: "5vw"}),
          ...isPortrait({width: "15vw"}),
        }
      }}/>
      {album.owner.pseudo}
    </div>}
    {edit || <div
      data-cy={"titleEdit"}
      style={{
        display: "flex",
        justifyContent: "center",
        ...isLandscape({
          fontSize: "2.5vw",
        }),
        ...isPortrait({
          fontSize: "7vw",
        }),
        alignItems: "center",
        cursor: "pointer",
      }}
      onClick={() => {
        setEdit(true);
      }}
    >
      {album.title}
      {loggedUser && loggedUser._id === album.owner._id && <img
        style={{
          ...isLandscape({
            width: "2vw", height: "2vw", marginLeft: "1vw"
          }),
          ...isPortrait({
            width: "5vw", height: "5vw", marginLeft: "2vw"
          }),
        }}
        src={require("../../commons/img/editWhite.png").default}/>}

    </div>}
    {edit && <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
      <input
        type="text"
        style={{
          textAlign: "center",
          color: colors.textClear,
          fontSize: "2.5vw",
          backgroundColor: "transparent",
          outline: "none !important",
          ...isLandscape({
            fontSize: "2.5vw",
            ...padding("0.2vw"),
            width: "50vw",
          }),
          ...isPortrait({
            fontSize: "5vw",
            ...padding("1vw"),
            width: "90vw",
          }),
          borderColor: "white",
          marginBottom
        }}
        placeholder={"Votre titre de l'album"}
        onChange={event => setTitle(event.target.value)}
        value={title}
        inputMode="email"
      />
      <div style={{
        display: "flex", justifyContent: "center", marginBottom,
        ...isPortrait({flexDirection: "column"})
      }}>
        <QueryMessage {...{error, loading}}>
          <div
            onClick={() => {
              updateAlbum({
                variables: {_id: album._id, title},
              }).then(res => {
                setEdit(false);
                console.log("mutate done, res: ", res);
              });
            }}
            style={{...btnStyle, marginRight: "1vw"}}>
            Sauvegarder
          </div>
          <div
            onClick={() => {
              setTitle(album.title);
              setEdit(false);
            }}
            style={{...btnStyle, marginLeft: "1vw"}}>
            Annuler
          </div>
        </QueryMessage>
      </div>
    </div>}
  </div>);
}

export function Header({album, showComment}: { album: TheAlbum, showComment: boolean }): Node {
  return (<div style={{}}>
    <Title {...{album}}/>
    <SubTitle {...{album, showComment}}/>
  </div>);

}
