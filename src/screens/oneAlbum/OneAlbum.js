//@flow
import React from "react";
import {Menu} from "../../commons/components/Menu";
import {Page} from "../../commons/components/Page";
import {gql, useQuery} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {Header} from "./Header";
import {albumResponse} from "./albumResponse";
import {PhotoList} from "./PhotoList";
import {bake_cookie, read_cookie} from "sfcookies";
import {isLandscape} from "../../commons/util";
import {updateSeenAlbum} from "../../commons/updateSeenAlbum";
import type {Node} from "react";

export const marginBottom:string = isLandscape() ? "0.5vw" : "2vw";
export const smallFontSize:string = isLandscape() ? "1.2vw" : "5vw";

export const getAlbumGql: any = gql`
  query getAlbum( $_id: String!) {
    getAlbum(_id: $_id) {
       ${albumResponse}
    }
  }
`;

export const OneAlbum = ({albumId, showComment}: { albumId: string,showComment:boolean }): Node => {

  const {loading, error, data} = useQuery(getAlbumGql, {
      variables: {_id: albumId}, onCompleted: updateSeenAlbum
    });
    const album = data ? data.getAlbum : data;
    console.log("data: ", data);
    console.log("loading: ", loading);
    console.log("error: ", error);
    console.log("album: ", album);
    return (
      <div>
        <Menu/>
        <Page>
          <QueryMessage {...{error, loading}}>
            <Header {...{album, showComment}}/>
            <PhotoList {...{album}}/>
          </QueryMessage>
        </Page>
      </div>
    );
  }
;
