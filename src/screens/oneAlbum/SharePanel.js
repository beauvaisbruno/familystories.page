//@flow
import type {Node} from "react";
import type {TheAlbum} from "../../model/TheAlbum";
import {Sep} from "../../commons/components/Sep";
import React, {useContext, useState} from "react";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import type {TheUser} from "../../model/TheUser";
import {css} from "../../commons/css";
import {btnStyle, marginBottom, marginRight} from "./styles";
import {isLandscape, isPortrait, plu} from "../../commons/util";
import {gql, useMutation} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";

const actionStyle = {
  ...btnStyle,
  ...isLandscape({
    width: "14vw",
    margin: "0 1vw"
  }),
  ...isPortrait({
    width: "75vw",
    margin: "2vw 1vw"
  }),
};

export const sendEmailGql: any = gql`
 mutation sendEmail($albumId: String!, $contacts: JSON! ) {
   sendEmail(albumId: $albumId, contacts: $contacts)
 }
`;

export function SharePanel({setShowSharePanel, album, setSentMessage}: {
  album: TheAlbum,
  setShowSharePanel: (boolean)=>void ,
  setSentMessage: (number)=>void ,
}):Node {
  const {user} = useContext(LoggedUserContext);
  if (!user) throw Error("An user user must be logged");
  const [listIds, setListIds] = useState([]);
  const [sendEmail, {loading, error}] = useMutation(sendEmailGql);

  const prevEmails = [];
  const contacts = [];
  let contactsTxt = "";

  const listRows = [];
  Object.keys(user.contactLists).forEach(listId => {
    const list = user.contactLists[listId];
    const selected = listIds.includes(listId);
    if (selected) {
      list.contacts.forEach((contact) => {
        if (!prevEmails.includes(contact.email)) {
          contacts.push(contact);
          contactsTxt += contact.name + " <" + contact.email + ">,\n";
          prevEmails.push(contact.email);
        }
      });
    }

    console.log("list: ", list);
    const contactCount = list.contacts.length;
    listRows.push(<div
      data-cy={selected ? "removeContacts" : "addContacts"}
      key={listId}
      style={{
        display: "flex",
        marginBottom,
        alignItems: "center",
        cursor: "pointer",
        backgroundColor: selected ? "#ffffff55" : "#ffffff00"
      }}
      onClick={() => {
        if (selected)
          setListIds([...listIds].filter(id => id !== listId));
        else
          setListIds([...listIds, listId]);
      }}
    >
      <img
        title={selected ? "Enlever les contacts":"Ajouter les contacts"}
        data-cy="deleteAlbum"
        style={{
          cursor: "pointer",
          ...isLandscape({width: "2vw", height: "2vw"}),
          ...isPortrait({width: "7vw", height: "7vw"}),
          marginRight
        }}
        src={selected ? require("../../commons/img/cancel.png").default
          : require("../../commons/img/plusWhite.png").default}
      />
      <div>{list.name + " (" + contactCount + " " + plu(contactCount, "contact") + ")"}</div>

    </div>);
  });
  return (<div style={{...css.flexCenterV}}>
    <Sep/>
    <div
      style={{
        ...isLandscape({width: "30vw"}),
      marginBottom}}>
      <div
        data-cy={"contactCount"}
        style={{marginBottom, cursor: "pointer", display:"flex", alignItems:"center"}}
        title={contactsTxt}
      >
        Destinataires : {contacts.length + " " + plu(contacts.length, "contact")}
        {isLandscape() && <img
          style={{
            // verticalAlign:"middle",
            ...isLandscape({width: "1.3vw", height: "1.3vw", marginLeft: "0.5vw"}),
            ...isPortrait({width: "5vw", height: "5vw", marginLeft: "1vw"}),
          }}
          src={require("../../commons/img/info.png").default}/>}
      </div>
      <div style={{}}>
        {listRows}
      </div>
    </div>
    <div style={{
      marginBottom,
      ...isLandscape({...css.flexCenterH}),
      ...isPortrait({...css.flexCenterV}),
    }}>
      <QueryMessage {...{loading, error}}>
        <div
          onClick={() => {
            console.log("contacts: ", contacts);
            sendEmail({variables: {albumId: album._id, contacts}})
              .then(res => {
                console.log("sendEmail res: ", res);
                if (contacts.length === 0)
                  return;
                setShowSharePanel(false);
                setSentMessage(contacts.length);
              })
            ;
          }}
          style={actionStyle}>
          Envoyer des emails
        </div>
        <div
          onClick={() => {
            setShowSharePanel(false);
          }}
          style={actionStyle}>
          Annuler
        </div>
      </QueryMessage>
    </div>
  </div>);
}
