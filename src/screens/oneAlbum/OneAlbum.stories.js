//@flow
import type {Node} from "react";
import React from "react";
import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";
import {mockUser, mockUsers} from "../../commons/.storybook/mocks/mockUser";
import {mockAlbum} from "../../commons/.storybook/mocks/mockAlbums";
import {pseudoRndReset} from "../../commons/.storybook/mocks/mockUtils";
import {getAlbumGql} from "./OneAlbum";
import {sendEmailGql} from "./SharePanel";

export default {
  title: "OneAlbum",
};

export const logged = ():Node => {
  pseudoRndReset(2);
  const owner = mockUser({email: "email1@familystory.page"});
  const users = mockUsers({nbr: 4});
  const album = mockAlbum({owner, users});

  return <MockScreen
    {...{
      screen: "OneAlbum",
      screenProps: {albumId: album._id},
      user: owner,
      remotes: [{
        query: getAlbumGql,
        data: {
          getAlbum: album,
        }
      },{
        query: sendEmailGql,
        data: {
          sendEmail:undefined,
        },
      }]
    }}/>;
};

export const notLogged = ():Node => {
  pseudoRndReset(2);
  const owner = mockUser({email: "email1@familystory.page"});
  const users = mockUsers({nbr: 4});
  const album = mockAlbum({owner, users});
  console.log("notLogged album: ",album);

  return <MockScreen
    {...{
      screen: "OneAlbum",
      screenProps: {albumId: album._id},
      remotes: [{
        query: getAlbumGql,
        data: {
          getAlbum:album,
        },
      }]
    }}/>;
};

export const empty = ():Node => {
  pseudoRndReset(4);
  const owner = mockUser({email: "email1@familystory.page"});
  const album = mockAlbum({owner,photoNbr: 0, commentNbr:0});
  console.log("empty album: ",album);
  console.log("empty _id: ",album._id);
  return <MockScreen
    {...{
      screen: "OneAlbum",
      screenProps: {albumId: album._id},
      user: owner,
      remotes: [{
        query: getAlbumGql,
        data: {
          getAlbum: album
        }
      }]
    }}/>;
};


