//@flow
import {gql} from "@apollo/client";
import {TheAlbumScalars} from "../../model/TheAlbum";
import {TheUserScalars} from "../../model/TheUser";
import {TheCommentScalars} from "../../model/TheComment";
import {ThePhotoScalars} from "../../model/ThePhoto";

export const albumResponse = `
  ${TheAlbumScalars}
  owner {
    ${TheUserScalars}
  }
  comments {
    ${TheCommentScalars}
    user {
      ${TheUserScalars}
    }
  }
  couverturePhoto{
      ${ThePhotoScalars}
  }
  photos {
      ${ThePhotoScalars}
      comments {
        ${TheCommentScalars}
        user {
          ${TheUserScalars}
        }
      }
  }
`;
// export const albumResponse = `
//       ${TheAlbumScalars}
//       owner {
//         ${TheUserScalars}
//       }
//       comments {
//         ${TheCommentScalars}
//       }
//       photos {
//           ${ThePhotoScalars}
//       }
// `;
