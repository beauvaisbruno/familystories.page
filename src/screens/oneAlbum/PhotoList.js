//@flow
import type {Node} from "react";
import {gql, useMutation} from "@apollo/client";

import type {TheAlbum} from "../../model/TheAlbum";
import {btnStyle} from "./styles";
import {env} from "../../commons/env";
import {Sep} from "../../commons/components/Sep";
import {colors, css, padding} from "../../commons/css";
import React, {useContext, useState} from "react";
import type {ThePhoto} from "../../model/ThePhoto";
import {ThePhotoScalars} from "../../model/ThePhoto";
import {isLandscape, isPortrait, plu} from "../../commons/util";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {ago} from "../../commons/timeAgo";
import {smallFontSize} from "./OneAlbum";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import {Comments} from "./PhotoComment";
import {albumResponse} from "./albumResponse";
import {Dash} from "../myAlbums/MyAlbums";
import type {TheUser} from "../../model/TheUser";
import {ScreenContext} from "../../commons/components/ScreenNavigator";

export const deletePhotoGql: any = gql`
  mutation deletePhoto($photoId: String!,$albumId: String! ) {
    deletePhoto(photoId: $photoId,albumId: $albumId) {
       ${albumResponse}
    }
  }
`;

export const marginBottom:string = isLandscape() ? "0.5vw" : "2vw";

export const DeletePhotoBtn = ({album, photo}: { album: TheAlbum, photo: ThePhoto }):Node => {
  const [deletePhoto, {error, loading}] = useMutation(deletePhotoGql);
  const {user: loggedUser} = useContext(LoggedUserContext);
  return (
    <QueryMessage {...{error, loading, errorMsg: "Oups :("}}>
      <img
        title={"Supprimer la photo"}
        data-cy="deleteImg"
        style={{
          cursor: "pointer",
          ...isLandscape({
            width: "2vw", height: "2vw", marginLeft: "1vw"
          }),
          ...isPortrait({
            width: "5vw", height: "5vw", marginLeft: "2vw"
          }),
        }}
        src={require("../../commons/img/deleteWhite.png").default}
        onClick={() => {
          if (!loggedUser)
            return;
          deletePhoto({variables: {photoId: photo._id, albumId: album._id}})
            .then(res => console.log("deletePhoto res: ", res));
        }}
      />
    </QueryMessage>);
};

export const updatePhotoDescriptionGql: any = gql`
  mutation updatePhotoDescription($_id: String!, $description: String! ) {
    updatePhotoDescription(_id: $_id, description: $description) {
       ${ThePhotoScalars}
    }
  }
`;

export const Description = ({album, photo}: { album: TheAlbum, photo: ThePhoto }):Node => {
  const {user: loggedUser} = useContext(LoggedUserContext);
  const [edit, setEdit] = useState(loggedUser && env.fillForm);
  const [description, setDescription] = useState(photo.description);
  const [updatePhotoDescription, {error, loading}] = useMutation(updatePhotoDescriptionGql);
  if (edit)
    return (<div style={{display: "flex", flexDirection: "column",
    ...isLandscape({width: "45vw", paddingRight: "1vw"}),
    ...isPortrait({width: "98vw", alignItems:"center"}),
    }}>
      <input
        type="text"
        data-cy={"photoDescription"}
        style={{
          color: colors.textClear,
          backgroundColor: "transparent",
          outline: "none !important",
          ...isLandscape({
            fontSize: "1.2vw",
            ...padding("0.2vw"),
            width: "45vw",
          }),
          ...isPortrait({
            fontSize: "5vw",
            ...padding("1vw"),
            width: "90vw",
          }),
          borderColor: "white",
          marginBottom
        }}
        onChange={event => setDescription(event.target.value)}
        value={description}
        inputMode="email"
      />
      <div style={{display: "flex", justifyContent: "center",
        ...isPortrait({flexDirection:"column",
        }),
      }}>
        <QueryMessage {...{error, loading}}>
          <div
            onClick={() => {
              updatePhotoDescription({
                variables: {_id: photo._id, description},
              }).then(res => {
                setEdit(false);
                console.log("mutate done, res: ", res);
              });
            }}
            style={{...btnStyle, marginRight: "1vw",marginBottom}}>
            Sauvegarder
          </div>
          <div
            onClick={() => {
              setDescription(photo.description);
              setEdit(false);
            }}
            style={{...btnStyle, marginLeft: "1vw",marginBottom}}>
            Annuler
          </div>
        </QueryMessage>
      </div>
    </div>);
  return (<div style={{display: "flex"}}>
    <div
      style={{
        display: "flex",
        marginBottom
      }}
    >
      {photo.description}
    </div>

    {loggedUser && loggedUser._id === album.owner._id && <div style={{
      display: "flex",
      width: "12vw",
      justifyContent: "flex-end",
      flexGrow: "1"
    }}>
      <img
        data-cy={"editPhotoDescription"}
        onClick={() => {
          if (!loggedUser)
            return;
          setEdit(true);
        }}
        style={{
          cursor: "pointer",
          ...isLandscape({
            width: "2vw", height: "2vw", marginLeft: "1vw"
          }),
          ...isPortrait({
            width: "5vw", height: "5vw", marginLeft: "2vw"
          }),
        }}
        src={require("../../commons/img/editWhite.png").default}/>
      <DeletePhotoBtn {...{photo, album}}/>
    </div>}

  </div>);
};

export const setCouvertureGql: any = gql`
  mutation setCouverture($photoId: String!, $albumId: String! ) {
    setCouverture(photoId: $photoId, albumId: $albumId) {
       ${albumResponse}
    }
  }
`;

export const Photo = ({album, photo}: { album: TheAlbum, photo: ThePhoto }):Node => {
  const {setScreen} = useContext(ScreenContext);
  let commentsCount = photo.comments.length;
  const [seeComment, setSeeComment] = useState(env.fillForm);
  const [setCouverture, {error, loading}] = useMutation(setCouvertureGql);
  return (
    <div>
      <div style={{display: "flex", ...isPortrait({flexDirection:"column", alignItems:"center"})}}>
        <img
          key={photo._id}
          style={{
            ...isLandscape({
              height: "7vw",
              width: "9.5vw",
              marginRight: "1vw",
            }),
            ...isPortrait({
              height: "28vw",
              width: "38vw",
              marginRight: "2vw",
            }),
            objectFit: "contain",
            backgroundColor: "black",
            cursor:"pointer"
          }}
          src={env.serverUrl  + photo.url}

          onClick={()=> {
            setScreen("Diaporama", {albumId: album._id, photoId: photo._id });
          }}
        />
        <div style={{...css.flexCol, width: "100%"}}>
          <Description {...{album, photo}}/>
          <div style={{fontSize: smallFontSize, color: colors.textClear, display: "flex", marginBottom,
            ...isPortrait({flexDirection:"column"})
          }}>
            Créé {ago(photo.addedDate)}
            {isLandscape() && <Dash/>}
            <div
              data-cy={"showPhotoCommentToggle"}
              className="underlineHover"
              style={{display: "flex", cursor: "pointer"}}
              onClick={() => setSeeComment(!seeComment)}
            >
              {commentsCount + " " + plu(commentsCount, "commentaire")}&nbsp;
              <img
                style={{...isLandscape({width:"1.3vw"}), ...isPortrait({width:"5vw"})}}
                src={(() => {
                  if (seeComment)
                    return require("../../commons/img/hideWhite.png").default;
                  if (commentsCount === 0)
                    return require("../../commons/img/editWhite.png").default;
                  return require("../../commons/img/seeWhite.png").default;
                })()}/>
            </div>
          </div>
          {(!album.couverturePhoto || album.couverturePhoto._id !== photo._id) &&
          <QueryMessage {...{error, loading}}>
            <div
              onClick={() => {
                setCouverture({variables: {photoId: photo._id, albumId: album._id,}}).then((res) => {
                  console.log("setCouverture res: ", res);
                });
              }}
              style={btnStyle}>
              Couverture
            </div>
          </QueryMessage>}
          {seeComment && <Sep/>}
          {seeComment && <Comments {...{photo, album}}/>}
        </div>
      </div>
      <Sep/>
    </div>
  );
};

export const PhotoList = ({album}: { album: TheAlbum }):Node => {
  const rows = [];

  album.photos.forEach((photo: ThePhoto) => {
    rows.push(<Photo {...{album, photo, key:photo._id}}/>);
  });

  return (<div style={{...css.flexCol}}>
    {rows}
  </div>);
};
