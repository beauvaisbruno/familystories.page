//To run a specified suite or test, append .only to the it function. it.only(...)
//To run for mobile mobile(() => it(...)) or desktop desktop(() => it(...)) or both mobileAndDesktop(() => it(...))

import {desktop, mobileAndDesktop} from "../../commons/.e2e/e2eUtils";

mobileAndDesktop(()=> {
  // desktop(()=> {
  it("OneAlbum: Auth, create album, import photos then comments", () => {
    //Auth
    cy.contains("email").click();
    cy.get("[placeholder='email@example.com']")
      .type("example1@familystories.page");
    cy.get("[placeholder='Mot de passe']")
      .type("family");
    cy.contains("connecter").click();
    //Create albums
    cy.contains("Albums").click();
    cy.contains("Créer un album").click();

    cy.get("[data-cy='titleEdit']").click();
    const albumTitle = "Les vacances";
    cy.get("[value='Titre de votre album']").clear()
      .type(albumTitle);
    cy.contains("Sauvegarder").click();

    cy.get("[data-cy='showAlbumCommentToggle']").first().click();
    cy.get("[data-cy='albumTextarea']").type("Dernier été en famille");
    cy.contains("Commenter").click();
    cy.get("[data-cy='deleteAlbumComment']").first().click();
    cy.get("[data-cy='showAlbumCommentToggle']").first().click();

    cy.contains("Ajouter des photos").click();
    cy.get("[data-cy='addPhotoInput']").attachFile(["beach.jpg","christmas.jpg"]);

    cy.get("[data-cy='editPhotoDescription']").last().click();
    cy.get("[value='beach']").clear()
      .type("Noël en famille");
    cy.contains("Sauvegarder").click();

    cy.contains("Couverture").click();

    cy.get("[data-cy='showPhotoCommentToggle']").first().click();
    cy.get("[data-cy='photoTextarea']").type("Photo du dernier noël");
    cy.contains("Commenter").click();
    cy.get("[data-cy='deletePhotoComment']").first().click();

    cy.contains("Albums").click();
    cy.get("[data-cy='deleteAlbum']").first().click();
    cy.contains("Confirmer").click();

  });
});
