//To run a specified suite or test, append .only to the it function. it.only(...)
//To run for mobile mobile(() => it(...)) or desktop desktop(() => it(...)) or both mobileAndDesktop(() => it(...))

import {desktop, mobileAndDesktop} from "../../commons/.e2e/e2eUtils";

mobileAndDesktop(()=> {
  // desktop(()=> {
  it("Diaporama: Auth, create album, display diaporama then Comments", () => {
    //Auth
    cy.contains("email").click();
    cy.get("[placeholder='email@example.com']")
      .type("example1@familystories.page");
    cy.get("[placeholder='Mot de passe']")
      .type("family");
    cy.contains("connecter").click();

    //create album
    cy.contains("Albums").click();
    cy.contains("Créer un album").click();

    cy.contains("Ajouter des photos").click();
    cy.get("[data-cy='addPhotoInput']").attachFile(["beach.jpg","christmas.jpg"]);

    //display diaporama
    cy.contains("Diaporama").click();
    cy.get("[data-cy='nextPhoto']").click();
    cy.get("[data-cy='prevPhoto']").click();

    //comments
    cy.get("[data-cy='photoTextarea']").type("Photo du dernier noël");
    cy.contains("Commenter").click();
    cy.get("[data-cy='deletePhotoComment']").first().click();


  });
});
