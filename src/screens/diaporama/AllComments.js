//@flow
import React, {useContext, useState} from "react";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import {colors, css} from "../../commons/css";
import {AvatarImg} from "../../commons/components/AvatarImg";
import {isLandscape, isPortrait} from "../../commons/util";
import {ago} from "../../commons/timeAgo";
import {marginBottom, marginLeft, smallFontSize} from "./style";
import {useMutation} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import type {TheUser} from "../../model/TheUser";
import type {TheComment} from "../../model/TheComment";
import type {ThePhoto} from "../../model/ThePhoto";
import {addPhotoCommentGql, deletePhotoCommentGql} from "../../commons/queries";
import {Sep} from "../../commons/components/Sep";
import {btnStyle} from "../oneAlbum/styles";
import {env} from "../../commons/env";
import {ScreenContext} from "../../commons/components/ScreenNavigator";
import type {Node} from "react";
import type {TheAlbum} from "../../model/TheAlbum";

export function OneComment({photo, comment} : {photo:ThePhoto, comment:TheComment}):Node {
  const {user: loggedUser} = useContext(LoggedUserContext);
  const [deletePhotoComment, {error, loading}] = useMutation(deletePhotoCommentGql);
  const user: TheUser = comment.user;
  return (
    <div>
      <div style={{display: "flex", alignSelf: "flex-start", alignItems: "center", marginBottom}}>
        <AvatarImg {...{user, style: {
          ...isLandscape({width: "3.5vw", height: "3.5vw"}),
          ...isPortrait({width: "10vw", height: "10vw"}),
          }
        }}/>
        <div style={{display: "flex", flexDirection: "column", marginLeft}}>
          <div>{user.pseudo}</div>
          <div style={{color: colors.textClear, fontSize: smallFontSize}}>{ago(comment.date)}</div>
        </div>

        {loggedUser && loggedUser._id === comment.user._id &&
        <div style={{display: "flex", flexGrow: 1, justifyContent: "flex-end"}}>
          <img
            data-cy="deletePhotoComment"
            style={{
              display: "inline", cursor: "pointer", verticalAlign: "center",
              ...isLandscape({width: "1.2vw", marginLeft: "0.3vw"}),
              ...isPortrait({width: "5vw", marginLeft: "1vw", verticalAlign: "middle"}),
            }}
            src={require("../../commons/img/deleteWhite.png").default}
            onClick={() => {
              deletePhotoComment({variables: {photoId: photo._id, commentId: comment._id}})
                .then(res => console.log("deletePhotoComment res: ", res))
              ;
            }}
          />
        </div>}

      </div>
      <div style={{width: "100%"}}>
        <QueryMessage {...{error, loading}}>
          {comment.text}

        </QueryMessage>
      </div>
      <Sep/>
    </div>);
}

export function NewComment({photo, album}:{photo:ThePhoto, album:TheAlbum}):Node {
  const {user: loggedUser} = useContext(LoggedUserContext);
  const {setScreen} = useContext(ScreenContext);
  const [newComment, setNewComment] = useState(env.fillForm ? "Vacance avec toute la famille." : "");
  const [addPhotoComment, {error, loading}] = useMutation(addPhotoCommentGql);
  return (
    <div style={{ ...css.flexCol }}>
      <textarea
        data-cy={"photoTextarea"}
        style={{
          ...isLandscape({
            fontSize: "1.3vw",
            height: "5vw",
            padding: "0.3vw 0.3vw",
          }),
          ...isPortrait({
            width: "98vw",
            fontSize: "5vw",
            height: "20vw",
            padding: "0.3vw 0.3vw",
          }),
          marginBottom,
          color: colors.textDark,
          backgroundColor: "#ffffffdd"
        }}
        value={newComment}
        onChange={event => setNewComment(event.target.value)}
        placeholder={"Commentez la photo ici..."}
      />
      {loggedUser && <QueryMessage {...{error, loading}}>
        <div
          onClick={() => {
            if(newComment.trim()!=="")
              addPhotoComment({variables: {albumId:album._id,photoId: photo._id, text: newComment.trim()}}).then((res) => {
              console.log("addPhotoComment res: ", res);
            });
          }}
          style={btnStyle}>
          Commenter
        </div>
      </QueryMessage>}
      {!loggedUser && <div>
        <div style={{fontSize: smallFontSize, fontStyle: "italic", marginBottom}}>Pour commenter la photo vous devez vous
          connecter.
        </div>
        <div
          onClick={() => {
            setScreen("Auth");
          }}
          style={btnStyle}>
          Se connecter
        </div>
      </div>}
    </div>
  );
}
export function AllComments({photo,album}:{photo:ThePhoto, album:TheAlbum}):Node {
  const rows = [];
  photo.comments.forEach((comment: TheComment) => {
    rows.push(<OneComment {...{photo, comment, key: comment._id}}/>);
  });
  return (<div style={css.flexCol}>
    {photo.comments.length === 0 && <div style={{fontStyle: "italic", marginBottom, color: colors.textClear, fontSize: smallFontSize}}>
      Il n'y a aucun autre commentaire pour l'instant
    </div>}
    {rows}
    <NewComment {...{photo,album}}/>
  </div>);
}
