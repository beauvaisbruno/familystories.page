//@flow
import React from "react";
import type {Node} from "react";
import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";
import {mockUser, mockUsers} from "../../commons/.storybook/mocks/mockUser";
import {mockAlbum, mockAlbums} from "../../commons/.storybook/mocks/mockAlbums";
import {pseudoRndReset} from "../../commons/.storybook/mocks/mockUtils";
import {getAlbumGql} from "../oneAlbum/OneAlbum";

export default {
  title: "Diaporama",
};

export const logged = ():Node => {
  pseudoRndReset(3);
  const owner = mockUser({email: "email1@familystory.page"});
  const users = mockUsers({nbr: 4});
  const album = mockAlbum({owner, users, photoNbr:10});

  return <MockScreen
    {...{
      screen: "Diaporama",
      screenProps: {albumId: album._id},
      user: owner,
      remotes: [{
        query: getAlbumGql,
        data: {
          getAlbum: album,
        }
      }]
    }}/>;
};

export const notLogged = ():Node => {
  pseudoRndReset(3);
  const owner = mockUser({email: "email1@familystory.page"});
  const users = mockUsers({nbr: 4});
  const album = mockAlbum({owner, users, photoNbr:10});

  return <MockScreen
    {...{
      screen: "Diaporama",
      screenProps: {albumId: album._id},
      remotes: [{
        query: getAlbumGql,
        data: {
          getAlbum: album,
        }
      }]
    }}/>;
};


