//@flow
import type {Node} from "react";
import React, {useContext, useEffect, useState} from "react";
import "react-confirm-alert/src/react-confirm-alert.css";
import {pushHistory, ScreenContext} from "../../commons/components/ScreenNavigator";
import {isLandscape, isPortrait} from "../../commons/util";
import {useQuery} from "@apollo/client";
import {updateSeenAlbum} from "../../commons/updateSeenAlbum";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {env} from "../../commons/env";
import {SideBar} from "./SideBar";
import {getAlbumGql} from "../oneAlbum/OneAlbum";
import {marginBottom} from "./style";
import type {TheAlbum} from "../../model/TheAlbum";

const withoutSideBarWidth = "80vw";
const imgMobileHeight = "65vh";

export const CloseBtn = ({album}: {album:TheAlbum}): Node => {
  const {setScreen} = useContext(ScreenContext);
  return (<div
    style={{
      position: "absolute",
      display: "flex",
      justifyContent: "flex-end",
      ...isLandscape({
        width: withoutSideBarWidth,
      }),
      ...isPortrait({
        width: "100vw",
      }),
    }}>
    <img
      style={{
        cursor: "pointer",
        opacity: "0.5",
        ...isLandscape({
          width: "2vw",
          padding: "0.5vw",
        }),
        ...isPortrait({
          width: "10vw",
          padding: "1vw",
        }),
      }}
      src={require("../../commons/img/cancel.png").default}

      onClick={() => {
        setScreen("OneAlbum", {albumId: album._id});
      }}
    />
  </div>);
};
export const NavBtns = (
  {album, index, setPhotoId}
:{album:TheAlbum, index:number, setPhotoId: (string)=>void}): Node => {
  const hasLeft = index > 0;
  const hasRight = index < album.photos.length - 1;
  const {screen, setScreen} = useContext(ScreenContext);

  const btnStyle = {
    borderRadius: "50%",
    cursor: "pointer",
    ...isLandscape({
      height: "1.5vw",
      width: "1.5vw",
      padding: "1vw",
      margin: "0vw 3vw",
    }),
    ...isPortrait({
      height: "10vw",
      width: "10vw",
      padding: "1vw",
      margin: "0vw 3vw",
    }),
    objectFit: "contain",
    backgroundColor: "#aaaaaa88",
    opacity: "0.6"
  };
  return (<div
    style={{
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "stretch",
      ...isLandscape({
        position: "absolute",
        width: withoutSideBarWidth,
        height: "100vh",
      }),
      ...isPortrait({
        width: "100vw",
        marginBottom
      }),
    }}>
    <div style={{
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    }}>
      <img
        data-cy={"prevPhoto"}
        style={{
          ...btnStyle,
          visibility: hasLeft ? "visible" : "hidden"
        }}
        src={require("../../commons/img/left.png").default}

        onClick={() => {
          const newPhotoId = album.photos[index - 1]._id;
          setPhotoId(newPhotoId);
          pushHistory({...screen, props: {...screen.props, photoId: newPhotoId}});
        }}
      />

      <div style={{flexGrow: 1, display: "flex", justifyContent: "center"}}>
        {isPortrait() && <img
          style={btnStyle}
          src={require("../../commons/img/cancel.png").default}
          onClick={() => {
            setScreen("OneAlbum", {albumId: album._id});
          }}
        />}
      </div>
      <img
        data-cy={"nextPhoto"}
        style={{
          ...btnStyle,
          visibility: hasRight ? "visible" : "hidden"
        }}
        src={require("../../commons/img/right.png").default}
        onClick={() => {
          const newPhotoId = album.photos[index + 1]._id;
          pushHistory({...screen, props: {...screen.props, photoId: newPhotoId}});
          setPhotoId(newPhotoId);
        }}
      />
    </div>
  </div>);

};
export const DiaporamaPage = ({album, photoId: initPhotoId} : {album:TheAlbum, photoId:string}): Node => {
  const [photoId, setPhotoId] = useState(initPhotoId || album.photos[0]._id);

  function getIndex():number {
    let index = undefined;
    for (let i = 0; i < album.photos.length; i++) {
      const id = album.photos[i]._id;
      console.log("id: ", id,", photoId: ",photoId);
      if (id === photoId) {
        index = i;
        break;
      }
    }
    if (index === undefined) {
      throw Error("Unknown photoId:" + photoId);
    }
    return index;
  }

  let index = getIndex();

  // console.log("initPhotoId: ",initPhotoId);
  // console.log("photoId: ",photoId);
  // console.log("index: ",index);
  // console.log("index: ", index, "; photoId: ", photoId, ", album: ", album);
  const photo = album.photos[index];
  console.log("photo: ", photo);

  return (<div style={{
    display: "flex",
    ...isPortrait({
      flexDirection: "column"
    })
  }}>
    {isLandscape() && <NavBtns {...{album, photo, index, setPhotoId}}/>}
    {isLandscape() && <CloseBtn {...{album,}}/>}
    <img
      style={{
        ...isLandscape({
          height: "100vh",
          width: withoutSideBarWidth,
        }),
        ...isPortrait({
          width: "100vw",
          height: imgMobileHeight,
        }),
        objectFit: "contain",
        backgroundColor: "black",
      }}
      src={env.serverUrl + photo.url}/>
    {isPortrait() && <NavBtns {...{album, photo, index, setPhotoId}}/>}
    <SideBar {...{album, photo, index}}/>
  </div>);
};
export const Diaporama = ({albumId, photoId}: {albumId:string, photoId:string}): Node => {
  const {setScreen} = useContext(ScreenContext);
  let {loading, error, data} = useQuery(getAlbumGql, {
    variables: {_id: albumId}, onCompleted: updateSeenAlbum
  });

  const isEmpty = data && data.getAlbum && data.getAlbum.photos.length === 0;

  useEffect(() => {
    if (isEmpty)
      setScreen("OneAlbum", {albumId});
  }, [data]);

  console.log("data: ", {error, loading, data});
  return (
    <div>
      <QueryMessage {...{error, loading: loading || isEmpty}}>
        <DiaporamaPage {...{album: data && data.getAlbum, photoId}}/>
      </QueryMessage>
    </div>
  );
};
