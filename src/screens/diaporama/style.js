//@flow
import {isLandscape} from "../../commons/util";

export const smallFontSize:string = isLandscape() ? "1.2vw" : "4vw";
export const marginBottom:string = isLandscape() ? "1.2vw" : "4vw";
export const marginLeft:string = isLandscape() ? "0.5vw" : "1.5vw";
