//@flow
import type {TheAlbum} from"../../model/TheAlbum";
import type {ThePhoto} from"../../model/ThePhoto";
import type {Node} from "react";
import React, {useContext} from "react";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import {isLandscape, isPortrait} from "../../commons/util";
import {colors, css} from "../../commons/css";
import {Sep} from "../../commons/components/Sep";
import {AvatarImg} from "../../commons/components/AvatarImg";
import {ago} from "../../commons/timeAgo";
import {AllComments} from "./AllComments";
import {marginBottom, marginLeft, smallFontSize} from "./style";
import {ScreenContext} from "../../commons/components/ScreenNavigator";

export const SideBar = ({album, photo, index}:{album: TheAlbum, photo:ThePhoto, index:number}): Node => {
  
  const {user: loggedUser} = useContext(LoggedUserContext);
  const {setScreen} = useContext(ScreenContext);
  const owner = album.owner; 

  return (<div style={{
    ...isLandscape({
      height: "100vh",
      width: "20vw",
      backgroundColor:"#202020",
      padding:"0vw 1vw",
      paddingTop: marginBottom,
      boxSizing: "border-box"
    }),
    ...css.flexCol,
    alignItems:"center"
  }}>
    <div
      className="underlineHover"
      style={{ cursor:"pointer" }}
      onClick={()=> {
        setScreen("OneAlbum", {albumId: album._id });
      }}
    >{`${album.title} (${index+1}/${album.photos.length})`}</div>
    <Sep/>
    <div style={{ display:"flex", alignSelf:"flex-start", alignItems:"center",marginBottom }}>
      <AvatarImg {...{user:owner, style: {
        ...isLandscape({width: "3.5vw", height: "3.5vw"}),
        ...isPortrait({width: "10vw", height: "10vw"}),
      }
    }}/>
      <div style={{ display:"flex", flexDirection:"column", marginLeft }}>
        <div>{owner.pseudo}</div>
        <div style={{ color: colors.textClear,fontSize:smallFontSize }}>{ago(photo.addedDate)}</div>
      </div>
    </div>
    <div style={{ width:"100%" }}>{photo.description}</div>
    <Sep/>
    <AllComments {...{photo, album}}/>

  </div>);
};
