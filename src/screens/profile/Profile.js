//@flow
import type {Node} from "react";
import React, {useContext} from "react";
import {Menu} from "../../commons/components/Menu";
import {Page} from "../../commons/components/Page";
import {size} from "../../commons/css";
import {gql, useQuery} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {LoggedUserContext} from "../../commons/components/AuthLoader";
import {getUserGql} from "../../commons/queries";
import {Pseudo} from "./Pseudo";
import {Email} from "./Email";
import {PasswordReset} from "./PasswordReset";
import {TheUserScalars} from "../../model/TheUser";
import {Avatar} from "./Avatar";
import type {TheUser} from "../../model/TheUser";

export const updateProfileGql: any = gql`
 mutation updateProfile($user: TheUserInput!) {
   updateProfile(user: $user) {
      ${TheUserScalars}
   }
 }
`;

export const Profile = (): Node => {
  const {user:loggedUser} = useContext(LoggedUserContext);
  if (!loggedUser) throw Error("An user must logged");

  const  {loading, error, data} = useQuery(getUserGql, {variables: {email:loggedUser.email}});
  const user = data ? data.getUser : data;
  // console.log("render Profile, pseudo: ", user.pseudo, ", loading: ", loading);
  return (
    <div>
      <Menu/>
      <Page>
        <QueryMessage {...{error, loading}}>
            <div style={{display: "flex", flexDirection: "column", alignItems: "center", fontSize: size.text}}>
              <Email {...{user}}/>
              <Pseudo {...{user}}/>
              <PasswordReset {...{user}}/>
              <Avatar {...{user}}/>
            </div>
        </QueryMessage>
      </Page>
    </div>
  );
};
