//@flow
import type {Node} from "react";
import {gql, useMutation} from "@apollo/client";
import {useLoader} from "../../commons/hooks";
import React, {useEffect} from "react";
import {env} from "../../commons/env";
import {btnStyle, buttonWidth, leftStyle, marginBottom, rightStyle, rowStyle} from "./styles";
import {isLandscape, isPortrait} from "../../commons/util";
import {AvatarImg} from "../../commons/components/AvatarImg";
import type {TheUser} from "../../model/TheUser";

export const updateAvatarGql: any = gql`
  mutation updateAvatar($file: Upload!, $user: TheUserInput!) {
     updateAvatar(file: $file, user: $user) {
         _id
        email
        pseudo
        avatar
      }
   }`;

export function Avatar({user}:{user:TheUser}) :Node{
  const {Loader, setLoading, setError} = useLoader();
  const [updateAvatar, {error}] = useMutation(updateAvatarGql);
  useEffect(() => {
    if (error)
      setError(true);
  }, [error]);

  const input = React.useRef(null);
  return (
    <div style={rowStyle}>
      <div style={leftStyle}>
        Avatar :
      </div>
      <div style={rightStyle}>
        <div style={{width: buttonWidth, display: "flex", justifyContent: "center"}}>
          <AvatarImg
            {...{
              style:{
                ...isLandscape({width: "5vw"}),
                ...isPortrait({width: "30vw"}),
                marginBottom,
              },
              user
            }}
          />
        </div>
        <input
          data-cy="avatarInput" type="file" ref={input} style={{display: "none"}} required
          onChange={({target: {validity, files: [file],},}) => {
            if (!validity.valid)
              return;
            setLoading(true);
            updateAvatar({variables: {file, user}}).then((res) => {
              setLoading(false);
              console.log("updateAvatar done, user: ", res.data.updateAvatar);
            });
          }}/>
        <Loader/>
        <div
          data-cy="avatarEdit"
          onClick={() => {
            if (input.current)
            input.current.click();
          }}
          style={btnStyle}>
          Modifier
        </div>
      </div>
    </div>);
}
