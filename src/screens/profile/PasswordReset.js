//@flow
import type {Node} from "react";
import React from "react";
import {useLoader} from "../../commons/hooks";
import {colors} from "../../commons/css";
import {auth} from "../../commons/firebaseInit";
import {useState} from "react";
import {env} from "../../commons/env";
import {btnStyle, leftStyle, marginBottom, rightStyle, rowStyle} from "./styles";
import type {TheUser} from "../../model/TheUser";

export function PasswordReset({user}:{user:TheUser}):Node {
  const {Loader, setLoading, setError} = useLoader();
  const [done,setDone] = useState(env.fillForm);
  return (
    <div style={rowStyle}>
      <div style={leftStyle}>
        Mot de passe :
      </div>
      <div style={rightStyle}>
        <Loader/>
        {done && <div style={{color: colors.textClear, marginBottom}}>
          {"Un email avec des instructions de réinitialisation vous a été envoyé."}
        </div>}
        <div
          onClick={() => {
            setLoading(true);
            auth().sendPasswordResetEmail(user.email).then(function() {
              setLoading(false);
              setDone(true);
            }).catch(function(error) {
              console.error("error: ",error);
              setError(true);
            });
          }}
          style={btnStyle}>
          Réinitialiser
        </div>
      </div>
    </div>);
}
