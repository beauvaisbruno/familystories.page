//@flow
import type {Node} from "react";
import React from "react";
import {env} from "../../commons/env";
import {useMutation} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {colors, padding} from "../../commons/css";
import {isLandscape, isPortrait} from "../../commons/util";
import {updateProfileGql,} from "./Profile";
import {useEffect, useState} from "react";
import {btnStyle, color, fontSize, leftStyle, marginBottom, marginRight, rightStyle, rowStyle} from "./styles";
import {useFocus} from "../../commons/hooks";
import type {TheUser} from "../../model/TheUser";

export function Pseudo({user}:{user:TheUser}):Node {
  const [pseudo, setPseudo] = useState(user.pseudo);
  const [editing, setEditing] = useState(env.fillForm);
  const [updateProfile, {data, error, loading}] = useMutation(updateProfileGql);
  const [inputRef, setFocus] = useFocus();
  useEffect(() => {
    if (editing)
      setFocus();
  }, [editing]);
  // console.log("updateProfile: ", {data, error, loading});
  return (
    <div style={rowStyle}>
      <div style={leftStyle}>
        Pseudo :
      </div>
      {editing || <div style={rightStyle}>
        <div data-cy={"pseudo"} style={{marginBottom, color}}>{user.pseudo}</div>
        <QueryMessage {...{error, loading}}/>
        <div
          data-cy="pseudoEdit"
          onClick={() => {
            setEditing(true);
          }}
          style={btnStyle}>
          Modifier
        </div>
      </div>}
      {editing && <div style={rightStyle}>
        <input
          ref={inputRef}
          type="text"
          style={{
            textAlign: "center",
            color: colors.textClear,
            fontSize,
            backgroundColor: "transparent",
            outline: "none !important",
            ...isLandscape({
              ...padding("0.2vw"),
              width: "20vw",
            }),
            ...isPortrait({
              ...padding("1vw"),
              width: "53vw",
            }),
            borderColor: "white",
            marginBottom
          }}
          onChange={event => setPseudo(event.target.value)}
          value={pseudo}
          inputMode="email"
        />
        <div style={{
          ...isLandscape({display: "flex"}),
          ...isPortrait({display: "block"}),

        }}>
          <div
            onClick={() => {
              setEditing(false);
              updateProfile({
                variables: {user: {...user, pseudo}},
              }).then(res => {
                // console.log("mutate done");
              });
            }}
            style={{
              ...btnStyle, marginRight,
              ...isLandscape({width: "9vw"}),
              ...isPortrait({width: "40vw"}),
            }}>
            Enregistrer
          </div>
          {editing && <div
            onClick={() => {
              setEditing(false);
            }}
            style={{
              ...btnStyle, marginRight,
              ...isLandscape({width: "9vw"}),
              ...isPortrait({width: "40vw"}),
            }}>
            Annuler
          </div>}
        </div>
      </div>}
    </div>);
}
