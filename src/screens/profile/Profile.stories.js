//@flow
import type {Node} from "react";
import React from "react";
import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";
import {getUserGql} from "../../commons/queries";
import {updateProfileGql} from "./Profile";
import {updateAvatarGql} from "./Avatar";
import {mockUser} from "../../commons/.storybook/mocks/mockUser";

export default {
  title: "Profile",
};

export const nominal = ():Node => <MockScreen
  {...{
    screen: "Profile",
    user : mockUser({ email: "email1@familystory.page"}),
    remotes: [{
      query: getUserGql,
      data : {
        getUser: {
            ...mockUser({ email: "email1@familystory.page"})
        }
      }
    },
      {
        query: updateProfileGql,
        data : {
          updateProfile: {
            ...mockUser({ email: "email1@familystory.page"}),
            pseudo: "pseudo2",
          }
        }
      },
      {
        query: updateAvatarGql,
        data : {
          updateAvatar: {
            ...mockUser({ email: "email1@familystory.page"}),
            pseudo: "pseudo2",
            avatar: require("./mockAvatar.png").default,
          }
        }
      }
    ]
  }}/>;


