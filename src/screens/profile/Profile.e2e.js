//To run a specified suite or test, append .only to the it function. it.only(...)
//To run for mobile mobile(() => it(...)) or desktop desktop(() => it(...)) or both mobileAndDesktop(() => it(...))

import {desktop, mobileAndDesktop} from "../../commons/.e2e/e2eUtils";

mobileAndDesktop(()=> {
  // desktop(()=> {
  it("Profile, Auth then update profile", () => {
    //Auth
    cy.contains("email").click();
    cy.get("[placeholder='email@example.com']")
      .type("example1@familystories.page");
    cy.get("[placeholder='Mot de passe']")
      .type("family");
    cy.contains("connecter").click();
    //update profile
    cy.get("[data-cy='pseudoEdit']").click();
    cy.focused().clear().type("pseudo2");
    cy.contains("Annuler").click();
    cy.get("[data-cy='pseudoEdit']").click();
    cy.focused().clear().type("pseudo3");
    cy.contains("Enregistrer").click();
    cy.get("[data-cy='pseudo']").contains("pseudo3");
    cy.get("[data-cy='avatarEdit']").click();
    cy.get("[data-cy='avatarInput']").attachFile("mockAvatar.png");
    cy.get("[data-cy='avatarImg']").should("have.attr", "src")
      .should("include","avatar.png");
    cy.contains("Se déconnecter").click();
    cy.get("[data-cy='screen-Auth']");
  });
});
