//@flow
import type {Node} from "react";
import React from "react";
import {ScreenContext} from "../../commons/components/ScreenNavigator";
import {auth} from "../../commons/firebaseInit";
import {apolloClient} from "../../commons/apolloClient";
import {useContext} from "react";
import {btnStyle, color, leftStyle, marginBottom, rightStyle, rowStyle} from "./styles";
import type {TheUser} from "../../model/TheUser";

export function Email({user}:{user:TheUser}):Node {
  const {setScreen} = useContext(ScreenContext);
  return (
    <div style={rowStyle}>
      <div style={leftStyle}>
        Email :
      </div>
      <div style={rightStyle}>
        <div style={{marginBottom, color}}>{user.email}</div>
        <div
          onClick={() => {
            auth().signOut().then(()=> console.log("signOut"));
            apolloClient.cache.reset();
            setScreen("Auth");
          }}
          style={btnStyle}>
          Se déconnecter
        </div>
      </div>
    </div>);
}
