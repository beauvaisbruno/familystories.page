//@flow
import React from "react";
import {colors} from "../../commons/css";
import {EmailConnection} from "./EmailConnection";
import {isLandscape, isPortrait} from "../../commons/util";
import {GoogleConnection} from "./GoogleConnection";
import type {Node} from "react";

export function Auth():Node {
  return <div style={{display: "flex", flexDirection: "column", alignItems: "center"}}>
    <img src={require("../../commons/img/headerBg.png").default} style={{width: "100vw",
      ...isLandscape({position: "absolute"}) , }}/>
    <div style={{
      display: "flex",
      flexDirection: "column",
      zIndex:1,
      ...isLandscape({
        width: "75vw",
        minHeight: "54vw",
        alignItems: "center",
        backgroundColor: "rgba(255, 255, 255, 0.6)",
      }),
      ...isPortrait({
        width: "100vw",
        alignItems:"center"
      }),
    }}>
      <img src={require("./title.png").default} style={{
        ...isLandscape({width: "65vw", marginTop: "5vw"}),
        ...isPortrait({
          position: "absolute",top:0,
          width: "95vw",
          marginTop: "5vw"
        }),
      }}/>
      <div style={{
        ...isLandscape({color:colors.text, fontSize:"3vw",textAlign:"center" }),
        ...isPortrait({fontSize:"5vw",textAlign:"center", width: "90vw", }),
      }}>
        Partagez simplement des photos avec vos proches
      </div>
      <EmailConnection/>
      <GoogleConnection/>
    </div>
  </div>;
}
