//@flow
import React, {useState} from "react";
import {colors, css} from "../../commons/css";
import {isLandscape, isPortrait} from "../../commons/util";
import {env} from "../../commons/env";
import {useLoader} from "../../commons/hooks";
import {auth} from "../../commons/firebaseInit";
import type {Node} from "react";


function errorCodeToMsg(code: string): string {
  //https://firebase.google.com/docs/reference/js/firebase.User
  if (code === "auth/invalid-credential") return "Email incorrect";
  if (code === "auth/wrong-password") return "Mot de passe incorrect";
  if (code === "auth/email-already-in-use") return "Email déjà utilisé";
  if (code === "auth/weak-password") return "mot de passe trop simple";
  return "Connexion impossible";
}


function connect({setLoading, setError, email, password}) {

  setLoading(true);
  const onSigninWithEmailSuccess = () => {
    console.log("signIn success");
  };

  console.log("connexion email: ", email, ", password: ", password);
  //https://firebase.google.com/docs/auth/web/password-auth
  auth()
    .signInWithEmailAndPassword(email, password)
    .then(user => {
      console.log("Auth signInWithEmailAndPassword success, email: ", email, ", password: ",
        password);
      onSigninWithEmailSuccess();
    })
    .catch(signInError => {
      console.log("Auth signInWithEmailAndPassword errorCode: ", signInError.errorCode,
        ", email: ", email, ", password: ", password);

      if (signInError.code === "auth/user-not-found") { //https://firebase.google.com/docs/reference/js/firebase.User
        console.log("createUserWithEmailAndPassword...");
        auth().createUserWithEmailAndPassword(email, password)
          .then(userCredential => {
            console.log("createUserWithEmailAndPassword, user: ", userCredential);
            onSigninWithEmailSuccess();
          })
          .catch(createUserError => {
            console.error("createUserWithEmailAndPassword, error: ", createUserError);
            setError(errorCodeToMsg(signInError.code));
          });
      } else {
        setError(errorCodeToMsg(signInError.code));
      }
    });
}

export const EmailConnection = ():Node => {
  const [showEmailForm, setShowEmailForm] = useState(env.fillForm === true);
  const [email, setEmail] = useState(env.fillForm ? "example1@familystories.page" : "");
  const [password, setPassword] = useState(env.fillForm ? "family" : "");
  const {Loader, error, setLoading, setError} = useLoader();

  return (
    <div style={{
      ...isLandscape({
        marginTop: "12vw",
        width: "30vw",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        marginBottom: "1vw",
      }),
      ...isPortrait({
        marginTop: "5vw",
        width: "95vw",
      }),
    }}>
      {showEmailForm && (<>
        <input
          type="text"
          style={{
            ...css.inputBig,
            ...isLandscape({
              marginBottom: "1vw",
            }),
            ...isPortrait({marginBottom: "5vw"})
          }}
          onChange={event => setEmail(event.target.value)}
          value={email}
          placeholder={"email@example.com"}
          inputMode="email"
        />
        <input
          style={{
            ...css.inputBig,
            display: "block",
            ...isLandscape({
              marginBottom: "1vw",
            }),
            ...isPortrait({marginBottom: "5vw"})
          }}
          onChange={event => setPassword(event.target.value)}
          placeholder={"Mot de passe"}
          value={password}
        />
        <div style={{
          ...isLandscape({
            marginBottom: "2vw",
            fontSize: "1.5vw",
            color: colors.linkDark,
          }),
          ...isPortrait({
            marginBottom: "5vw",
            fontSize: "4.5vw",
            color: colors.linkClear,
          }),
          textDecoration: "underline",
          textAlign: "center"
        }}>
          Mot de passe oublié
        </div>
        {/*{error && <ResetEmail {...{ email }} />}*/}
        {error && <Loader/>}
      </>)}
      <div
        onClick={() => {
          if (!showEmailForm) {
            setShowEmailForm(true);
            return;
          }
          connect(
            {
              setLoading,
              setError,
              email,
              password,
            });
        }}
        style={{
          ...css.buttonBig,
          borderColor: "black",
          ...isLandscape({
            backgroundColor: colors.gold,
          }),
          ...isPortrait({
            backgroundColor: colors.gold,
          }),
        }}>
        {showEmailForm ? "Se Connecter" : "Connexion avec votre email"}
      </div>
    </div>
  );
};
