//@flow
import React, {useEffect, useState} from "react";
import {css} from "../../commons/css";
import {isLandscape, isPortrait} from "../../commons/util";
import {auth} from "../../commons/firebaseInit";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {useLoader} from "../../commons/hooks";
import type {Node} from "react";

function connect({setLoading, setError}) {
  setLoading(true);
  const provider = new auth.GoogleAuthProvider();
  auth().signInWithRedirect(provider).then((result) => {
    console.log("signInWithRedirect result: ", result);
    setLoading(false);
  }).catch((error) => {
    setError(true);
    // console.log("signInWithRedirect result: ", error);
    console.log("signInWithRedirect error.code: ", error.code);
  });
}

export const GoogleConnection = ():Node => {

  const {loading,error, setLoading, setError} = useLoader();

  useEffect(() => {
    auth()
      .getRedirectResult()
      .then((result) => {
        // console.log("result: ", result);
        console.log("RedirectResult user: ", result.user);
        //Handle by AuthListener
      }).catch((error) => {
      // console.log("result: ", error);
      console.log("RedirectResult error.code: ", error.code);
    });
  }, []);

  return (
    <div style={{
      ...isLandscape({
        width: "30vw",
        display: "flex",
        alignItems: "center",
        flexDirection: "column"
      }),
      ...isPortrait({
        marginTop: "5vw",
        width: "95vw",
      }),
    }}>
      <QueryMessage {...{loading,error}}>
        <div
          onClick={() => {
            connect({setLoading, setError});
          }}
          style={{
            ...css.buttonBig,
            borderColor: "black",
            backgroundColor: "#e81e1caa",
            // backgroundColor: "#e05c5baa",
          }}>
          Connexion avec Gmail
        </div>
      </QueryMessage>
    </div>
  );
};
