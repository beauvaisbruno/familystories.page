//@flow
import React from "react";

import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";
import type {Node} from "react";

export default {
  title: "Auth",
};

export const nominal = ():Node => <MockScreen
  {...{
    screen: "Auth",
  }}/>;
