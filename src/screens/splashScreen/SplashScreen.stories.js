//@flow
import type {Node} from "react";
import React from "react";
import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";

export default {
  title: "SplashScreen",
};

export const nominal = ():Node => <MockScreen
  {...{
    screen: "SplashScreen",
  }}/>;


