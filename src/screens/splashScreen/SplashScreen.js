//@flow
import React from "react";
import type {Node} from "react";

export const SplashScreen = (props: {||}):Node => {
  return (<div
    style={{
      height:"100vh",
      width:"100vw",
      display:"flex",
      flexDirection:"column",
      justifyContent:"center",
      alignItems: "center",
      backgroundColor:"white"
    }}
  >
    <img
      style={{
      width:"50vw",
    }}
      src={require("./splash.gif").default} />
  </div>);
};

