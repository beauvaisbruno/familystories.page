//@flow
import type {Node} from "react";
import React from "react";
import {MockScreen} from "../../commons/.storybook/mocks/MockScreen";
import {getAllAlbumsGql} from "./MyAlbums";
import {mockUser, mockUsers} from "../../commons/.storybook/mocks/mockUser";
import {mockAlbums} from "../../commons/.storybook/mocks/mockAlbums";
import {getUserGql} from "../../commons/queries";
import {pseudoRndReset} from "../../commons/.storybook/mocks/mockUtils";

export default {
  title: "MyAlbums",
};

export const logged = ():Node => {
  pseudoRndReset(8);
  const owner = mockUser({email: "email1@familystory.page"});
  const users = mockUsers({nbr: 4});

  return <MockScreen
    {...{
      screen: "MyAlbums",
      loggedUser: {email: "email1@familystory.page"},
      user: owner,
      remotes: [{
        query: getAllAlbumsGql,
        data: {
          getAllAlbums: mockAlbums({owner, users, nbr: 4}),
        },

      }]
    }}/>;
};

export const empty = ():Node => {
  pseudoRndReset(3);
  const owner = mockUser({email: "email1@familystory.page"});
  return <MockScreen
    {...{
      screen: "MyAlbums",
      loggedUser: {email: "email1@familystory.page"},
      remotes: [{
        query: getAllAlbumsGql,
        data: {
          getAllAlbums: []
        }
      }]
    }}/>;
};


