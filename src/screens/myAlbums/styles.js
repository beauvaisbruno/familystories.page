//@flow
import {colors, css, size} from "../../commons/css";
import {isLandscape, isPortrait} from "../../commons/util";

export const marginBottom:string = isLandscape() ? "1vw" : "2.5vw";
export const marginRight:string = isLandscape() ? "1vw" : "2.5vw";
export const lineHeight :string= isLandscape() ? "2.1vw" : "8vw";

export const buttonWidth :string= isLandscape() ? "20vw" : "50vw";
export const btnStyle = {
  ...css.buttonSmall,
  width: buttonWidth,
  marginBottom,
  // ...isLandscape({lineHeight: "1.5vw"}),
  // ...isPortrait({lineHeight: "5vw"}),
};
