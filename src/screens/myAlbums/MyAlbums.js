//@flow
import type {Node} from "react";
import React, {useContext} from "react";
import {confirmAlert} from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css"; // Import css
import {Menu} from "../../commons/components/Menu";
import {Page} from "../../commons/components/Page";
import {gql, useMutation, useQuery} from "@apollo/client";
import {QueryMessage} from "../../commons/components/QueryMessage";
import {btnStyle} from "./styles";
import type {TheAlbum} from "../../model/TheAlbum";
import {TheAlbumScalars} from "../../model/TheAlbum";
import {ScreenContext} from "../../commons/components/ScreenNavigator";
import {env} from "../../commons/env";
import {Sep} from "../../commons/components/Sep";
import {ThePhotoScalars} from "../../model/ThePhoto";
import {ago} from "../../commons/timeAgo";
import {isLandscape, isPortrait, plu} from "../../commons/util";
import {colors} from "../../commons/css";

export const smallFontSize:string = isLandscape() ? "1.2vw" : "4vw";

export const albumsResponse: any = `
  ${TheAlbumScalars}
   couverturePhoto {
    ${ThePhotoScalars}
   }
`;

export const deleteAlbumGql: any = gql`
  mutation deleteAlbum($albumId: String! ) {
    deleteAlbum(albumId: $albumId)
  }
`;

export const Dash = (): Node => {
  return <span>&nbsp;-&nbsp;</span>;
};
export const AlbumRow = ({album}: { album: TheAlbum }): Node => {
  const {setScreen} = useContext(ScreenContext);
  const [deleteAlbum, {error, loading}] = useMutation(deleteAlbumGql, {});
  // const sep = isLandscape() ? "&nbsp;-&nbsp;" : "";
  const Sep = isLandscape() ? Dash : () => null;
  return (<div
    style={{display: "flex", cursor: "pointer"}}
    onClick={() => {
      setScreen("OneAlbum", {albumId: album._id});
    }}
  >
    <QueryMessage {...{loading, error}}>
      <img

        style={{
          ...isLandscape({
            height: "7vw",
            width: "9.5vw",
            marginRight: "1vw",
          }),
          ...isPortrait({
            height: "28vw",
            width: "38vw",
            marginRight: "2vw",
          }),
          objectFit: "contain",
          backgroundColor: "black",
        }}
        src={album.couverturePhoto ? env.serverUrl + album.couverturePhoto.url : require("../../commons/img/defaultCover.png").default}/>
      <div style={{display: "flex", flexDirection: "column", flexGrow: "1"}}>
        <div style={{display: "flex"}}>
          {album.title}
        </div>
        <div style={{
          display: "flex",
          color: colors.textClear,
          fontSize: smallFontSize,
          ...isPortrait({flexDirection: "column"}),
        }}>
          <div>Créé {ago(album.addedDate)}</div>
          <div><Sep/>{album.photoIds.length + " " + plu(album.photoIds.length, "photo")}</div>
          <div><Sep/>{album.commentIds.length + " " + plu(album.commentIds.length, "commentaire")}</div>
          <div><Sep/>{album.seenCount + " " + plu(album.seenCount, "vue")}</div>
        </div>
      </div>
      <div style={{}}>
        <img
          title={"Supprimer l'album"}
          data-cy="deleteAlbum"
          style={{
            cursor: "pointer",
            ...isLandscape({width: "2vw", height: "2vw"}),
            ...isPortrait({width: "7vw", height: "7vw"}),
          }}
          src={require("../../commons/img/deleteWhite.png").default}
          onClick={(event) => {
            // Avoid album selection
            event.stopPropagation();
            confirmAlert({
              message: "Êtes vous certain de vouloir supprimer cet album et toutes les photos associées ?",
              buttons: [
                {
                  label: "Confirmer",
                  onClick: () => {
                    deleteAlbum({refetchQueries: [{query: getAllAlbumsGql}], variables: {albumId: album._id}})
                      .then(res => console.log("deleteAlbum res: ", res));
                  }
                },
                {
                  label: "Annuler",
                }
              ],
            });
          }}
        />
      </div>
    </QueryMessage>
  </div>);
};

export const getAllAlbumsGql: any = gql`
  query {
    getAllAlbums {
      ${albumsResponse}
    }
  }`;

export const AlbumList = (): Node => {
  const {loading, error, data} = useQuery(getAllAlbumsGql);
  const rows = [];
  // console.log("AlbumList data: ", data);
  if (data)
    data.getAllAlbums.forEach((album) => {
      rows.push(<Sep {...{key: album._id + "sep"}}/>);
      rows.push(<AlbumRow {...{album, key: album._id}}/>);
    });
  return (
    <QueryMessage {...{error, loading}}>
      {rows}
    </QueryMessage>
  );
};

export const createAlbumGql: any = gql`
    mutation createAlbum {
      createAlbum {
        ${TheAlbumScalars}
      }
    }
    `;

export const MyAlbums = (): Node => {
  const [createAlbum, {error, loading}] = useMutation(createAlbumGql);

  const {setScreen} = useContext(ScreenContext);

  return (
    <div>
      <Menu/>
      <Page>
        <div style={{display: "flex", justifyContent: "center"}}>
          <QueryMessage {...{error, loading}}>
            <div
              onClick={() => {
                createAlbum({refetchQueries: [{query: getAllAlbumsGql}]}).then(res => {
                  const album: TheAlbum = res.data.createAlbum;
                  console.log("album: ", album);
                  setScreen("OneAlbum", {albumId: album._id});
                });
              }}
              style={btnStyle}>
              Créer un album
            </div>
          </QueryMessage>
        </div>
        <AlbumList/>
      </Page>
    </div>
  );
};
