const {join} = require("path");
const {NodeSSH} = require("node-ssh");

let credential;
if (process.env.ssh)
  credential = JSON.parse(Buffer.from(process.env.ssh, "base64").toString("ascii"));
else
  credential = require("./ssh.secure.json");
console.log("credential: ",credential);

const deploy = async () => {
  const clientRootPath = join(__dirname, "..");
  const remoteRootPath = credential.remotePath;

  const ssh = new NodeSSH();
  await ssh.connect(credential);

  const exec = async (cmd) => {
    console.log(">cmd:", cmd);
    const result = await ssh.execCommand(cmd, {
      onStdout(chunk) {
        console.log(chunk.toString("utf8"));
      },
      onStderr(chunk) {
        console.log(chunk.toString("utf8"));
      },
    });
    // result.stdout !== "" && console.log(">" + result.stdout.trim());
    // result.stderr !== "" && console.error(">" + result.stderr.trim());
  };

  //provider dependent
  await exec("pkill Passenger");

  await exec("rm -r " + remoteRootPath + "/.version");
  await ssh.putFile( join(clientRootPath, ".version"),remoteRootPath + "/.version");
  console.log(join(clientRootPath, ".version") +" => "+remoteRootPath + "/.version");

  await exec("rm " + remoteRootPath + "/server/package.json");
  await exec("rm " + remoteRootPath + "/server/yarn.lock");
  await exec("rm -r " + remoteRootPath + "/server/build");
  await exec("rm -r " + remoteRootPath + "/build");

  const fileToServer = async (name) => {
    await ssh.putFile(
      join(clientRootPath, "server", name),
      remoteRootPath + "/server/"+name);
    console.log(remoteRootPath + "/server/"+name);
  };

  await fileToServer("package.json");
  await fileToServer("yarn.lock");
  await fileToServer("app.js");

  let putOptions = {
    recursive: true,
    concurrency: 10,
    tick: function (localPath, remotePath, error) {
      if (error){
        console.error("putDirectory error:",error,", remotePath: ",remotePath);
        process.exit(error);
      }else
        console.log(remotePath);
    }
  };
  await ssh.putDirectory(
    join(clientRootPath, "build"),
    remoteRootPath + "/build",
    putOptions);
  await ssh.putDirectory(
    join(clientRootPath, "server", "build"),
    remoteRootPath + "/server/build",
    putOptions);

  await exec(
    //host provider dependent
    "source /home/ficheski/nodevenv/familystories.page/server/10/bin/activate " +
    "&& cd " + remoteRootPath + "/server " +
    // "&& npm install --production"
    "&& npm install"
  );

  await exec("touch " + remoteRootPath + "/server/tmp/restart.txt");
  ssh.dispose();
};

deploy().then(() => {
  console.log("Deploy done.");
}).catch((error) => {
  console.error("Deploy error: ", error);
});

