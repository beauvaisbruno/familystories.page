# Available Scripts

## Main Scripts

`nps workflow.dev_with_storybook`

*Start to dev using Component-Driven Development methodology with story book.*

`nps workflow.snapshot_tests`

*Check stories against previous snapshots.*

`nps workflow.snapshot_update`

*Take a snapshot for each story.*

`nps workflow.dev_with_cypress`

*Start to dev driven by en-to-end tests with Cypress.*

`nps workflow.dev_with_cypress`

*Start to dev driven by en-to-end tests with Cypress.*

`nps eslint`

*Check eslint rules for all the project*

`nps flow`

*Check flow type for all the project*

`nps react.start`

*start a react server*

`nps react.start_ci`

`nps react.build`

`nps storybook.start`

*start to dev using Component-Driven Development methodology with story book.*

`nps storybook.snapshot_tests`

*check stories against previous snapshots.*

`nps storybook.snapshot_update`

*take a snapshot for each story.*

`nps storybook.build`

`nps storybook.start_ci`

`nps storybook.tests_ci`

`nps server.start_express`

*start an express server on port 8080*

`nps server.start_mongodb_no_restore`

`nps server.restore_default_db`

*restore database and upload folder to default values*

`nps server.export_default_db`

*Set database as default*

`nps server.open_db_tools`

*open a db explorer (MongoDBCompass) and a graphql playground*

`nps server.encode_secure`

*print encoded secure credentials (base64) to export as environment variable on production platform and CI server.*

`nps server.install`

`nps server.start_mongodb`

*start mongodb on port 27017 with default database*

`nps server.start_all`

*start express server and mongodb with default database.*

`nps server.build`

`nps cypress.react`

*start react server for Cypress with code coverage support*

`nps cypress.open`

*open Cypress user interface*

`nps cypress.coverage_report`

*open code coverage report page*

`nps cypress.start_to_dev`

*Start to dev driven by en-to-end tests with Cypress.*

`nps sync_model.watch`

*Sync model folder on change to client (./src) and server (./server)*

`nps sync_model.once`

`nps docker.create_docker`

`nps docker.register_gitlab_runner`

`nps docker.install_mangodb_debian`
