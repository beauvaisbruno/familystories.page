//https://github.com/sezna/nps
//run 'npx update_scripts' to export to scripts sections of package.json

//You need to setup the react native environnement to run these scripts
//https://reactnative.dev/docs/environment-setup

//Sugar functions
const util = require("nps-utils");
const _ = (script, description) => ({script, description});
const seq = (...args) => util.series(...args);
const par = (...args) => util.concurrent(...args);

const REACT_PORT = 80;
const STORYBOOK_PORT_DEV = REACT_PORT;
const STORYBOOK_PORT_CI = 9009;
const EXPRESS_PORT = 8080;
const MONGO_PORT = 27017;

const sync_model = (() => {
  return {
    watch: _("npx nodemon --watch src/model --watch server/src/model ./scripts/copyModel.js",
      "Copy model folder on change to client (./src) and server (./server)"),
    once: "node ./scripts/copyModel.js",
  };
})();

const react = (() => {
  const s = {
    start: _(par({
      sync_model: sync_model.watch,
      //start on REACT_PORT see .env file
      react: seq("npx kill-port " + REACT_PORT, "react-scripts start"),
    }), "start a react server"),
    //start on REACT_PORT see .env file
    start_ci: seq("npx kill-port " + REACT_PORT, sync_model.once, "react-scripts -r @cypress/instrument-cra start"),
    //CI=false => https://github.com/facebook/create-react-app/issues/3657
    build: seq(sync_model.once, "cross-env CI=false react-scripts build")
  };
  return s;
})();

const server = (() => {
  const cd = "cd server";
  const s = {
    //nyc => https://github.com/cypress-io/code-coverage
    start_express: _(seq(cd, "kill-port " + EXPRESS_PORT, "nodemon --watch src --exec nyc --silent babel-node src/server.js"), "start express on port " + EXPRESS_PORT),
    start_mongodb_no_restore: seq(cd, "kill-port " + MONGO_PORT, "mongod --dbpath=database_live"),
    restore_default_db: _(seq("node ./scripts/restoreUpload.js",
      cd, "mongo --eval \"db = db.getSiblingDB('family-stories'); db.dropDatabase();\"",
      "mongorestore database_default --drop"),
      "restore database and upload folder to default values"),
    export_default_db: _(seq(cd, "mongodump -o database_default"), "Set database as default"),
    open_db_tools: _(seq("open-cli http://localhost:" + EXPRESS_PORT + "/graphql", "MongoDBCompass"), "open a db explorer (MongoDBCompass) and a graphql playground"),
    encode_secure: _("node scripts/encodeSecure.js", "print encoded secure credentials (base64) to export as environment variable on production platform and CI server."),
    install: seq("cd server", "yarn install", "cd ..")
  };

  s.start_mongodb = _(par({
      mangodb: s.start_mongodb_no_restore,
      restore: s.restore_default_db.script
    }), "start mongodb on port 27017 with default database"
  );

  s.start_all = _(par({
    mongodb: s.start_mongodb.script,
    express: s.start_express.script,
    tools: s.open_db_tools.script
  }), "start express server and mongodb with default database.");

  s.build = seq(sync_model.once, cd, "npx babel src --out-dir build", "cd ..");

  s.start_ci = _(par({
    mongodb: seq(cd, "mongod --dbpath=database_live --quiet || echo 'mongodb stopped'"),
    express: seq(sync_model.once, cd, "npx cross-env CI=true nyc --silent babel-node src/server.js  || echo 'express server stopped'"),
    // restore_default_db: seq("sleep 5000",s.restore_default_db.script), //db is restore before each e2e tests
  }));

  return s;
})();

const screen_captures_build = "node ./scripts/capture.js";
const storybook = (() => {
  let s = {
    start: _(par({
      sync_model: sync_model.watch,
      storybook: seq("kill-port " + STORYBOOK_PORT_DEV, "start-storybook -p " + STORYBOOK_PORT_DEV + " --quiet")
    }), "start to dev using Component-Driven Development methodology with story book."),
    snapshot_tests: _("npx react-scripts test --watchAll=false src/commons/.storybook/storyshots.test.js",
      "check stories against previous snapshots."),
    snapshot_update: _(seq("npx cross-env STORY_UPDATE=true react-scripts test --watchAll=false src/commons/.storybook/storyshots.test.js",
      screen_captures_build,
      "open-cli ./screen_captures/index.html"
      ),
      "take a snapshot for each story."),
  };
  s.build = seq(sync_model.once, "cross-env build-storybook --quiet -o ./storybook_build");
  s.start_ci = seq(sync_model.once, "kill-port " + STORYBOOK_PORT_CI, "start-storybook -p " + STORYBOOK_PORT_CI + " --quiet");
  s.tests_ci = "npx react-scripts test --watchAll=false src/commons/.storybook/storyshots.test.js";
  return s;
})();

const cypress = (() => {
  const s = {
    //BROWSER=none => prevent to open localhost page by react on the browser
    react: _(seq("cross-env BROWSER=none react-scripts -r @cypress/instrument-cra start"), "start react server for Cypress with code coverage support"),
    open: _("cypress open", "open Cypress user interface"),
    coverage_report: _("open-cli ./coverage/lcov-report/index.html", "open code coverage report page"),
    headless_tests: _("cypress run","Run all end-to-end tests"),
    tests_prod_ci: "cross-env CYPRESS_INSTRUMENT_PRODUCTION=true cypress run"
  };
  s.start_to_dev = _(par({
    react: s.react.script,
    sync_model: sync_model.watch,
    cypress: s.open.script,
    coverage_report: s.coverage_report.script,
    express: server.start_all.script,
  }), "Start to dev driven by en-to-end tests with Cypress.");
  return s;
})();

const workflow = (() => {
  return {
    dev_with_storybook: storybook.start,
    snapshot_tests: storybook.snapshot_tests,
    snapshot_update: storybook.snapshot_update,
    dev_with_cypress: cypress.start_to_dev,
    e2e_tests: cypress.headless_tests,
  };
})();

const docker = {
  //https://docs.gitlab.com/runner/install/docker.html
  create_docker: seq("docker volume create gitlab-runner-config",
    "docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest"),
  //https://docs.gitlab.com/runner/register/index.html#docker
  register_gitlab_runner: "docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register",
  //https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/
  install_mangodb_debian: seq(
    // "cat /etc/os-release",
    "wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -",
    "echo \"deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main\" | tee /etc/apt/sources.list.d/mongodb-org-4.4.list",
    "apt-get update --allow-insecure-repositories --allow-unauthenticated",
    //NOTE: since the docker image has no sytemd the install exit code != 0
    "apt-get install --allow-unauthenticated -y mongodb-org || echo 'mongodb installed with success'",
  )
};

//run by gitlab CI pipeline: see .gitlab-ci.yml
const ci = (() => {
  const s = {
    install_mangodb: docker.install_mangodb_debian,
    install_server_module: server.install,
    start_servers: server.start_ci,
    start_storybook: storybook.start_ci,
    storybook_tests: storybook.tests_ci,
    start_react: react.start_ci,
    cypress_tests: cypress.headless_tests.script,
    stop_servers: seq("npx kill-port " + REACT_PORT, "npx kill-port " + STORYBOOK_PORT_CI, "npx kill-port " + EXPRESS_PORT, "npx kill-port " + MONGO_PORT),
    storybook_build: storybook.build,
    screen_captures_build,
    //master branch only
    react_build: react.build,
    server_build: server.build,
    incrementVersion: "node ./scripts/version.js",
    deploy: "node ./scripts/deploy.js",
  };

  return s;
})();

const packageScripts = {
  scripts: {
    workflow,
    eslint: "eslint ./src ./server/src", //Check eslint rules for all the project, should be used through ide
    flow: "flow", //Check flow type for all the project, should be used through ide
    react,
    storybook,
    server,
    cypress,
    sync_model,
    docker,
    ci,
  },
};

console.warn(
  "To see all available commands run 'nps'. You may have a look at scripts/package-scripts.js for further documentations.");
module.exports = packageScripts;
