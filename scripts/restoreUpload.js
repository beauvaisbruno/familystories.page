const fs = require("fs");
const path = require("path");

const upload = path.resolve(__dirname, "..", "server", "uploads");

if (fs.existsSync(upload)) {
  const files = fs.readdirSync(upload);
  files.forEach(function (file, index) {
    if (file === ".gitkeep")
      return;
    const curPath = upload + "/" + file;
    if (fs.lstatSync(curPath).isFile())
      fs.unlinkSync(curPath);
  });
} else {
  fs.mkdirSync(upload);
  fs.closeSync(fs.openSync(path.join(upload, ".gitkeep"), "w"));
}

