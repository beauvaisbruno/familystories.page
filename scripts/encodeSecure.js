const fs = require("fs");
const path = require("path");

const encode = (file)=> {
  return Buffer.from(fs.readFileSync(path.resolve(__dirname, "..",file))).toString("base64");
};

console.log("Environment variables to export on production platform and CI server.\n");
console.log("ssh=\n",encode("scripts/ssh.secure.json"));
console.log("\nmongodb=\n",encode("server/mongodb.secure.json"));
console.log("\nfirebaseKey=\n",encode("server/firebaseKey.secure.json"));
console.log("\nsmtp=\n",encode("server/smtp.secure.json"));
