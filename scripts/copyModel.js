const fs = require("fs")
const path = require("path")

const copyRecursiveSync = function(src, dest) {
  const exists = fs.existsSync(src);
  const stats = exists && fs.statSync(src);
  const isDirectory = exists && stats.isDirectory();
  if (isDirectory) {
    if (!fs.existsSync(dest))
      fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function(childItemName) {
      copyRecursiveSync(path.join(src, childItemName),
        path.join(dest, childItemName));
    });
  } else {
    if (!fs.existsSync(dest) ||
      fs.statSync(dest).mtime < fs.statSync(src).mtime){
      // console.log(fs.statSync(dest).mtime,"<",fs.statSync(src).mtime);
      console.log("copy ",src," => ",dest);
      fs.copyFileSync(src, dest);
    }
  }
};

console.log("copy model client => server");
copyRecursiveSync(path.resolve(__dirname, "../server/src/model"),path.resolve(__dirname, "../src/model"),);
console.log("copy model server => client");
copyRecursiveSync(path.resolve(__dirname, "../src/model"),path.resolve(__dirname, "../server/src/model"),);
console.log("Copy model done.");
