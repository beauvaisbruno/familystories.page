const glob = require("glob");
const path = require("path");
const fs = require("fs");

const projectPath = path.resolve(__dirname, "..");
const dstPath = path.join(projectPath, "screen_captures");

const snaps = glob.sync("./src/**/__snapshots__/**/*.png", {cwd: projectPath});
const videos = glob.sync("./cypress/videos/**/*.mp4", {cwd: projectPath});

const items = {};
// console.log("snaps: ", snaps);
// console.log("videos: ", videos);

if (!fs.existsSync(dstPath)) {
  fs.mkdirSync(dstPath);
}
snaps.forEach(filePath => {
  const src = path.basename(filePath);
  const name = path.basename(filePath, ".png");
  const comp = name.split("-")[0];
  if (!items[comp])
    items[comp] = {};
  const dest = path.join(dstPath, src);
  items[comp][src] = {
    filePath,
    dest,
    name,
    comp,
    src,
  };
  const from = path.resolve(projectPath, filePath);
  console.log("copy: ", from, "=>", dest);
  fs.writeFileSync(dest, fs.readFileSync(from));
});

videos.forEach(filePath => {
  //filePath === './cypress/videos/auth/Auth.e2e.js.mp4'
  let name = path.basename(filePath, ".mp4").replace(".e2e.js", "").toLowerCase();
  const comp = name;
  const src = name + ".mp4";
  if (!items[comp])
    items[comp] = {};
  const dest = path.join(dstPath, src);
  items[name][src] = {
    filePath,
    dest,
    name,
    src,
  };
  fs.writeFileSync(dest, fs.readFileSync(path.resolve(projectPath, filePath)));
  console.log("copy: ", dest);
});


// console.log("items: ", items);
let html = "<html><head><style>" +
  "img, video{height: 20vw; padding: 1vw;}" +
  "a{display: inline-flex; align-items: center; flex-direction: column;}" +
  "h2 {background-color: cornflowerblue;}" +
  "</style></head><body>";

// console.log("process.env: ",process.env);

html += "commit: "+process.env.CI_COMMIT_BRANCH+" > "+process.env.CI_COMMIT_SHA+" - "+new Date().toISOString()+"</br>";

Object.keys(items).forEach((comp) => {
  html += "<div class='comp'><h2>" + comp.toUpperCase() + "</h2>";
  Object.keys(items[comp]).forEach((name) => {
    const src = items[comp][name].src;
    html += "<a href='./" + src + "'>" + name;
    if (src.includes("mp4"))
      html += "<video src='./" + src + "' controls>";
    else
      html += "<img src='./" + src + "'>";
    html += "</a>";
  });
  html += "</div>";
});

html += "</body></html>";

// console.log("html: ", html);
fs.writeFileSync(path.join(dstPath, "index.html"), html);
console.log("write: ", path.resolve(path.join(dstPath, "index.html")));

