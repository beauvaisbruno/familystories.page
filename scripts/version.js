// The project follow the Semantic Versioning https://semver.org/
// Uncomment to increment when the branch is merged to master
//https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/
const {exec} = require("child_process");
const path = require("path");
const fs = require("fs");

// const incrementVersion = 0; // "MAJOR"
// const incrementVersion = 1; // "MINOR"
const incrementVersion = 2; // "HOTFIX"

// if (!process.env.CI)
//   throw Error("This script should be run on CI server");

const cmdSync = (cmd) => {
  return new Promise((resolve, reject) => {
    console.log("cmd: ", cmd);
    exec(cmd, (error, stdout, stderr) => {
      if (error) console.error(error);
      if (stderr) console.error(stderr);
      if (stdout) console.log(stdout);
      if (error) reject(error);
      resolve(stdout);
    });
  });
}

(async () => {
  //Add ssh key to the CI machine
  await cmdSync("rm -rf ~/.ssh");
  await cmdSync("mkdir -p ~/.ssh && chmod 700 ~/.ssh");
  await cmdSync("ssh-keyscan gitlab.com >> ~/.ssh/known_hosts && chmod 644 ~/.ssh/known_hosts");
  await cmdSync("eval $(ssh-agent -s) && echo \"$SSH_GIT_PRIVATE_KEY\" | ssh-add -");

  //Convert remote https url to git url
  // https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/username/project.git
  // git@gitlab.com:username/project.git
  const repositoryHttps = process.env.CI_REPOSITORY_URL;
  console.log("repositoryHttps: ", repositoryHttps);
  const split = repositoryHttps.split("@")[1].split("/");
  const repositoryGit = "git@" + split[0] + ":" + split[1] + "/" + split[2];
  console.log("repositoryGit: ", repositoryGit);
  await cmdSync("git remote set-url --push origin " + repositoryGit);

  let lastVersion = "0.0.0";
  try {
    //https://gist.github.com/rponte/fdc0724dd984088606b0
    const tags = await cmdSync("git tag --sort=committerdate");
    const tagLines = tags.trim().split("\n");
    console.log("tagLines: ", tagLines);
    if (tagLines[0].length > 4)
      lastVersion = tagLines[tagLines.length - 1];
  } catch (e) {
    console.log("No last version tag found.");
  }
  console.log("lastVersion: ", lastVersion);

  const splitted = lastVersion.split(".");
  splitted[incrementVersion]++;
  const newVersion = splitted.join(".");

  console.log("newVersion: ", newVersion);

  await cmdSync("git tag " + newVersion);

  await cmdSync("eval $(ssh-agent -s) && echo \"$SSH_GIT_PRIVATE_KEY\" | ssh-add - && git push origin " + newVersion);

  fs.writeFileSync(path.resolve(__dirname, "..", ".version"), newVersion);
})().catch(error => {
  console.log("Version updated failed, error: ", error);
  process.exit(1);
});












